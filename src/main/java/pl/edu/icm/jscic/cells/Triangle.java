/* ***** BEGIN LICENSE BLOCK *****
 * Java Scientific Containers (JSciC)
 * Copyright (C) 2015 onward University of Warsaw, ICM
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * ***** END LICENSE BLOCK ***** */
package pl.edu.icm.jscic.cells;

import pl.edu.icm.jscic.utils.MatrixMath;
import pl.edu.icm.jlargearrays.FloatLargeArray;

/**
 * Triangle cell.
 *
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 */
public class Triangle extends Cell {

    /**
     * Constructor of a triangle.
     *
     * @param nsp space dimension
     * @param i0 node index
     * @param i1 node index
     * @param i2 node index
     * @param orientation significant if cell is of dim nspace or is a face of a
     * cell of dim nspace
     */
    public Triangle(int nsp, int i0, int i1, int i2, byte orientation) {
        type = CellType.TRIANGLE;
        nspace = nsp;
        vertices = new int[3];
        vertices[0] = i0;
        vertices[1] = i1;
        vertices[2] = i2;
        this.orientation = orientation;
        normalize();
    }

    /**
     * Constructor of a triangle.
     *
     * @param nsp space dimension
     * @param i0 node index
     * @param i1 node index
     * @param i2 node index
     */
    public Triangle(int nsp, int i0, int i1, int i2) {
        this(nsp, i0, i1, i2, (byte) 1);
    }

    @Override
    public Cell[][] subcells() {
        Cell[][] tr = new Cell[3][];
        tr[2] = triangulation();
        tr[1] = faces();
        tr[0] = new Point[vertices.length];
        for (int i = 0; i < vertices.length; i++) {
            tr[0][i] = new Point(nspace, vertices[i]);
        }
        return tr;
    }

    @Override
    public Cell[] triangulation() {
        Triangle[] subdiv = {this};
        return subdiv;
    }

    @Override
    public int[][] triangulationVertices() {
        return new int[][]{vertices};
    }

    /**
     * Generates the array of triangle vertices from the array of point
     * vertices.
     *
     * @param vertices array of vertices
     *
     * @return array of triangle vertices
     */
    public static int[][] triangulationVertices(int[] vertices) {
        return new int[][]{vertices};
    }

    @Override
    public Cell[] faces() {
        return faces(vertices, orientation);
    }

    @Override
    public Cell[] faces(int[] nodes, byte orientation) {
        Segment[] faces = new Segment[3];
        faces[0] = new Segment(nspace, nodes[1], nodes[2], orientation);
        faces[1] = new Segment(nspace, nodes[2], nodes[0], orientation);
        faces[2] = new Segment(nspace, nodes[0], nodes[1], orientation);
        return faces;
    }

    @Override
    public final int[] normalize() {
        if (!normalize(vertices)) {
            orientation = (byte) (1 - orientation);
        }
        return vertices;
    }

    /**
     * A convenience function that reorders array of vertices to increasing
     * order.
     *
     * @param vertices vertices
     *
     * @return always true
     */
    public static boolean normalize(int[] vertices) {
        int i = 0;
        for (int j = 1; j < 3; j++) {
            if (vertices[j] < vertices[i]) {
                i = j;
            }
        }
        if (i == 1) {
            int t = vertices[0];
            vertices[0] = vertices[1];
            vertices[1] = vertices[2];
            vertices[2] = t;
        } else if (i == 2) {
            int t = vertices[0];
            vertices[0] = vertices[2];
            vertices[2] = vertices[1];
            vertices[1] = t;
        }
        if (vertices[1] > vertices[2]) {
            int t = vertices[2];
            vertices[2] = vertices[1];
            vertices[1] = t;
            return false;
        }
        return true;
    }

    @Override
    public byte compare(int[] v) {
        if (v.length != 3) {
            return 0;
        }
        return compare(new Triangle(this.nspace, v[0], v[1], v[2]));
    }

    /**
     * Computes barycentric coordinates.
     *
     * @param p point coordinates
     * @param coords array of coordinates
     *
     * @return barycentric coordinates of point p in this triangle
     */
    public TetrahedronPosition barycentricCoords(float[] p, FloatLargeArray coords) {
        float[][] A = new float[2][2];
        float[] v0 = new float[2];
        float[] b = new float[2];
        long l = 2 * vertices[0];
        for (int i = 0; i < 2; i++) {
            v0[i] = coords.getFloat(l + i);
        }
        for (int i = 0; i < 2; i++) {
            b[i] = p[i] - v0[i];
            for (int j = 0; j < 2; j++) {
                A[i][j] = coords.getFloat(2 * (long) vertices[j + 1] + i) - v0[i];
            }
        }

        float[] x;
        try {
            x = MatrixMath.lsolve(A, b);
        } catch (IllegalArgumentException ex) {
            return null;
        }
        if (x == null || x[0] < 0 || x[1] < 0 || x[0] + x[1] > 1) {
            return null;
        }
        x[0] = 1 - (x[0] + x[1]);
        TetrahedronPosition result = new TetrahedronPosition(vertices, x, this);
        return result;
    }

}
