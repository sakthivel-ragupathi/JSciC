/* ***** BEGIN LICENSE BLOCK *****
 * Java Scientific Containers (JSciC)
 * Copyright (C) 2015 onward University of Warsaw, ICM
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * ***** END LICENSE BLOCK ***** */
package pl.edu.icm.jscic.cells;

/**
 * Cell type.
 *
 * @author Piotr Wendykier (p.wendykier@icm.edu.pl), University of Warsaw, ICM
 *
 */
public enum CellType
{

    EMPTY,
    POINT
    {
        @Override
        public int getValue()
        {
            return 0;
        }

        @Override
        public int getNVertices()
        {
            return 1;
        }

        @Override
        public boolean isSimplex()
        {
            return true;
        }

        @Override
        public String getName()
        {
            return "point";
        }

        @Override
        public String getUCDName()
        {
            return "pt";
        }

        @Override
        public String getPluralName()
        {
            return "points";
        }

    },
    SEGMENT
    {
        @Override
        public int getValue()
        {
            return 1;
        }

        @Override
        public int getNVertices()
        {
            return 2;
        }

        @Override
        public int getDim()
        {
            return 1;
        }

        @Override
        public boolean isSimplex()
        {
            return true;
        }

        @Override
        public String getName()
        {
            return "line";
        }

        @Override
        public String getUCDName()
        {
            return "line";
        }

        @Override
        public String getPluralName()
        {
            return "lines";
        }

    },
    TRIANGLE
    {
        @Override
        public int getValue()
        {
            return 2;
        }

        @Override
        public int getNVertices()
        {
            return 3;
        }

        @Override
        public int getDim()
        {
            return 2;
        }

        @Override
        public boolean isSimplex()
        {
            return true;
        }

        @Override
        public String getName()
        {
            return "triangle";
        }

        @Override
        public String getUCDName()
        {
            return "tri";
        }

        @Override
        public String getPluralName()
        {
            return "triangles";
        }

    },
    QUAD
    {
        @Override
        public int getValue()
        {
            return 3;
        }

        @Override
        public int getNVertices()
        {
            return 4;
        }

        @Override
        public int getDim()
        {
            return 2;
        }

        @Override
        public String getName()
        {
            return "quadrangle";
        }

        @Override
        public String getUCDName()
        {
            return "quad";
        }

        @Override
        public String getPluralName()
        {
            return "quadrangles";
        }

    },
    TETRA
    {
        @Override
        public int getValue()
        {
            return 4;
        }

        @Override
        public int getNVertices()
        {
            return 4;
        }

        @Override
        public int getDim()
        {
            return 3;
        }

        @Override
        public boolean isSimplex()
        {
            return true;
        }

        @Override
        public String getName()
        {
            return "tetrahedron";
        }

        @Override
        public String getUCDName()
        {
            return "tet";
        }

        @Override
        public String getPluralName()
        {
            return "tetrahedra";
        }

    },
    PYRAMID
    {
        @Override
        public int getValue()
        {
            return 5;
        }

        @Override
        public int getNVertices()
        {
            return 5;
        }

        @Override
        public int getDim()
        {
            return 3;
        }

        @Override
        public String getName()
        {
            return "pyramid";
        }

        @Override
        public String getUCDName()
        {
            return "pyr";
        }

        @Override
        public String getPluralName()
        {
            return "pyramids";
        }

    },
    PRISM
    {
        @Override
        public int getValue()
        {
            return 6;
        }

        @Override
        public int getNVertices()
        {
            return 6;
        }

        @Override
        public int getDim()
        {
            return 3;
        }

        @Override
        public String getName()
        {
            return "prism";
        }

        @Override
        public String getUCDName()
        {
            return "prism";
        }

        @Override
        public String getPluralName()
        {
            return "prisms";
        }

    },
    HEXAHEDRON
    {
        @Override
        public int getValue()
        {
            return 7;
        }

        @Override
        public int getNVertices()
        {
            return 8;
        }

        @Override
        public int getDim()
        {
            return 3;
        }

        @Override
        public String getName()
        {
            return "hexahedron";
        }

        @Override
        public String getUCDName()
        {
            return "hex";
        }

        @Override
        public String getPluralName()
        {
            return "hexahedra";
        }

    },
    EXPLICIT
    {
        @Override
        public int getValue()
        {
            return 8;
        }

        @Override
        public int getDim()
        {
            return -1;
        }

        @Override
        public String getName()
        {
            return "explicit";
        }

        @Override
        public String getUCDName()
        {
            return "explicit";
        }

        @Override
        public String getPluralName()
        {
            return "explicit";
        }

    },
    CUSTOM
    {
        @Override
        public int getValue()
        {
            return 9;
        }

        @Override
        public int getDim()
        {
            return -1;
        }

        @Override
        public String getName()
        {
            return "custom";
        }

        @Override
        public String getUCDName()
        {
            return "custom";
        }

        @Override
        public String getPluralName()
        {
            return "custom";
        }

    };

    /**
     * Returns the value of the type.
     *
     * @return value of the type
     */
    public int getValue()
    {
        return -1;
    }

    /**
     * Returns the cell type corresponding to the given value.
     *
     * @param value cell value
     *
     * @return cell type corresponding to the given value
     */
    public static CellType getType(int value)
    {
        switch (value) 
        {
            case -1:
                return EMPTY;
            case 0:
                return POINT;
            case 1:
                return SEGMENT;
            case 2:
                return TRIANGLE;
            case 3:
                return QUAD;
            case 4:
                return TETRA;
            case 5:
                return PYRAMID;
            case 6:
                return PRISM;
            case 7:
                return HEXAHEDRON;
            case 8:
                return EXPLICIT;
            case 9:
                return CUSTOM;
            default:
                return EMPTY;
        }
    }

    /**
     * Returns the number of vertices of the cell corresponding to the cell type.
     *
     * @return number of vertices of the cell
     */
    public int getNVertices()
    {
        return 0;
    }

    /**
     * Returns the dimension of the cell corresponding to the cell type.
     *
     * @return dimension of the cell corresponding to the cell type
     */
    public int getDim()
    {
        return 0;
    }

    /**
     * Returns true if the cell corresponding to the cell type is simplex, false otherwise.
     *
     * @return true if the cell corresponding to the cell type is simplex, false otherwise
     */
    public boolean isSimplex()
    {
        return false;
    }

    /**
     * Returns the name of the cell corresponding to the cell type.
     *
     * @return the name of the cell corresponding to the cell type.
     */
    public String getName()
    {
        return "empty";
    }

    /**
     * Returns the UCD name of the cell corresponding to the cell type.
     *
     * @return the UCD name of the cell corresponding to the cell type.
     */
    public String getUCDName()
    {
        return "empty";
    }

    /**
     * Returns the plural name of the cell corresponding to the cell type.
     *
     * @return the plural name of the cell corresponding to the cell type.
     */
    public String getPluralName()
    {
        return "empty";
    }
}
