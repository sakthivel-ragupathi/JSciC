/* ***** BEGIN LICENSE BLOCK *****
 * Java Scientific Containers (JSciC)
 * Copyright (C) 2015 onward University of Warsaw, ICM
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * ***** END LICENSE BLOCK ***** */
package pl.edu.icm.jscic.cells;

/**
 *
 * Tetrahedron position. The class contains barycentric coordinates of a point
 * in a tetrahedron or trinagle, a cell containing this tetrahedron and
 * optionally a set of nearby cells. Used as a cache to find a point in a field.
 *
 * @author Krzysztof S. Nowinski
 *
 * University of Warsaw, ICM
 */
public class TetrahedronPosition {

    private int[] vertices;
    private float[] coords;
    private Cell simplex;
    private Cell cell;
    private int[] cells;

    /**
     * Creates a new instance of TetrahedronPosition.
     *
     * @param vertices vertices of the tetrahedron containing the point
     * @param coords barycentric coordinates of the point in the simplex
     * @param simplex simplex containing the point
     */
    public TetrahedronPosition(int[] vertices, float[] coords, Cell simplex) {
        this(vertices, coords, simplex, null, null);
    }

    /**
     * Creates a new instance of TetrahedronPosition.
     *
     * @param vertices vertices of the tetrahedron containing the point
     * @param coords barycentric coordinates of the point in the simplex
     * @param simplex simplex containing the point
     * @param cell cell containing the point
     * @param cells set of cells near the point
     */
    public TetrahedronPosition(int[] vertices, float[] coords, Cell simplex, Cell cell, int[] cells) {
        this.vertices = vertices;
        this.coords = coords;
        this.simplex = simplex;
        this.cell = cell;
        this.cells = cells;
    }

    /**
     * Returns the cell.
     *
     * @return cell
     */
    public Cell getCell() {
        return cell;
    }

    /**
     * Sets the cell.
     *
     * @param cell the cell to set
     */
    public void setCell(Cell cell) {
        this.cell = cell;
    }

    /**
     * Returns set of cells.
     *
     * @return set of cells
     */
    public int[] getCells() {
        return cells;
    }

    /**
     * Sets the cells.
     *
     * @param cells the cells to set
     */
    public void setCells(int[] cells) {
        this.cells = cells;
    }

    /**
     * Returns barycentric coordinates.
     *
     * @return barycentric coordinates
     */
    public float[] getCoords() {
        return coords;
    }

    /**
     * Sets barycentric coordinates.
     *
     * @param coords barycentric coordinates to set
     */
    public void setCoords(float[] coords) {
        this.coords = coords;
    }

    /**
     * Returns the simplex.
     *
     * @return the simplex
     */
    public Cell getSimplex() {
        return simplex;
    }

    /**
     * Sets the simplex.
     *
     * @param simplex the simplex to set
     */
    public void setSimplex(Cell simplex) {
        this.simplex = simplex;
    }

    /**
     * Returns the vertices.
     *
     * @return the vertices
     */
    public int[] getVertices() {
        return vertices;
    }

    /**
     * Sets the vertices.
     *
     * @param vertices the vertices to set
     */
    public void setVertices(int[] vertices) {
        this.vertices = vertices;
    }

    @Override
    public String toString() {
        return String.format("%5d %5.3f, %5d %5.3f, %5d %5.3f, %5d %5.3f",
                getVertices()[0], getCoords()[0],
                getVertices()[1], getCoords()[1],
                getVertices()[2], getCoords()[2],
                getVertices()[3], getCoords()[3]);
    }

}
