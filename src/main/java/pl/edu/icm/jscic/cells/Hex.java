/* ***** BEGIN LICENSE BLOCK *****
 * Java Scientific Containers (JSciC)
 * Copyright (C) 2015 onward University of Warsaw, ICM
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * ***** END LICENSE BLOCK ***** */
package pl.edu.icm.jscic.cells;

/**
 * Hexahedron cell.
 *
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 */
public class Hex extends Cell {

    /**
     * Constructor of a hexahedron.
     *
     * <pre>
     *  i4----------i7
     *  |\          |\
     *  |  \           \
     *  |    \      |    \
     *  |     i5----------i6
     *  |     |     |     |
     *  |     |           |
     *  i0- - | - - i3    |
     *   \    |      \    |
     *     \  |        \  |
     *       \|          \|
     *        i1----------i2
     *
     * Indices are ordered so that
     * 
     * i0&lt;(i1,i2,i3,i4,i5,i6,i7), i1&lt;i3&lt;i4
     * 
     * </pre>
     * 
     * @param nsp space dimension 
     * @param i0 node index
     * @param i1 node index
     * @param i2 node index
     * @param i3 node index
     * @param i4 node index
     * @param i5 node index
     * @param i6 node index
     * @param i7 node index
     * @param orientation significant if cell is of dim nspace or is a face of a
     * cell of dim nspace, true if vectors <code>i1-i0, i3-i0, i4-i0 </code>form
     * positively oriented reper
     */
    public Hex(int nsp, int i0, int i1, int i2, int i3, int i4, int i5, int i6, int i7, byte orientation) {
        type = CellType.HEXAHEDRON;
        nspace = nsp;
        vertices = new int[8];
        vertices[0] = i0;
        vertices[1] = i1;
        vertices[2] = i2;
        vertices[3] = i3;
        vertices[4] = i4;
        vertices[5] = i5;
        vertices[6] = i6;
        vertices[7] = i7;
        this.orientation = orientation;
        normalize();
    }

    /**
     * Constructor of a hexahedron.
     *
     *  <pre>
     *  i4----------i7
     *  |\          |\
     *  |  \           \
     *  |    \      |    \
     *  |     i5----------i6
     *  |     |     |     |
     *  |     |           |
     *  i0- - | - - i3    |
     *   \    |      \    |
     *     \  |        \  |
     *       \|          \|
     *        i1----------i2
     *
     * Indices are ordered so that
     * 
     * i0&lt;(i1,i2,i3,i4,i5,i6,i7), i1&lt;i3&lt;i4
     * 
     *</pre>
     * 
     * @param nsp space dimension
     * @param i0 node index
     * @param i1 node index
     * @param i2 node index
     * @param i3 node index
     * @param i4 node index
     * @param i5 node index
     * @param i6 node index
     * @param i7 node index
     */
    public Hex(int nsp, int i0, int i1, int i2, int i3, int i4, int i5, int i6, int i7) {
        this(nsp, i0, i1, i2, i3, i4, i5, i6, i7, (byte) 1);
    }

    @Override
    public Cell[][] subcells() {
        Cell[][] tr = new Cell[4][];
        tr[3] = new Hex[1];
        tr[3][0] = this;
        tr[2] = faces();
        tr[1] = new Segment[12];
        tr[1][0] = new Segment(nspace, vertices[0], vertices[1], orientation);
        tr[1][1] = new Segment(nspace, vertices[1], vertices[3], orientation);
        tr[1][2] = new Segment(nspace, vertices[3], vertices[2], orientation);
        tr[1][3] = new Segment(nspace, vertices[2], vertices[0], orientation);
        tr[1][4] = new Segment(nspace, vertices[4], vertices[5], orientation);
        tr[1][5] = new Segment(nspace, vertices[5], vertices[7], orientation);
        tr[1][6] = new Segment(nspace, vertices[7], vertices[6], orientation);
        tr[1][7] = new Segment(nspace, vertices[6], vertices[4], orientation);
        tr[1][8] = new Segment(nspace, vertices[0], vertices[4], orientation);
        tr[1][9] = new Segment(nspace, vertices[1], vertices[5], orientation);
        tr[1][10] = new Segment(nspace, vertices[2], vertices[6], orientation);
        tr[1][11] = new Segment(nspace, vertices[3], vertices[7], orientation);
        tr[0] = new Point[vertices.length];
        for (int i = 0; i < vertices.length; i++) {
            tr[0][i] = new Point(nspace, vertices[i]);
        }
        return tr;
    }

    @Override
    public Cell[] faces() {
        return faces(vertices, orientation);
    }

    @Override
    public Cell[] faces(int[] nodes, byte orientation) {
        Cell[] faces = {new Quad(nspace, nodes[0], nodes[1], nodes[2], nodes[3], (byte) (1 - orientation)),
            new Quad(nspace, nodes[4], nodes[5], nodes[6], nodes[7], orientation),
            new Quad(nspace, nodes[0], nodes[1], nodes[5], nodes[4], orientation),
            new Quad(nspace, nodes[1], nodes[2], nodes[6], nodes[5], orientation),
            new Quad(nspace, nodes[2], nodes[3], nodes[7], nodes[6], orientation),
            new Quad(nspace, nodes[0], nodes[3], nodes[7], nodes[4], (byte) (1 - orientation))};
        return faces;
    }

    @Override
    public Cell[] triangulation() {
        Tetra[] subdiv = new Tetra[6];
        int[][] s;
        int[] f0 = {vertices[1], vertices[2], vertices[6], vertices[5]};
        s = diagonalSubdiv(f0);
        subdiv[0] = new Tetra(nspace, vertices[0], s[0][0], s[0][1], s[0][2], orientation);
        subdiv[1] = new Tetra(nspace, vertices[0], s[1][0], s[1][1], s[1][2], orientation);
        int[] f1 = {vertices[2], vertices[3], vertices[7], vertices[6]};
        s = diagonalSubdiv(f1);
        subdiv[2] = new Tetra(nspace, vertices[0], s[0][0], s[0][1], s[0][2], orientation);
        subdiv[3] = new Tetra(nspace, vertices[0], s[1][0], s[1][1], s[1][2], orientation);
        int[] f2 = {vertices[4], vertices[5], vertices[6], vertices[7]};
        s = diagonalSubdiv(f2);
        subdiv[4] = new Tetra(nspace, vertices[0], s[0][0], s[0][1], s[0][2], orientation);
        subdiv[5] = new Tetra(nspace, vertices[0], s[1][0], s[1][1], s[1][2], orientation);
        return subdiv;
    }

    @Override
    public int[][] triangulationVertices() {
        return triangulationVertices(vertices);
    }

    /**
     * Generates the array of tetrahedra vertices from the array of hex
     * vertices.
     *
     * @param vertices array of vertices
     *
     * @return array of tetrahedra vertices
     */
    public static int[][] triangulationVertices(int[] vertices) {
        int[][] subdiv = new int[6][];
        int[][] s;
        s = diagonalSubdiv(new int[]{vertices[1], vertices[2], vertices[6], vertices[5]});
        subdiv[0] = new int[]{vertices[0], s[0][0], s[0][1], s[0][2]};
        subdiv[1] = new int[]{vertices[0], s[1][0], s[1][1], s[1][2]};
        s = diagonalSubdiv(new int[]{vertices[2], vertices[3], vertices[7], vertices[6]});
        subdiv[2] = new int[]{vertices[0], s[0][0], s[0][1], s[0][2]};
        subdiv[3] = new int[]{vertices[0], s[1][0], s[1][1], s[1][2]};
        s = diagonalSubdiv(new int[]{vertices[4], vertices[5], vertices[6], vertices[7]});
        subdiv[4] = new int[]{vertices[0], s[0][0], s[0][1], s[0][2]};
        subdiv[5] = new int[]{vertices[0], s[1][0], s[1][1], s[1][2]};
        return subdiv;
    }

    private static final int[][] oppQuads = new int[][]{{1, 2, 6, 5}, {2, 3, 7, 6}, {4, 5, 6, 7}};

    /**
     * Generates the array of triangles in boundary triangulation of the
     * hexahedron.
     *
     * @param vertices array of vertices
     * @return array of triangles in boundary triangulation
     */
    public static int[][] triangulationIndices(int[] vertices) {
        int[][] subdiv = new int[6][];
        int[][] s;
        for (int i = 0; i < 3; i++) {
            int[] oppQuad = oppQuads[i];
            int[] oppQuadVerts = new int[4];
            for (int j = 0; j < oppQuadVerts.length; j++) {
                oppQuadVerts[j] = vertices[oppQuad[j]];
            }
            s = diagonalSubdivIndices(oppQuadVerts);
            subdiv[2 * i] = new int[]{0, oppQuad[s[0][0]], oppQuad[s[0][1]], oppQuad[s[0][2]]};
            subdiv[2 * i + 1] = new int[]{0, oppQuad[s[1][0]], oppQuad[s[1][1]], oppQuad[s[1][2]]};
        }
        return subdiv;
    }

    @Override
    protected final int[] normalize() {
        if (!normalize(vertices)) {
            orientation = (byte) (1 - orientation);
        }
        return vertices;
    }

    /**
     * A convenience function that reorders array of vertices to increasing order (as
     * long as possible).
     *
     * @param vertices vertices
     *
     * @return true if orientation has been preserved, false otherwise
     */
    public static final boolean normalize(int[] vertices) {
        int i = 0, t = 0;
        for (int j = 1; j < 8; j++) {
            if (vertices[j] < vertices[i]) {
                i = j;
            }
        }
        switch (i) {
            case 0:
                break;
            case 1:
                t = vertices[0];
                vertices[0] = vertices[1];
                vertices[1] = vertices[2];
                vertices[2] = vertices[3];
                vertices[3] = t;
                t = vertices[4];
                vertices[4] = vertices[5];
                vertices[5] = vertices[6];
                vertices[6] = vertices[7];
                vertices[7] = t;
                break;
            case 2:
                t = vertices[0];
                vertices[0] = vertices[2];
                vertices[2] = t;
                t = vertices[1];
                vertices[1] = vertices[3];
                vertices[3] = t;
                t = vertices[4];
                vertices[4] = vertices[6];
                vertices[6] = t;
                t = vertices[5];
                vertices[5] = vertices[7];
                vertices[7] = t;
                break;
            case 3:
                t = vertices[0];
                vertices[0] = vertices[3];
                vertices[3] = vertices[2];
                vertices[2] = vertices[1];
                vertices[1] = t;
                t = vertices[4];
                vertices[4] = vertices[7];
                vertices[7] = vertices[6];
                vertices[6] = vertices[5];
                vertices[5] = t;
                break;
            case 4:
                t = vertices[0];
                vertices[0] = vertices[4];
                vertices[4] = t;
                t = vertices[1];
                vertices[1] = vertices[7];
                vertices[7] = t;
                t = vertices[2];
                vertices[2] = vertices[6];
                vertices[6] = t;
                t = vertices[3];
                vertices[3] = vertices[5];
                vertices[5] = t;
                break;
            case 5:
                t = vertices[0];
                vertices[0] = vertices[5];
                vertices[5] = t;
                t = vertices[3];
                vertices[3] = vertices[6];
                vertices[6] = t;
                t = vertices[1];
                vertices[1] = vertices[4];
                vertices[4] = t;
                t = vertices[2];
                vertices[2] = vertices[7];
                vertices[7] = t;
                break;
            case 6:
                t = vertices[0];
                vertices[0] = vertices[6];
                vertices[6] = t;
                t = vertices[3];
                vertices[3] = vertices[7];
                vertices[7] = t;
                t = vertices[1];
                vertices[1] = vertices[5];
                vertices[5] = t;
                t = vertices[2];
                vertices[2] = vertices[4];
                vertices[4] = t;
                break;
            case 7:
                t = vertices[0];
                vertices[0] = vertices[7];
                vertices[7] = t;
                t = vertices[3];
                vertices[3] = vertices[4];
                vertices[4] = t;
                t = vertices[1];
                vertices[1] = vertices[6];
                vertices[6] = t;
                t = vertices[2];
                vertices[2] = vertices[5];
                vertices[5] = t;
                break;
            default:
                throw new IllegalArgumentException("Invalid index " + i);
        }
        i = 1;
        for (int j = 2; j < 5; j++) {
            if (vertices[j] < vertices[i] && j != 2) {
                i = j;
            }
        }
        switch (i) {
            case 1:
                break;
            case 3:
                t = vertices[1];
                vertices[1] = vertices[3];
                vertices[3] = vertices[4];
                vertices[4] = t;
                t = vertices[2];
                vertices[2] = vertices[7];
                vertices[7] = vertices[5];
                vertices[5] = t;
                break;
            case 4:
                t = vertices[1];
                vertices[1] = vertices[4];
                vertices[4] = vertices[3];
                vertices[3] = t;
                t = vertices[2];
                vertices[2] = vertices[5];
                vertices[5] = vertices[7];
                vertices[7] = t;
                break;
            default:
                throw new IllegalArgumentException("Invalid index " + i);

        }
        if (vertices[3] > vertices[4]) {
            t = vertices[3];
            vertices[3] = vertices[4];
            vertices[4] = t;
            t = vertices[2];
            vertices[2] = vertices[5];
            vertices[5] = t;
            return false;
        }
        return true;
    }

    @Override
    public byte compare(int[] v) {
        if (v.length != 8) {
            return 0;
        }
        return compare(new Hex(this.nspace, v[0], v[1], v[2], v[3], v[4], v[5], v[6], v[7]));
    }

}
