/* ***** BEGIN LICENSE BLOCK *****
 * Java Scientific Containers (JSciC)
 * Copyright (C) 2015 onward University of Warsaw, ICM
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * ***** END LICENSE BLOCK ***** */
package pl.edu.icm.jscic;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import pl.edu.icm.jscic.cells.Cell;
import pl.edu.icm.jscic.cells.RegularHex;
import pl.edu.icm.jscic.cells.Tetra;
import pl.edu.icm.jscic.cells.TetrahedronPosition;
import pl.edu.icm.jscic.dataarrays.DataArray;
import pl.edu.icm.jscic.utils.MatrixMath;
import pl.edu.icm.jlargearrays.DoubleLargeArray;
import pl.edu.icm.jlargearrays.FloatLargeArray;
import pl.edu.icm.jlargearrays.LongLargeArray;
import static org.apache.commons.math3.util.FastMath.*;
import pl.edu.icm.jscic.cells.CellType;
import pl.edu.icm.jscic.dataarrays.DataArraySchema;
import pl.edu.icm.jscic.dataarrays.DataArrayType;
import pl.edu.icm.jscic.utils.ArrayUtils;
import pl.edu.icm.jlargearrays.ConcurrencyUtils;
import static pl.edu.icm.jscic.utils.CropDownUtils.downArray;
import pl.edu.icm.jscic.utils.EngineeringFormattingUtils;
import pl.edu.icm.jscic.utils.VectorMath;
import pl.edu.icm.jlargearrays.UnsignedByteLargeArray;
import pl.edu.icm.jlargearrays.IntLargeArray;
import pl.edu.icm.jlargearrays.LargeArray;
import pl.edu.icm.jlargearrays.ShortLargeArray;
import pl.edu.icm.jlargearrays.LargeArrayUtils;
import static pl.edu.icm.jscic.FieldSchema.COORD_NAMES;
import pl.edu.icm.jscic.utils.RegularFieldNeighbors;

/**
 * A data structure composed of 1D, 2D or 3D array of nodes.
 *
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 */
public class RegularField extends Field implements Serializable {

    private long[][] derivHistograms = null;
    private double[][] thrHistograms = null;
    private double[] avgGrad = null;
    private double[] stdDevGrad = null;

    private static final String[] INDEX_NAMES = {"__index i", "__index j", "__index k"};
   
    private static final long serialVersionUID = -6060827781890957096L;
    private static final int MAXCELLDIM = 100;
    protected int[] dims = null;
    protected long[] lDims = null;
    /**
     * An array (4x3) containing 4 columns - elementary cell vectors v0, v1, v2
     * and the origin point.
     * <p>
     * It is used to convert: set of indexes to point coordinates:<br>
     * <code>i,j,k</code> node from data array has coordinates:<br>
     * <code>o + v0*i + v1*j + v2*k</code> in local coordinates (o - origin).
     * <pre>
     * +-              -+   +- -+   +-                          -+
     * |            |   |   | i |   | i*v0x + j*v1x + k*v2x + ox |
     * | v0  v1  v2 | o | . | j | = | i*v0y + j*v1y + k*v2y + oy | = i*v0 + j*v1 + k*v2 + o
     * |            |   |   | k |   | i*v0z + j*v1z + k*v2z + oz |
     * +-              -+   | 1 |   +-                          -+
     *                      +- -+
     * </pre>
     * <pre>
     * affine[i] = v_i (i-th column)
     * affine[3] = origin</pre>
     * <p>
     * NOTE: Used when coords == null.
     *
     * @see Field#coords
     */
    protected float[][] affine = new float[4][3];
    /**
     * Inversion of v0, v1, v2 array (3x3 matrix) statisticsComputed
     * automatically when affine is set (invAffine * affine = I). Used to
     * convert: point coordinates to set of indexes (float values)
     * <pre>
     *                                     +- -+
     *                                     | i |
     *      invAffine * (point - origin) = | j |,
     *                                     | k |
     *                                     +- -+
     * </pre> <code>i, j, k</code> - indexes, float values (for interpolation)
     * <p>
     * <code>invAffine[i]</code> - i-th column
     */
    protected float[][] invAffine = new float[3][3];
    protected float[][] rectilinarCoords = new float[3][];
    protected FloatLargeArray coordsFromAffine = null;
    protected int[] cellExtentsDown = new int[3];
    protected int[] cellExtentsDims = new int[3];
    protected int[] cellNodeOffsets;
    protected int[] fullNeighbOffsets;
    protected int[] partNeighbOffsets;
    protected int[] strictNeighbOffsets;

    /**
     * Creates a new instance of RegularField
     *
     * @param dims field dimension (dims.length can be 1,2 or 3)
     */
    public RegularField(int[] dims) {
        if (dims == null) {
            throw new IllegalArgumentException("dims cannot be null");
        }
        if (dims.length < 1 || dims.length > 3) {
            throw new IllegalArgumentException("Only 1D, 2D and 3D regular fields are supported");
        }
        for (int i = 0; i < dims.length; i++) {
            if (dims[i] <= 1) {
                throw new IllegalArgumentException("All values in array dims must be greater than 1");
            }
        }
        type = FieldType.FIELD_REGULAR;
        schema = new RegularFieldSchema();
        for (int i = 0; i < dims.length; i++) {
            schema.addPseudoComponentSchema(new DataArraySchema(INDEX_NAMES[i], "", null,
                    DataArrayType.FIELD_DATA_INT, 1, 1, false,
                    0, dims[i], 0, dims[i], 0, dims[i],
                    dims[i] / 2., dims[i] / 2., dims[i] / 2., false));
        }
        setDims(dims);
        setExtents(((RegularFieldSchema) schema).getExtents(), true);
        timestamp = System.nanoTime();
    }

    /**
     * Creates a new instance of RegularField
     *
     * @param dims field dimension (dims.length can be 1,2 or 3)
     */
    public RegularField(long[] dims) {
        if (dims == null) {
            throw new IllegalArgumentException("dims cannot be null");
        }
        for (int i = 0; i < dims.length; i++) {
            if (dims[i] <= 1) {
                throw new IllegalArgumentException("All values in array dims must be greater than 1");
            }
        }
        type = FieldType.FIELD_REGULAR;
        schema = new RegularFieldSchema();
        for (int i = 0; i < dims.length; i++) {
            schema.addPseudoComponentSchema(new DataArraySchema(INDEX_NAMES[i], "", null,
                    DataArrayType.FIELD_DATA_INT, 1, 1, false,
                    0, dims[i], 0, dims[i], 0, dims[i],
                    dims[i] / 2., dims[i] / 2., dims[i] / 2., false));
        }
        setDims(dims);
        setExtents(((RegularFieldSchema) schema).getExtents(), true);
        timestamp = System.nanoTime();
    }

    /**
     * Creates a new instance of RegularField
     *
     * @param dims field dimension (dims.length can be 1,2 or 3)
     * @param extents field extents
     */
    public RegularField(int[] dims, float[][] extents) {
        if (dims == null) {
            throw new IllegalArgumentException("dims cannot be null");
        }
        for (int i = 0; i < dims.length; i++) {
            if (dims[i] <= 1) {
                throw new IllegalArgumentException("All values in array dims must be greater than 1");
            }
        }
        type = FieldType.FIELD_REGULAR;
        schema = new RegularFieldSchema();
        for (int i = 0; i < dims.length; i++) {
            schema.addPseudoComponentSchema(new DataArraySchema(INDEX_NAMES[i], "", null,
                    DataArrayType.FIELD_DATA_INT, 1, 1, false,
                    0, dims[i], 0, dims[i], 0, dims[i],
                    dims[i] / 2., dims[i] / 2., dims[i] / 2., false));
        }
        for (int i = 0; i < dims.length; i++) {
            schema.addPseudoComponentSchema(new DataArraySchema(COORD_NAMES[i], "", null,
                    DataArrayType.FIELD_DATA_FLOAT, 1, 1, false,
                    extents[0][i], extents[1][i],
                    extents[0][i], extents[1][i],
                    extents[0][i], extents[1][i],
                    (extents[0][i] + extents[1][i]) / 2.,
                    (extents[0][i] + extents[1][i]) / 2.,
                    (extents[0][i] + extents[1][i]) / 2., false));
        }
        setDims(dims);
        super.setExtents(extents);
        nElements = 1;
        setNSpace(extents[0].length);
        affineFromExtents();
        computeInvAffine();
        timestamp = System.nanoTime();
    }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();
        if (isLarge()) {
            s.append("Large ");
        }
        s.append("Regular Field ").append(dims.length).append("D  ").
                append(getNSpace()).append("-space, ");
        if (getNFrames() > 1) {
            s.append(getNFrames()).append(" time frames, ");
        }
        s.append("dimensions = {").append(dims[0]);
        if (dims.length > 1) {
            s.append("x").append(dims[1]);
        }
        if (dims.length > 2) {
            s.append("x").append(dims[2]);
        }
        s.append("}, ").append(components.size()).append(" data components");
        return s.toString();
    }

    @Override
    public String shortDescription() {
        StringBuilder s = new StringBuilder();
        s.append("<html>");
        for (int i = 0; i < dims.length; i++) {
            if (i > 0) {
                s.append("x").append(dims[i]);
            } else {
                s.append(dims[i]);
            }
        }
        if (coords != null) {
            s.append("<br>coords");
        }
        if (getNFrames() > 1) {
            s.append("<br>").append(getNFrames()).append(" timesteps");
        }
        s.append("<br>").append(getNComponents()).append(" components");
        s.append("</html>");
        return s.toString();
    }

    @Override
    public String toMultilineString() {
        StringBuilder s = new StringBuilder();
        s.append("<html>");
        if (isLarge()) {
            s.append("Large ");
        }
        s.append("Regular Field ").append(dims.length).append("D  ");
        s.append(getNSpace()).append("-space,<br>");
        if (getNFrames() > 1) {
            s.append(getNFrames()).append(" time frames <br>");
        }
        s.append("dimensions = {").append(dims[0]);
        if (dims.length > 1) {
            s.append("x").append(dims[1]);
        }
        if (dims.length > 2) {
            s.append("x").append(dims[2]);
        }
        return s.toString() + "}</html>";
    }

    @Override
    public String description() {
        return description(false);
    }

    @Override
    public String description(boolean debug) {
        StringBuffer s = new StringBuffer();
        s.append("<p>Field: ").append(schema.getName()).append("</p>");
        if (isLarge()) {
            s.append("Large ");
        }
        s.append("Regular ").append(dims.length).append("D  ");
        s.append(getNSpace()).append("-space, ");
        if (trueNSpace > 0) {
            s.append("true ").append(dims.length).append("-dim ");
        }
        if (getNFrames() > 1) {
            s.append(getNFrames()).append(" timesteps<p>");
            s.append("time range ");
            s.append(EngineeringFormattingUtils.formatHtml(getStartTime()));
            s.append(timeUnit);
            s.append(":");
            s.append(EngineeringFormattingUtils.formatHtml(getEndTime()));
            s.append(timeUnit);
            s.append("</p><p>current time: ");
            s.append(EngineeringFormattingUtils.formatHtml(getCurrentTime()));
            s.append("").append(timeUnit).append("</p>");
        }
        if (schema.getUserData() != null) {
            s.append("<p>user data:");
            for (String u : schema.getUserData()) {
                s.append("<p>").append(u);
            }
        }
        s.append("<p>Dimensions: ").append(Arrays.toString(dims).replaceAll(", ", " x ").replaceAll("[\\[\\]]", ""));

        s.append("<p>Geometric extents:</p>");
        s.append(asXYZTable(getExtents(), getNSpace(), 2, true));

        s.append("<p>Physical extents:</p>");
        s.append(asXYZTable(getPhysExtents(), getNSpace(), 2, true));

        if (timeCoords != null && !timeCoords.isEmpty()) {
            s.append("<p>Explicit coordinates</p>");
        } else {
            s.append("<p>Affine ");
            if (isAffineOrthonormalXYZ(true, false)) {
                s.append("XYZ orthonormal ");
            } else if (isAffineXYZ(false)) {
                s.append("XYZ orthogonal ");
            } else if (isAffineOrthogonal()) {
                s.append("orthogonal ");
            }
            s.append("geometry:</p>");

            s.append("<p>&nbsp;- origin at (");
            String[] affineString = EngineeringFormattingUtils.formatInContextHtml(affine[3]);
            s.append(String.format("%s, %s, %s", affineString[0], affineString[1], affineString[2]));

            s.append(")<p>&nbsp;- cell vectors:</p>");
            s.append(asTable(affine, new String[]{"v0", "v1", "v2"}, getDimNum(), getNSpace(), false));
        }
        if (timeMask != null && !timeMask.isEmpty()) {
            s.append("<p>with mask</p>");
        }
        s.append("<TABLE border=\"0\" cellspacing=\"5\">");
        if (debug) {
            s.append("<TR valign='top' align='right'><TD align = 'left'>Name</td><TD>Vector<br/>length</td><td>Type</td><td>Time<br/>steps</td><td>Min</td><td>Max</td><td>Physical<br/>min</td><td>Physical<br/>max</td><td>Preferred<br/>min</td><td>Preferred<br/>max</td><td>Preferred<br/>physical<br/>min</td><td>Preferred<br/>physical<br/>max</td></tr>");
        } else {
            s.append("<TR valign='top' align='right'><TD align = 'left'>Name</td><TD>Vector<br/>length</td><td>Type</td><td>Time<br/>steps</td><td>Min</td><td>Max</td><td>Physical<br/>min</td><td>Physical<br/>max</td></tr>");
        }
        for (int i = 0; i < components.size(); i++) {
            s.append(getComponent(i).description(debug));
        }
        s.append("</TABLE>");
        return "<html>" + s + "</html>";
    }

    @Override
    public RegularField cloneShallow() {
        RegularField clone = new RegularField(this.dims, ArrayUtils.cloneDeep(getExtents()));
        clone.type = type;
        clone.setSchema(this.schema.cloneDeep());
        clone.setPhysExtents(ArrayUtils.cloneDeep(getPhysExtents()), false);
        clone.setAffine(ArrayUtils.cloneDeep(this.affine));
        clone.invAffine = ArrayUtils.cloneDeep(this.invAffine);
        clone.geoTree = this.geoTree != null ? this.geoTree.cloneDeep() : null;
        clone.derivHistograms = ArrayUtils.cloneDeep(this.derivHistograms);
        clone.thrHistograms = ArrayUtils.cloneDeep(this.thrHistograms);
        clone.avgGrad = this.avgGrad != null ? this.avgGrad.clone() : null;
        clone.stdDevGrad = this.stdDevGrad != null ? this.stdDevGrad.clone() : null;
        clone.rectilinarCoords = this.rectilinarCoords;    
        clone.cellExtents = ArrayUtils.cloneDeep(this.cellExtents);
        clone.cellExtentsDown = this.cellExtentsDown != null ? this.cellExtentsDown.clone() : null;
        clone.cellExtentsDims = this.cellExtentsDims != null ? this.cellExtentsDims.clone() : null;
        clone.cellNodeOffsets = this.cellNodeOffsets != null ? this.cellNodeOffsets.clone() : null;
        clone.fullNeighbOffsets = this.fullNeighbOffsets != null ? this.fullNeighbOffsets.clone() : null;
        clone.partNeighbOffsets = this.partNeighbOffsets != null ? this.partNeighbOffsets.clone() : null;
        clone.strictNeighbOffsets = this.strictNeighbOffsets != null ? this.strictNeighbOffsets.clone() : null;
        clone.timeUnit = this.timeUnit;
        clone.axesNames = this.axesNames != null ? this.axesNames.clone() : null;
        clone.statisticsComputed = this.statisticsComputed;
        clone.trueNSpace = this.trueNSpace;
        clone.nElements = nElements;
        clone.normals = this.normals;

        clone.coordsFromAffine = this.coordsFromAffine;
        if (this.timeCoords != null && !timeCoords.isEmpty()) {
            clone.setCoords(this.timeCoords.cloneShallow());
        }
        if (this.timeMask != null && !timeMask.isEmpty()) {
            clone.setMask(this.timeMask.cloneShallow());
        }

        if (components != null && !components.isEmpty()) {
            ArrayList<DataArray> componentsClone = new ArrayList<>();
            for (DataArray dataArray : components) {
                componentsClone.add(dataArray.cloneShallow());
            }
            clone.components = componentsClone;
        }
        clone.coordsTimestamp = this.coordsTimestamp;
        clone.maskTimestamp = this.maskTimestamp;
        clone.timestamp = this.timestamp;
        return clone;
    }

    @Override
    public RegularField cloneDeep() {
        RegularField clone = new RegularField(this.dims, ArrayUtils.cloneDeep(getExtents()));
        clone.type = type;
        clone.setSchema(this.schema.cloneDeep());
        clone.setPhysExtents(ArrayUtils.cloneDeep(getPhysExtents()), false);
        clone.setAffine(ArrayUtils.cloneDeep(this.affine));
        clone.invAffine = ArrayUtils.cloneDeep(this.invAffine);
        clone.geoTree = this.geoTree != null ? this.geoTree.cloneDeep() : null;
        clone.derivHistograms = ArrayUtils.cloneDeep(this.derivHistograms);
        clone.thrHistograms = ArrayUtils.cloneDeep(this.thrHistograms);
        clone.avgGrad = this.avgGrad != null ? this.avgGrad.clone() : null;
        clone.stdDevGrad = this.stdDevGrad != null ? this.stdDevGrad.clone() : null;
        clone.rectilinarCoords = ArrayUtils.cloneDeep(this.rectilinarCoords);
        clone.cellExtents = ArrayUtils.cloneDeep(this.cellExtents);
        clone.cellExtentsDown = this.cellExtentsDown != null ? this.cellExtentsDown.clone() : null;
        clone.cellExtentsDims = this.cellExtentsDims != null ? this.cellExtentsDims.clone() : null;
        clone.cellNodeOffsets = this.cellNodeOffsets != null ? this.cellNodeOffsets.clone() : null;
        clone.fullNeighbOffsets = this.fullNeighbOffsets != null ? this.fullNeighbOffsets.clone() : null;
        clone.partNeighbOffsets = this.partNeighbOffsets != null ? this.partNeighbOffsets.clone() : null;
        clone.strictNeighbOffsets = this.strictNeighbOffsets != null ? this.strictNeighbOffsets.clone() : null;
        clone.timeUnit = this.timeUnit;
        clone.axesNames = this.axesNames != null ? this.axesNames.clone() : null;
        clone.statisticsComputed = this.statisticsComputed;
        clone.trueNSpace = this.trueNSpace;
        clone.nElements = nElements;
        clone.normals = this.normals != null ? this.normals.clone() : null;

        clone.coordsFromAffine = this.coordsFromAffine != null ? this.coordsFromAffine.clone() : null;
        if (this.timeCoords != null && !timeCoords.isEmpty()) {
            clone.setCoords(this.timeCoords.cloneDeep());
        }
        if (this.timeMask != null && !timeMask.isEmpty()) {
            clone.setMask(this.timeMask.cloneDeep());
        }

        if (components != null && !components.isEmpty()) {
            ArrayList<DataArray> componentsClone = new ArrayList<>();
            for (DataArray dataArray : components) {
                componentsClone.add(dataArray.cloneDeep());
            }
            clone.components = componentsClone;
        }
        clone.coordsTimestamp = this.coordsTimestamp;
        clone.maskTimestamp = this.maskTimestamp;
        clone.timestamp = this.timestamp;
        return clone;
    }

    @Override
    public RegularFieldSchema getSchema() {
        return (RegularFieldSchema) schema;
    }

    private void affineFromExtents() {
        float[][] extents = getExtents();
        for (int i = 0; i < extents[0].length; i++) {
            affine[3][i] = extents[0][i];
            for (int j = 0; j < 3; j++) {
                affine[j][i] = 0;
            }
            if (i < dims.length && dims[i] > 1) {
                affine[i][i] = (extents[1][i] - extents[0][i]) / (dims[i] - 1);
            }
        }
        for (int i = extents[0].length; i < 3; i++) {
            for (int j = 0; j < 4; j++) {
                affine[j][i] = 0;
            }
            affine[i][i] = 1;
        }
        affineVsNspace();
        computeInvAffine();
        timestamp = System.nanoTime();
    }

    /**
     * Returns offsets of all neighbor vertices (26 in 3D case, 8 in 2D case and 2 in 1D case).
     *
     * @return offsets of all neighbor vertices
     */
    public int[] getFullNeighbOffsets() {
        return fullNeighbOffsets;
    }

    /**
     * Returns offsets of neighbor vertices (18 in 3D case, 4 in 2D case and 2 in 1D case)
     *
     * @return neighbor vertices
     */
    public int[] getPartNeighbOffsets() {
        return partNeighbOffsets;
    }

    /**
     * Returns offsets of neighbor vertices (6 in 3D case, 4 in 2D case and 2 in 1D case)
     *
     * @return neighbor vertices
     */
    public int[] getStrictNeighbOffsets() {
        return strictNeighbOffsets;
    }

    /**
     * Returns offsets of vertices of the cell with respect to its lowest vertex.
     *
     * @return offsets of vertices
     */
    public int[] getCellNodeOffsets() {
        return cellNodeOffsets;
    }

    /**
     * Returns matrix of size dim + 1 x dim: each spanning vector in one row and
     * translation vector in last row.
     *
     * @return affine vectors
     */
    @SuppressWarnings("ReturnOfCollectionOrArrayField")
    public float[][] getAffine() {
        return affine;
    }

    /**
     * Calculates and returns norm of affine vectors. These values are not
     * cached, so they are calculated in every call to this method.
     *
     * @return norm of affine vectors
     */
    public double[] getAffineNorm() {
        int dim = affine.length - 1; //assuming that affine.length - 1 and affine[0..n].length are equal

        double[] norm = new double[dim];

        for (int i = 0; i < dim; i++) {
            double sum = 0;
            for (int j = 0; j < dim; j++) {
                sum += affine[i][j] * affine[i][j];
            }
            norm[i] = sqrt(sum);
        }

        return norm;
    }

    /**
     * Returns the inverse of affine vectors
     *
     * @return inverse of affine vectors
     */
    public float[][] getInvAffine() {
        return invAffine;
    }

    /**
     * Sets affine vectors
     *
     * @param affine new affine vectors
     */
    public void setAffine(float[][] affine) {
        if (affine == null || affine.length != 4
                || affine[0].length != 3 || affine[1].length != 3 || affine[2].length != 3 || affine[3].length != 3) {
            throw new IllegalArgumentException("ffine == null || affine.length != 4 ||\n"
                    + "            affine[0].length != 3 || affine[1].length != 3 || affine[2].length != 3 || affine[3].length != 3");
        }
        this.affine = affine;

        recomputeExtents();
        affineVsNspace();
        computeInvAffine();
        timestamp = System.nanoTime();
    }

    /**
     * Checks if regular field geometry is affine
     *
     * @return - true if field geometry is affine, false otherwise
     */
    public boolean isAffine() {
        return (coords == null);
    }

    /**
     * Checks if regular field geometry is orthonormal (perpendicular affine
     * vectors with equal length and requested or not unit length)
     *
     * @param requestUnitLength - sets if checing only perpendicular and equal
     * affine vectors or also unit length (if set to true, unit length is
     * checked)
     *
     * @return - true if field geometry is orthonormal, false otherwise
     */
    public boolean isAffineOrthonormal(boolean requestUnitLength) {
        if (!isAffineOrthogonal()) {
            return false;
        }

        double[] lls = new double[dims.length];
        for (int i = 0; i < dims.length; i++) {
            lls[i] = (double) affine[i][0] * (double) affine[i][0] + (double) affine[i][1] * (double) affine[i][1] + (double) affine[i][2] * (double) affine[i][2];
        }
        double ll = lls[0];
        for (int i = 1; i < lls.length; i++) {
            if (lls[i] != ll) {
                return false;
            }
        }

        if (requestUnitLength) {
            for (int i = 0; i < lls.length; i++) {
                if (lls[i] != 1.0) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Checks if regular field geometry is orthogonal (perpendicular affine
     * vectors)
     *
     * @return - true if field geometry is orthogonal, false otherwise
     */
    public boolean isAffineOrthogonal() {
        if (!isAffine()) {
            return false;
        }
        double eps = 0.000001;
        double a01, a02, a12;
        //check if angles are == PI/2

        switch (dims.length) {
            case 3:
                a01 = VectorMath.vectorAngle(affine[0], affine[1], false);
                if (Math.abs(a01 - Math.PI / 2.0) > eps) {
                    return false;
                }
                a02 = VectorMath.vectorAngle(affine[0], affine[2], false);
                if (Math.abs(a02 - Math.PI / 2.0) > eps) {
                    return false;
                }
                a12 = VectorMath.vectorAngle(affine[1], affine[2], false);
                if (Math.abs(a12 - Math.PI / 2.0) > eps) {
                    return false;
                }
                return true;
            case 2:
                a01 = VectorMath.vectorAngle(affine[0], affine[1], false);
                if (Math.abs(a01 - Math.PI / 2.0) > eps) {
                    return false;
                }
                return true;
            case 1:
                return true;
        }
        return false;
    }

    /**
     * Checks if affine vectors are aligned with main XYZ directions
     *
     * @param ordered - sets if the order of vectors is important (v0 = ex, v1 =
     * ey, v2 = ez)
     *
     * @return - true if field geometry is aligned with XYZ directions, false
     * otherwise
     */
    public boolean isAffineXYZ(boolean ordered) {
        if (!isAffineOrthogonal()) {
            return false;
        }

        if (ordered) {
            switch (dims.length) {
                case 3:
                    return (affine[0][0] != 0 && affine[0][1] == 0 && affine[0][2] == 0
                            && affine[1][0] == 0 && affine[1][1] != 0 && affine[1][2] == 0
                            && affine[2][0] == 0 && affine[2][1] == 0 && affine[2][2] != 0);
                case 2:
                    return (affine[0][0] != 0 && affine[0][1] == 0 && affine[0][2] == 0
                            && affine[1][0] == 0 && affine[1][1] != 0 && affine[1][2] == 0);
                case 1:
                    return (affine[0][0] != 0 && affine[0][1] == 0 && affine[0][2] == 0);
            }
        } else {
            boolean ok = true;
            for (int i = 0; i < dims.length; i++) {
                ok = ok & (affine[i][0] != 0 && affine[i][1] == 0 && affine[i][2] == 0
                        || affine[i][0] == 0 && affine[i][1] != 0 && affine[i][2] == 0
                        || affine[i][0] == 0 && affine[i][1] == 0 && affine[i][2] != 0);
                if (!ok) {
                    break;
                }
            }
            return ok;
        }
        return false;
    }

    /**
     * Checks if regular field geometry is orthonormal (perpendicular affine
     * vectors with equal length and requested or not unit length) and affine
     * vectors aligned with XYZ directions
     *
     * @param requestUnitLength - sets if checing only perpendicular and equal
     * affine vectors or also unit length (if set to true, unit length is
     * checked)
     * @param ordered - sets if the order of vectors is important (v0 = ex, v1 =
     * ey, v2 = ez)
     *
     * @return - true if field geometry is orthonormal and aligned with XYZ
     * directions, false otherwise
     */
    public boolean isAffineOrthonormalXYZ(boolean requestUnitLength, boolean ordered) {
        return (isAffineXYZ(ordered) && isAffineOrthonormal(requestUnitLength));
    }

    /**
     * Computes extents from affine.
     */
    private void recomputeExtents() {
        float t;
        float[][] extents = new float[2][3];
        for (int i = 0; i < extents[0].length; i++) {
            extents[0][i] = extents[1][i] = affine[3][i];
            for (int x = 0; x < 2; x++) {
                for (int y = 0; y < 2; y++) {
                    for (int z = 0; z < 2; z++) {
                        t = affine[3][i] + x * (dims[0] - 1) * affine[0][i];
                        if (dims.length > 1) {
                            t += y * (dims[1] - 1) * affine[1][i];
                        }
                        if (dims.length > 2) {
                            t += z * (dims[2] - 1) * affine[2][i];
                        }

                        if (t < extents[0][i]) {
                            extents[0][i] = t;
                        }
                        if (t > extents[1][i]) {
                            extents[1][i] = t;
                        }
                    }
                }
            }
        }
        super.setExtents(extents);
        physExtentsFromExtents();
        timestamp = System.nanoTime();
    }

    /**
     * Sets the origin point.
     *
     * @param origin new origin point
     */
    public void setOrigin(float[] origin) {
        if (origin == null || origin.length != 3) {
            throw new IllegalArgumentException("origin must be float[3]");
        }
        System.arraycopy(origin, 0, affine[3], 0, 3);
        setAffine(affine);
        recomputeExtents();
        timestamp = System.nanoTime();
    }

    private void computeInvAffine() {
        float[][] a = new float[3][3];
        for (int i = 0; i < a.length; i++) {
            System.arraycopy(affine[i], 0, a[i], 0, a[i].length);
        }
        MatrixMath.invert(a, invAffine);
        coordsTimestamp = System.nanoTime();
        timestamp = System.nanoTime();
    }

    @Override
    public void setNSpace(int nSpace) {
        super.setNSpace(nSpace);
        affineVsNspace();
        timestamp = System.nanoTime();
    }

    private void affineVsNspace() {
        for (int i = 0; i < 4; i++) {
            for (int j = getNSpace(); j < 3; j++) {
                affine[i][j] = 0;
            }
        }
    }

    /**
     * Returns dimensions of regular field
     *
     * @return dimensions of regular field
     */
    public int[] getDims() {
        return this.dims;
    }

    /**
     * Returns dimensions of regular field
     *
     * @return dimensions of regular field
     */
    public long[] getLDims() {
        return lDims;
    }

    /**
     * Returns number of dimensions.
     *
     * @return number of dimensions.
     */
    public int getDimNum() {
        return dims.length;
    }

    /**
     * When field is of affine type then return cell volume (in respective
     * dimension):
     * <ul>
     * <li>1D - length of span vector (which is just absolute value of one
     * number anyway)
     * <li>2D - area of parallelogram spanned by spanning vectors
     * <li>3D - volume of parallelepiped spanned by spanning vector.
     * </ul>
     * If field is coords-like then throws IllegalStateException.
     *
     * @return cell volume (in respective dimension)
     *
     * @throws IllegalStateException if field is of coords-type
     */
    public double getCellVolume() {
        if (getCurrentCoords() != null) {
            throw new IllegalStateException("Cell volume is not defined for coords-like field");
        }
        switch (dims.length) {
            case 1:
                return abs(affine[0][0]);
            case 2:
                return abs(affine[0][0] * affine[1][1] - affine[0][1] * affine[1][0]);
            case 3:
                return abs(affine[0][0] * affine[1][1] * affine[2][2]
                        + affine[0][1] * affine[1][2] * affine[2][0]
                        + affine[0][2] * affine[1][0] * affine[2][1]
                        - affine[0][0] * affine[1][2] * affine[2][1]
                        - affine[0][1] * affine[1][0] * affine[2][2]
                        - affine[0][2] * affine[1][1] * affine[2][0]);
        }
        return 0;
    }

    private void setDims(int[] dims) {
        this.dims = dims;
        lDims = new long[dims.length];
        nElements = 1;
        for (int i = 0; i < dims.length; i++) {
            lDims[i] = dims[i];
            nElements *= dims[i];
        }
        setNSpace(dims.length);
        float[][] extents = new float[2][3];
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 3; j++) {
                extents[i][j] = 0.0f;
            }
        }

        for (int i = 0; i < dims.length; i++) {
            extents[1][i] = dims[i] - 1.f;
        }
        for (int i = dims.length; i < 3; i++) {
            extents[1][i] = 0.0f;
        }
        ((RegularFieldSchema) schema).setNDims(dims.length);
        super.setExtents(extents);
        affineFromExtents();
        int[][] neighbors = RegularFieldNeighbors.neighbors(dims);
        fullNeighbOffsets = neighbors[0];
        partNeighbOffsets = neighbors[1];
        strictNeighbOffsets = neighbors[2];
        switch (dims.length) {
            case 3:
                cellNodeOffsets = new int[]{0, 1,
                    dims[0] + 1, dims[0],
                    dims[0] * dims[1], dims[0] * dims[1] + 1,
                    dims[0] * dims[1] + dims[0] + 1, dims[0] * dims[1] + dims[0]};
                break;
            case 2:
                cellNodeOffsets = new int[]{0, 1, dims[0] + 1, dims[0]};
                break;
            case 1:
                cellNodeOffsets = new int[]{0, 1};
                break;
        }
        timestamp = System.nanoTime();
    }

    private void setDims(long[] dims) {
        this.lDims = dims;
        this.dims = new int[dims.length];
        nElements = 1;
        for (int i = 0; i < dims.length; i++) {
            this.dims[i] = (int) dims[i];
            nElements *= dims[i];
        }
        setNSpace(dims.length);
        float[][] extents = new float[2][3];
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 3; j++) {
                extents[i][j] = 0.0f;
            }
        }

        for (int i = 0; i < dims.length; i++) {
            extents[1][i] = dims[i] - 1.f;
        }
        for (int i = dims.length; i < 3; i++) {
            extents[1][i] = 0.0f;
        }
        ((RegularFieldSchema) schema).setNDims(dims.length);
        super.setExtents(extents);
        affineFromExtents();
        int[][] neighbors = RegularFieldNeighbors.neighbors(this.dims);
        fullNeighbOffsets = neighbors[0];
        partNeighbOffsets = neighbors[1];
        strictNeighbOffsets = neighbors[2];
        switch (dims.length) {
            case 3:
                cellNodeOffsets = new int[]{0, 1,
                    this.dims[0] + 1, this.dims[0],
                    this.dims[0] * this.dims[1], this.dims[0] * this.dims[1] + 1,
                    this.dims[0] * this.dims[1] + this.dims[0] + 1, this.dims[0] * this.dims[1] + this.dims[0]};
                break;
            case 2:
                cellNodeOffsets = new int[]{0, 1,
                    this.dims[0] + 1, this.dims[0]};
                break;
            case 1:
                cellNodeOffsets = new int[]{0, 1};
                break;
        }
        timestamp = System.nanoTime();
    }

    /**
     * Sets field extents.
     *
     * @param extents new value of extents.
     * @param recomputeAffine if true, affine matrix is set to affine grid for
     * rectangular mesh of new extents
     */
    public final void setExtents(float[][] extents, boolean recomputeAffine) {
        super.setExtents(extents);
        float[][] physExtents = new float[2][3];
        for (int i = 0; i < 2; i++) {
            System.arraycopy(extents[i], 0, physExtents[i], 0, extents[0].length);
        }
        super.setPhysExtents(physExtents, false);
        if (recomputeAffine) {
            affineFromExtents();
        }
        timestamp = System.nanoTime();
    }

    /**
     * Sets field extents.
     *
     * @param extents new value of extents.
     */
    @Override
    public void setExtents(float[][] extents) {
        setExtents(extents, true);
    }

    /**
     * Rescales the coordinates of a uniform regular field. 
     *
     * @param scale relative dimensions of the field
     */
    public void setScale(float[] scale) {
        float[][] extents = new float[2][3];
        if (dims.length == 2) {
            int nn = dims[0];
            if (dims[1] > nn) {
                nn = dims[1];
            }
            extents[1][0] = scale[0] * (dims[0] - 1.f) / (2.f * nn);
            extents[1][1] = scale[1] * (dims[1] - 1.f) / (2.f * nn);
            extents[1][2] = .5f;
            extents[0][0] = -extents[1][0];
            extents[0][1] = -extents[1][1];
            extents[0][2] = -extents[1][2];

        } else if (dims.length == 3) {
            int nn = dims[0];
            if (dims[1] > nn) {
                nn = dims[1];
            }
            if (dims[2] > nn) {
                nn = dims[2];
            }
            extents[1][0] = scale[0] * (dims[0] - 1.f) / (2.f * nn);
            extents[1][1] = scale[1] * (dims[1] - 1.f) / (2.f * nn);
            extents[1][2] = scale[2] * (dims[2] - 1.f) / (2.f * nn);
            extents[0][0] = -extents[1][0];
            extents[0][1] = -extents[1][1];
            extents[0][2] = -extents[1][2];
        }
        super.setExtents(extents);
        affineFromExtents();
        timestamp = System.nanoTime();
    }

    /**
     * Sets rectilinear coordinates.
     *
     * @param coord index
     * @param c coordinates
     */
    public void setRectilinearCoords(int coord, float[] c) {
        if (c == null || coord < 0 || coord >= 3) {
            throw new IllegalArgumentException("c == null || coord < 0 || coord >= 3");
        }
        rectilinarCoords[coord] = c;
        float[][] extents = new float[2][3];
        extents[0][coord] = Float.MAX_VALUE;
        extents[1][coord] = -Float.MAX_VALUE;
        for (int i = 0; i < c.length; i++) {
            float f = c[i];
            if (extents[0][coord] > f) {
                extents[0][coord] = f;
            }
            if (extents[1][coord] < f) {
                extents[1][coord] = f;
            }
        }
        super.setExtents(extents);
        affineFromExtents();
        timestamp = System.nanoTime();
    }

    /**
     * TODO: add support for time data
     */
    private void createStatistics() {
        avgGrad = new double[getNComponents()];
        stdDevGrad = new double[getNComponents()];
        derivHistograms = new long[getNComponents()][256];
        thrHistograms = new double[getNComponents()][256];
        long[] count = new long[256];
        final long[] dimensions = getLDims();
        for (int n = 0; n < getNComponents(); n++) {
            if (getComponent(n).isNumeric()) {
                double max = getComponent(n).getPreferredMaxValue();
                double min = getComponent(n).getPreferredMinValue();
                double da = 0, da2 = 0;
                long m;
                Arrays.fill(count, 1);
                m = 1;
                if (min >= max - .001) {
                    double med = .5 * (min + max);
                    min = med - .0005;
                    max = med + .0005;
                }
                final double d = 255 / (max - min);
                final double minf = min;
                final int nf = n;
                if (dimensions.length == 3 && dimensions[0] > 1 && dimensions[1] > 1 && dimensions[2] > 1) {
                    final long s1 = dimensions[0], s2 = dimensions[0] * dimensions[1];
                    long length = dimensions[2] - 1;
                    int nthreads = (int) Math.min(ConcurrencyUtils.getNumberOfThreads(), length);
                    long tmp = length / (long) nthreads;
                    Future<?>[] futures = new Future[nthreads];
                    for (int t = 0; t < nthreads; t++) {
                        final long firstIdx = t * tmp;
                        final long lastIdx = (t == nthreads - 1) ? length : firstIdx + tmp;
                        futures[t] = ConcurrencyUtils.submit(new Callable<Object[]>() {
                            @Override
                            public Object[] call() {
                                long[] derivHistograms = new long[256];
                                double[] thrHistograms = new double[256];
                                long[] count = new long[256];
                                double da = 0, da2 = 0;
                                double r = 1;
                                int h, h0, h1, h2;
                                for (long k = firstIdx; k < lastIdx; k++) {
                                    for (long j = 0; j < dimensions[1] - 1; j++) {
                                        final FloatLargeArray v0 = getComponent(nf).getCurrent1DFloatSlice(k * s2 + j * s1, dimensions[0], 1);
                                        final FloatLargeArray v1 = getComponent(nf).getCurrent1DFloatSlice(k * s2 + (j + 1) * s1, dimensions[0], 1);
                                        final FloatLargeArray v2 = getComponent(nf).getCurrent1DFloatSlice((k + 1) * s2 + j * s1, dimensions[0], 1);
                                        for (long i = 0; i < dimensions[0] - 1; i++) {
                                            h0 = (int) ((v0.getDouble(i) - minf) * d);
                                            if (h0 < 0) {
                                                h0 = 0;
                                            }
                                            if (h0 > 255) {
                                                h0 = 255;
                                            }

                                            double d0 = v0.getDouble(i + 1) - v0.getDouble(i);
                                            if (d0 < 0) {
                                                d0 = -d0;
                                            }
                                            da += d0;
                                            da2 += d0 * d0;
                                            h = (int) (d0 * d);
                                            if (h < 0) {
                                                h = 0;
                                            }
                                            if (h > 255) {
                                                h = 255;
                                            }
                                            derivHistograms[h] += 1;
                                            h = (int) ((v0.getDouble(i + 1) - minf) * d);
                                            if (h < 0) {
                                                h = 0;
                                            }
                                            if (h > 255) {
                                                h = 255;
                                            }
                                            if (h > h0) {
                                                h1 = h0;
                                                h2 = h;
                                            } else {
                                                h1 = h;
                                                h2 = h0;
                                            }
                                            if (d0 < .001) {
                                                r = 1000000.0;
                                            } else {
                                                r = 1 / (d0 * d0);
                                            }
                                            for (int l = h1; l < h2; l++) {
                                                thrHistograms[l] += r;
                                                count[l] += 1;
                                            }

                                            double d1 = v1.getDouble(i) - v0.getDouble(i);
                                            if (d1 < 0) {
                                                d1 = -d1;
                                            }
                                            da += d1;
                                            da2 += d1 * d1;
                                            h = (int) (d1 * d);
                                            if (h < 0) {
                                                h = 0;
                                            }
                                            if (h > 255) {
                                                h = 255;
                                            }
                                            derivHistograms[h] += 1;
                                            h = (int) ((v1.getDouble(i) - minf) * d);
                                            if (h < 0) {
                                                h = 0;
                                            }
                                            if (h > 255) {
                                                h = 255;
                                            }
                                            if (h > h0) {
                                                h1 = h0;
                                                h2 = h;
                                            } else {
                                                h1 = h;
                                                h2 = h0;
                                            }
                                            if (d1 < .001) {
                                                r = 1000000.0;
                                            } else {
                                                r = 1 / (d1 * d1);
                                            }
                                            for (int l = h1; l < h2; l++) {
                                                thrHistograms[l] += r;
                                                count[l] += 1;
                                            }

                                            double d2 = v2.getDouble(i) - v0.getDouble(i);
                                            if (d2 < 0) {
                                                d2 = -d2;
                                            }
                                            da += d2;
                                            da2 += d2 * d2;
                                            h = (int) (d2 * d);
                                            if (h < 0) {
                                                h = 0;
                                            }
                                            if (h > 255) {
                                                h = 255;
                                            }
                                            derivHistograms[h] += 1;
                                            h = (int) ((v2.getDouble(i) - minf) * d);
                                            if (h < 0) {
                                                h = 0;
                                            }
                                            if (h > 255) {
                                                h = 255;
                                            }
                                            if (h > h0) {
                                                h1 = h0;
                                                h2 = h;
                                            } else {
                                                h1 = h;
                                                h2 = h0;
                                            }
                                            if (d2 < .001) {
                                                r = 1000000.0;
                                            } else {
                                                r = 1 / (d2 * d2);
                                            }
                                            for (int l = h1; l < h2; l++) {
                                                thrHistograms[l] += r;
                                                count[l] += 1;
                                            }
                                        }
                                    }
                                }
                                return new Object[]{derivHistograms, thrHistograms, count, da, da2};
                            }
                        });
                    }
                    try {
                        for (int t = 0; t < nthreads; t++) {
                            Object[] res = (Object[]) futures[t].get();
                            for (int i = 0; i < 256; i++) {
                                derivHistograms[n][i] += ((long[]) res[0])[i];
                                thrHistograms[n][i] += ((double[]) res[1])[i];
                                count[i] += ((long[]) res[2])[i];
                            }
                            da += (double) res[3];
                            da2 += (double) res[4];
                        }
                    } catch (InterruptedException | ExecutionException ex) {
                        throw new IllegalStateException(ex);
                    }
                    m = (dimensions[2] - 1) * (dimensions[1] - 1) * (dimensions[0] - 1);
                } else if (dimensions.length == 2 && dimensions[0] > 1 && dimensions[1] > 1) {
                    final long s1 = dimensions[0];
                    long length = dimensions[1] - 1;
                    int nthreads = (int) Math.min(ConcurrencyUtils.getNumberOfThreads(), length);
                    long tmp = length / (long) nthreads;
                    Future<?>[] futures = new Future[nthreads];
                    for (int t = 0; t < nthreads; t++) {
                        final long firstIdx = t * tmp;
                        final long lastIdx = (t == nthreads - 1) ? length : firstIdx + tmp;
                        futures[t] = ConcurrencyUtils.submit(new Callable<Object[]>() {
                            @Override
                            public Object[] call() {
                                long[] derivHistograms = new long[256];
                                double[] thrHistograms = new double[256];
                                long[] count = new long[256];
                                double da = 0, da2 = 0;
                                double r = 1;
                                int h, h0, h1, h2;
                                for (long j = firstIdx; j < lastIdx; j++) {
                                    FloatLargeArray v0 = getComponent(nf).getCurrent1DFloatSlice(j * s1, dimensions[0], 1);
                                    FloatLargeArray v1 = getComponent(nf).getCurrent1DFloatSlice((j + 1) * s1, dimensions[0], 1);
                                    for (long i = 0; i < dimensions[0] - 1; i++) {
                                        h0 = (int) ((v0.getDouble(i) - minf) * d);
                                        if (h0 < 0) {
                                            h0 = 0;
                                        }
                                        if (h0 > 255) {
                                            h0 = 255;
                                        }

                                        double d0 = v0.getDouble(i + 1) - v0.getDouble(i);
                                        if (d0 < 0) {
                                            d0 = -d0;
                                        }
                                        da += d0;
                                        da2 += d0 * d0;
                                        h = (int) (d0 * d);
                                        if (h < 0) {
                                            h = 0;
                                        }
                                        if (h > 255) {
                                            h = 255;
                                        }
                                        derivHistograms[h] += 1;
                                        h = (int) ((v0.getDouble(i + 1) - minf) * d);
                                        if (h < 0) {
                                            h = 0;
                                        }
                                        if (h > 255) {
                                            h = 255;
                                        }
                                        if (h > h0) {
                                            h1 = h0;
                                            h2 = h;
                                        } else {
                                            h1 = h;
                                            h2 = h0;
                                        }
                                        if (d0 < .001) {
                                            r = 1000000.0;
                                        } else {
                                            r = 1 / (d0 * d0);
                                        }
                                        for (int l = h1; l < h2; l++) {
                                            thrHistograms[l] += r;
                                            count[l] += 1;
                                        }

                                        double d1 = v1.getDouble(i) - v0.getDouble(i);
                                        if (d1 < 0) {
                                            d1 = -d1;
                                        }
                                        da += d1;
                                        da2 += d1 * d1;
                                        h = (int) (d1 * d);
                                        if (h < 0) {
                                            h = 0;
                                        }
                                        if (h > 255) {
                                            h = 255;
                                        }
                                        derivHistograms[h] += 1;
                                        h = (int) ((v1.getDouble(i) - minf) * d);
                                        if (h < 0) {
                                            h = 0;
                                        }
                                        if (h > 255) {
                                            h = 255;
                                        }
                                        if (h > h0) {
                                            h1 = h0;
                                            h2 = h;
                                        } else {
                                            h1 = h;
                                            h2 = h0;
                                        }
                                        if (d1 < .001) {
                                            r = 1000000.0;
                                        } else {
                                            r = 1 / (d1 * d1);
                                        }
                                        for (int l = h1; l < h2; l++) {
                                            thrHistograms[l] += r;
                                            count[l] += 1;
                                        }
                                    }
                                }
                                return new Object[]{derivHistograms, thrHistograms, count, da, da2};
                            }
                        });
                    }
                    m = (dimensions[1] - 1) * (dimensions[0] - 1);
                    try {
                        for (int t = 0; t < nthreads; t++) {
                            Object[] res = (Object[]) futures[t].get();
                            for (int i = 0; i < 256; i++) {
                                derivHistograms[n][i] += ((long[]) res[0])[i];
                                thrHistograms[n][i] += ((double[]) res[1])[i];
                                count[i] += ((long[]) res[2])[i];
                            }
                            da += (double) res[3];
                            da2 += (double) res[4];
                        }
                    } catch (InterruptedException | ExecutionException ex) {
                        throw new IllegalStateException(ex);
                    }
                } else if (dimensions.length == 1 && dimensions[0] > 1) {
                    final FloatLargeArray v0 = getComponent(n).getRawFloatArray();
                    long length = dimensions[0] - 1;
                    int nthreads = (int) Math.min(ConcurrencyUtils.getNumberOfThreads(), length);
                    long tmp = length / (long) nthreads;
                    Future<?>[] futures = new Future[nthreads];
                    for (int t = 0; t < nthreads; t++) {
                        final long firstIdx = t * tmp;
                        final long lastIdx = (t == nthreads - 1) ? length : firstIdx + tmp;
                        futures[t] = ConcurrencyUtils.submit(new Callable<Object[]>() {
                            @Override
                            public Object[] call() {
                                long[] derivHistograms = new long[256];
                                double[] thrHistograms = new double[256];
                                long[] count = new long[256];
                                double da = 0, da2 = 0;
                                double r = 1;
                                int h, h0, h1, h2;
                                for (long i = firstIdx; i < lastIdx; i++) {
                                    h0 = (int) ((v0.getDouble(i) - minf) * d);
                                    if (h0 < 0) {
                                        h0 = 0;
                                    }
                                    if (h0 > 255) {
                                        h0 = 255;
                                    }

                                    double d0 = v0.getDouble(i + 1) - v0.getDouble(i);
                                    if (d0 < 0) {
                                        d0 = -d0;
                                    }
                                    da += d0;
                                    da2 += d0 * d0;
                                    h = (int) (d0 * d);
                                    if (h < 0) {
                                        h = 0;
                                    }
                                    if (h > 255) {
                                        h = 255;
                                    }
                                    derivHistograms[h] += 1;
                                    h = (int) ((v0.getDouble(i + 1) - minf) * d);
                                    if (h < 0) {
                                        h = 0;
                                    }
                                    if (h > 255) {
                                        h = 255;
                                    }
                                    if (h > h0) {
                                        h1 = h0;
                                        h2 = h;
                                    } else {
                                        h1 = h;
                                        h2 = h0;
                                    }
                                    if (d0 < .001) {
                                        r = 1000000.0;
                                    } else {
                                        r = 1 / (d0 * d0);
                                    }
                                    for (int l = h1; l < h2; l++) {
                                        thrHistograms[l] += r;
                                        count[l] += 1;
                                    }
                                }
                                return new Object[]{derivHistograms, thrHistograms, count, da, da2};
                            }
                        });
                        m = (dimensions[0] - 1);
                    }
                    try {
                        for (int t = 0; t < nthreads; t++) {
                            Object[] res = (Object[]) futures[t].get();
                            for (int i = 0; i < 256; i++) {
                                derivHistograms[n][i] += ((long[]) res[0])[i];
                                thrHistograms[n][i] += ((double[]) res[1])[i];
                                count[i] += ((long[]) res[2])[i];
                            }
                            da += (double) res[3];
                            da2 += (double) res[4];
                        }
                    } catch (InterruptedException | ExecutionException ex) {
                        throw new IllegalStateException(ex);
                    }
                }
                for (int i = 0; i < 256; i++) {
                    thrHistograms[n][i] /= count[i];
                }
                for (int i = 1; i < 255; i++) {
                    thrHistograms[n][i] = (thrHistograms[n][i - 1] + 2 * thrHistograms[n][i] + thrHistograms[n][i + 1]) / 4;
                }
                m *= 2l;
                avgGrad[n] = da / (double) m;
                stdDevGrad[n] = sqrt(max(0, da2 / (double) m - avgGrad[n] * avgGrad[n]));
            }
        }
        statisticsComputed = true;
        timestamp = System.nanoTime();
    }

    /**
     * Returns optimum thresholds histogram.
     *
     * @return optimum thresholds histogram
     */
    public double[][] getThrHistograms() {
        if (!statisticsComputed) {
            createStatistics();
        }
        return thrHistograms;
    }

    /**
     * Returns combinatorial gradient norm histogram (derivatives are taken over indices).
     *
     * @return combinatorial gradient norm histogram
     */
    public long[][] getDerivHistograms() {
        if (!statisticsComputed) {
            createStatistics();
        }
        return derivHistograms;
    }

    /**
     * Retruns average gradient norm (derivatives are taken over indices)
     *
     * @return average gradient norm
     */
    public double[] getAvgGrad() {
        if (!statisticsComputed) {
            createStatistics();
        }
        return avgGrad;
    }

     /**
     * Retruns standard deviation of gradient norm (derivatives are taken over indices)
     *
     * @return standard deviation of gradient norm
     */
    public double[] getStdDevGrad() {
        if (!statisticsComputed) {
            createStatistics();
        }
        return stdDevGrad;
    }

    /**
     * Returns interpolated data from an array and given indexes. Only
     * UnsignedByte, Short, Int, Float and Double large arrays are supported.
     *
     * @param data input array
     * @param u index
     * @param v index
     * @param w index
     *
     * @return interpolated data
     */
    public Object getInterpolatedData(LargeArray data, float u, float v, float w) {
        switch (data.getType()) {
            case UNSIGNED_BYTE:
                return RegularFieldInterpolator.getInterpolatedData((UnsignedByteLargeArray) data, this.dims, u, v, w);
            case SHORT:
                return RegularFieldInterpolator.getInterpolatedData((ShortLargeArray) data, this.dims, u, v, w);
            case INT:
                return RegularFieldInterpolator.getInterpolatedData((IntLargeArray) data, this.dims, u, v, w);
            case FLOAT:
                return RegularFieldInterpolator.getInterpolatedData((FloatLargeArray) data, this.dims, u, v, w);
            case DOUBLE:
                return RegularFieldInterpolator.getInterpolatedData((DoubleLargeArray) data, this.dims, u, v, w);
            default:
                throw new IllegalArgumentException("Unsupported array type.");
        }
    }

    /**
     * Returns interpolated data from an array and given indexes.
     *
     * @param data input array
     * @param u index
     * @param v index
     * @param w index
     *
     * @return interpolated data
     */
    public byte[] getInterpolatedData(UnsignedByteLargeArray data, float u, float v, float w) {
        return RegularFieldInterpolator.getInterpolatedData(data, this.dims, u, v, w);
    }

    /**
     * Returns interpolated data from an array and given indexes.
     *
     * @param data input array
     * @param u index
     * @param v index
     * @param w index
     *
     * @return interpolated data
     */
    public short[] getInterpolatedData(ShortLargeArray data, float u, float v, float w) {
        return RegularFieldInterpolator.getInterpolatedData(data, this.dims, u, v, w);
    }

    /**
     * Returns interpolated data from an array and given indexes.
     *
     * @param data input array
     * @param u index
     * @param v index
     * @param w index
     *
     * @return interpolated data
     */
    public int[] getInterpolatedData(IntLargeArray data, float u, float v, float w) {
        return RegularFieldInterpolator.getInterpolatedData(data, this.dims, u, v, w);
    }

    /**
     * Returns interpolated data from an array and given indexes.
     *
     * @param data input array
     * @param u index
     * @param v index
     * @param w index
     *
     * @return interpolated data
     */
    public float[] getInterpolatedData(FloatLargeArray data, float u, float v, float w) {
        return RegularFieldInterpolator.getInterpolatedData(data, this.dims, u, v, w);
    }

    /**
     * Returns interpolated data from an array and given indexes.
     *
     * @param data input array
     * @param u index
     * @param v index
     * @param w index
     *
     * @return interpolated data
     */
    public double[] getInterpolatedData(DoubleLargeArray data, float u, float v, float w) {
        return RegularFieldInterpolator.getInterpolatedData(data, this.dims, u, v, w);
    }

    /**
     * Returns coordinates of the grid point of a 1D regular field addressed by integer index u.
     * Integer addressing limits the method to grid nodes only.
     * For affine fields the coordinates are calculated from affine matrix.
     * For non-affine fields the coordinates are taken from explicit coordinates.
     *
     * @param u first index of grid point, trimmed to range 0:dims[0]-1
     *
     * @return grid point coordinates in nSpace-dimensional space
     */
    public float[] getGridCoords(int u) {
        if (dims.length != 1) {
            throw new IllegalArgumentException("dims.length != 1");
        }
        if (u < 0) {
            u = 0;
        }
        if (u > dims[0] - 1) {
            u = dims[0] - 1;
        }
        float[] c = new float[getNSpace()];
        if (timeCoords == null || timeCoords.isEmpty()) {
            for (int l = 0; l < getNSpace(); l++) {
                c[l] = affine[3][l] + u * affine[0][l];
            }
        } else {
            for (int i = 0, j = u * getNSpace(); i < c.length; i++, j++) {
                c[i] = timeCoords.getValue(currentTime).getFloat(j);
            }
        }
        return c;
    }

    /**
     * Returns coordinates of the grid point of a 2D regular field addressed by integer indices u,v.
     * Integer addressing limits the method to grid nodes only.
     * For affine fields the coordinates are calculated from affine matrix.
     * For non-affine fields the coordinates are taken from explicit coordinates.
     *
     * @param u first index of grid point, trimmed to range 0:dims[0]-1
     * @param v second index of grid point, trimmed to range 0:dims[1]-1
     *
     * @return grid point coordinates in nSpace-dimensional space
     */
    public float[] getGridCoords(int u, int v) {
        if (dims.length != 2) {
            throw new IllegalArgumentException("dims.length != 2");
        }
        if (u < 0) {
            u = 0;
        }
        if (u > dims[0] - 1) {
            u = dims[0] - 1;
        }
        if (v < 0) {
            v = 0;
        }
        if (v > dims[1] - 1) {
            v = dims[1] - 1;
        }
        float[] c = new float[getNSpace()];
        if (timeCoords == null || timeCoords.isEmpty()) {
            for (int l = 0; l < getNSpace(); l++) {
                c[l] = affine[3][l] + u * affine[0][l] + v * affine[1][l];
            }
        } else {
            for (int i = 0, j = (v * dims[0] + u) * getNSpace(); i < c.length; i++, j++) {
                c[i] = timeCoords.getValue(currentTime).getFloat(j);
            }
        }
        return c;
    }

    /**
     * Returns coordinates of the grid point of a 3D regular field addressed by integer indices u,v,w.
     * Integer addressing limits the method to grid nodes only.
     * For affine fields the coordinates are calculated from affine matrix.
     * For non-affine fields the coordinates are taken from explicit coordinates.
     *
     * @param u first index of grid point, trimmed to range 0:dims[0]-1
     * @param v second index of grid point, trimmed to range 0:dims[1]-1
     * @param w third index of grid point, trimmed to range 0:dims[2]-1
     *
     * @return grid point coordinates in nSpace-dimensional space
     */
    public float[] getGridCoords(int u, int v, int w) {
        if (dims.length != 3) {
            throw new IllegalArgumentException("dims.length != 3");
        }
        if (u < 0) {
            u = 0;
        }
        if (u > dims[0] - 1) {
            u = dims[0] - 1;
        }
        if (v < 0) {
            v = 0;
        }
        if (v > dims[1] - 1) {
            v = dims[1] - 1;
        }
        if (w < 0) {
            w = 0;
        }
        if (w > dims[2] - 1) {
            w = dims[2] - 1;
        }
        float[] c = new float[getNSpace()];
        if (timeCoords == null || timeCoords.isEmpty()) {
            for (int l = 0; l < 3; l++) {
                c[l] = affine[3][l] + u * affine[0][l] + v * affine[1][l] + w * affine[2][l];
            }
        } else {
            for (int i = 0, j = ((w * dims[1] + v) * dims[0] + u) * getNSpace(); i < c.length; i++, j++) {
                c[i] = timeCoords.getValue(currentTime).getFloat(j);
            }
        }
        return c;
    }

    /**
     * Returns coordinates of the pseudo-grid point of a 1D regular field addressed by float index u.
     * Float addressing allows to get the coordinates of any pseudo-grid point.
     * For affine fields the coordinates are calculated from affine matrix.
     * For non-affine fields the coordinates are interpolated from explicit coordinates.
     *
     * @param u first index of grid point, trimmed to range 0:dims[0]-1
     *
     * @return point coordinates in nSpace-dimensional space
     */
    public float[] getGridCoords(float u) {
        if (dims.length != 1) {
            throw new IllegalArgumentException("dims.length != 1");
        }
        if (u < 0) {
            u = 0;
        }
        if (u > dims[0] - 1) {
            u = dims[0] - 1;
        }
        if (timeCoords == null || timeCoords.isEmpty()) {
            float[] c = new float[getNSpace()];
            if (u < 0) {
                u = 0;
            }
            if (u > dims[0] - 1) {
                u = dims[0] - 1;
            }
            for (int l = 0; l < getNSpace(); l++) {
                c[l] = affine[3][l] + u * affine[0][l];
            }
            return c;
        }
        return getInterpolatedData((FloatLargeArray) timeCoords.getValue(currentTime), u, 0.f, 0.f);
    }

    /**
     * Returns coordinates of the pseudo-grid point of a 2D regular field addressed by float indices u,v.
     * Float addressing allows to get the coordinates of any pseudo-grid point.
     * For affine fields the coordinates are calculated from affine matrix.
     * For non-affine fields the coordinates are interpolated from explicit coordinates.
     *
     * @param u first index of grid point, trimmed to range 0:dims[0]-1
     * @param v second index of grid point, trimmed to range 0:dims[1]-1
     *
     * @return point coordinates in nSpace-dimensional space
     */
    public float[] getGridCoords(float u, float v) {
        if (dims.length != 2) {
            throw new IllegalArgumentException("dims.length != 2");
        }
        if (u < 0) {
            u = 0;
        }
        if (u > dims[0] - 1) {
            u = dims[0] - 1;
        }
        if (v < 0) {
            v = 0;
        }
        if (v > dims[1] - 1) {
            v = dims[1] - 1;
        }
        if (timeCoords == null || timeCoords.isEmpty()) {
            float[] c = new float[getNSpace()];
            for (int l = 0; l < getNSpace(); l++) {
                c[l] = affine[3][l] + u * affine[0][l] + v * affine[1][l];
            }
            return c;
        }
        return getInterpolatedData((FloatLargeArray) timeCoords.getValue(currentTime), u, v, 0.f);
    }

    /**
     * Returns coordinates of the pseudo-grid point of a 3D regular field addressed by float indices u,v,w.
     * Float addressing allows to get the coordinates of any pseudo-grid point.
     * For affine fields the coordinates are calculated from affine matrix.
     * For non-affine fields the coordinates are interpolated from explicit coordinates.
     *
     * @param u first index of grid point, trimmed to range 0:dims[0]-1
     * @param v second index of grid point, trimmed to range 0:dims[1]-1
     * @param w third index of grid point, trimmed to range 0:dims[2]-1
     *
     * @return point coordinates in nSpace-dimensional space
     */
    public float[] getGridCoords(float u, float v, float w) {
        if (dims.length != 3) {
            throw new IllegalArgumentException("dims.length != 3");
        }
        if (u < 0) {
            u = 0;
        }
        if (u > dims[0] - 1) {
            u = dims[0] - 1;
        }
        if (v < 0) {
            v = 0;
        }
        if (v > dims[1] - 1) {
            v = dims[1] - 1;
        }
        if (w < 0) {
            w = 0;
        }
        if (w > dims[2] - 1) {
            w = dims[2] - 1;
        }
        if (timeCoords == null || timeCoords.isEmpty()) {
            float[] c = new float[getNSpace()];
            for (int l = 0; l < 3; l++) {
                c[l] = affine[3][l] + u * affine[0][l] + v * affine[1][l] + w * affine[2][l];
            }
            return c;
        }
        return getInterpolatedData((FloatLargeArray) timeCoords.getValue(currentTime), u, v, w);
    }

    /**
     * Returns a 2D slice.
     *
     * @param comp index of a component
     * @param axis axis specification: 0, 1, or 2
     * @param slice slice number
     *
     * @return a 2D slice
     */
    public DataArray get2DSlice(int comp, int axis, int slice) {
        return components.get(comp).get2DSlice(getLDims(), axis, slice);
    }

    /**
     * Returns a 2D slice of the current time step.
     *
     * @param comp index of a component
     * @param axis axis specification: 0, 1, or 2
     * @param slice slice number
     *
     * @return 2D slice of the current time step.
     */
    public LargeArray getCurrent2DSlice(int comp, int axis, int slice) {
        return components.get(comp).getCurrent2DSlice(getLDims(), axis, slice);
    }

    /**
     * Returns a 2D float slice.
     *
     * @param comp index of a component
     * @param axis axis specification: 0, 1, or 2
     * @param slice slice number
     *
     * @return a 2D float slice
     */
    public DataArray get2DFloatSlice(int comp, int axis, int slice) {
        return components.get(comp).get2DFloatSlice(getLDims(), axis, slice);
    }

    /**
     * Returns a 2D float slice of the current time step.
     *
     * @param comp index of a component
     * @param axis axis specification: 0, 1, or 2
     * @param slice slice number
     *
     * @return 2D float slice of the current time step.
     */
    public FloatLargeArray getCurrent2DFloatSlice(int comp, int axis, int slice) {
        return components.get(comp).getCurrent2DFloatSlice(getLDims(), axis, slice);
    }

    /**
     * Returns a 2D slice of norms.
     *
     * @param comp index of a component
     * @param axis axis specification: 0, 1, or 2
     * @param slice slice number
     *
     * @return a 2D slice of norms
     */
    public DataArray get2DNormSlice(int comp, int axis, int slice) {
        return components.get(comp).get2DNormSlice(getLDims(), axis, slice);
    }

    /**
     * Returns a 2D slice of norms of the current time step.
     *
     * @param comp index of a component
     * @param axis axis specification: 0, 1, or 2
     * @param slice slice number
     *
     * @return 2D slice of norms of the current time step.
     */
    public FloatLargeArray getCurrent2DNormSlice(int comp, int axis, int slice) {
        return components.get(comp).getCurrent2DNormSlice(getLDims(), axis, slice);
    }

    private DataArray interpolateDataToIrregularMesh(FloatLargeArray mesh, DataArray da) {
        LargeArray outData;
        int vlen = da.getVectorLength();
        if (getNSpace() != 3) {
            throw new IllegalArgumentException("nSpace != 3");
        }
        long nMeshNodes = mesh.length() / getNSpace();
        if (mesh.length() != getNSpace() * nMeshNodes) {
            throw new IllegalArgumentException("mesh.length() != nSpace * nMeshNodes");
        }
        if (timeCoords != null && !timeCoords.isEmpty()) {
            throw new UnsupportedOperationException("Not supported yet.");
        }
        DataArrayType daType = da.getType();
        if (daType != DataArrayType.FIELD_DATA_BYTE && daType != DataArrayType.FIELD_DATA_SHORT && daType != DataArrayType.FIELD_DATA_INT && daType != DataArrayType.FIELD_DATA_LONG && daType != DataArrayType.FIELD_DATA_FLOAT && daType != DataArrayType.FIELD_DATA_DOUBLE) {
            throw new IllegalArgumentException("Unsupported array type.");
        }

        outData = LargeArrayUtils.create(da.getRawArray().getType(), vlen * nMeshNodes, false);
        for (long i = 0, l = 0; i < nMeshNodes; i++, l += vlen) {
            float[] p = new float[3];
            float[] v = new float[3];
            for (int j = 0; j < 3; j++) {
                p[j] = mesh.getFloat(3 * i + j) - affine[3][j];
                v[j] = 0;
            }
            for (int j = 0; j < v.length; j++) {
                for (int k = 0; k < v.length; k++) {
                    v[j] += invAffine[j][k] * p[k];
                }
            }

            Object od = getInterpolatedData(da.getRawArray(), v[0], v[1], v[2]);
            LargeArrayUtils.arraycopy(od, 0, outData, l, vlen);
        }
        return DataArray.create(outData, vlen, da.getName());
    }

    private DataArray interpolateDataToAffineMesh(int[] dims, float[][] meshAffine, DataArray da) {
        LargeArray outData;
        DataArray outDA;
        int vlen = da.getVectorLength();
        if (getNSpace() != 3) {
            throw new IllegalArgumentException("nSpace != 3");
        }
        long nMeshNodes = 1;
        for (int i = 0; i < dims.length; i++) {
            nMeshNodes *= (long) dims[i];
        }
        if (timeCoords != null && !timeCoords.isEmpty()) {
            throw new UnsupportedOperationException("Not supported yet.");
        }
        DataArrayType daType = da.getType();
        if (daType != DataArrayType.FIELD_DATA_BYTE && daType != DataArrayType.FIELD_DATA_SHORT && daType != DataArrayType.FIELD_DATA_INT && daType != DataArrayType.FIELD_DATA_LONG && daType != DataArrayType.FIELD_DATA_FLOAT && daType != DataArrayType.FIELD_DATA_DOUBLE) {
            throw new IllegalArgumentException("Unsupported array type.");
        }
        outData = LargeArrayUtils.create(daType.toLargeArrayType(), vlen * nMeshNodes, false);

        for (long i = 0, l = 0; i < nMeshNodes; i++, l += vlen) {
            long i0, i1, i2;
            float[] p = new float[3];
            float[] v = new float[3];
            switch (dims.length) {
                case 1:
                    for (int j = 0; j < 3; j++) {
                        p[j] = meshAffine[3][j] + i * meshAffine[0][j] - affine[3][j];
                    }
                    break;
                case 2:
                    i1 = i / dims[0];
                    i0 = i % dims[0];
                    for (int j = 0; j < 3; j++) {
                        p[j] = meshAffine[3][j] + i0 * meshAffine[0][j] + i1 * meshAffine[1][j] - affine[3][j];
                    }
                    break;
                case 3:
                    i2 = i / (dims[1] * dims[0]);
                    i1 = (i / dims[0]) % dims[1];
                    i0 = i % dims[0];
                    for (int j = 0; j < 3; j++) {
                        p[j] = meshAffine[3][j] + i0 * meshAffine[0][j] + i1 * meshAffine[1][j] + i2 * meshAffine[2][j] - affine[3][j];
                    }
                    break;
            }
            for (int j = 0; j < 3; j++) {
                v[j] = 0;
            }

            for (int j = 0; j < v.length; j++) {
                for (int k = 0; k < v.length; k++) {
                    v[j] += invAffine[j][k] * p[k];
                }
            }

            Object od = getInterpolatedData(da.getRawArray(), v[0], v[1], v[2]);
            LargeArrayUtils.arraycopy(od, 0, outData, l, vlen);
        }
        outDA = DataArray.create(outData, vlen, da.getName());
        outDA.recomputeStatistics();
        return outDA;
    }

    @Override
    public DataArray interpolateDataToMesh(Field mesh, DataArray da) {
        if (mesh instanceof RegularField && ((RegularField) mesh).getCurrentCoords() == null) {
            RegularField fld = (RegularField) mesh;
            return interpolateDataToAffineMesh(fld.getDims(), fld.getAffine(), da);
        } else {
            return interpolateDataToIrregularMesh(mesh.getCurrentCoords(), da);
        }
    }

    /**
     * Downsamples a regular field.
     *
     * @param down amount of downsampling in each dimension
     *
     * @return downsampled regular filed
     */
    public RegularField downsample(int[] down) {
        if (down == null) {
            throw new IllegalArgumentException("down argument cannot be null");
        }
        if (down.length != getDims().length) {
            throw new IllegalArgumentException("down.length != getDims().length");
        }
        int[] inDims = getDims();
        int[] outDims = new int[inDims.length];
        for (int i = 0; i < inDims.length; i++) {
            outDims[i] = (inDims[i] - 1) / down[i] + 1;
        }

        RegularField outField = new RegularField(outDims);
        outField.setNSpace(getNSpace());
        if (getCurrentCoords() != null) {
            outField.setCurrentCoords((FloatLargeArray) downArray(getCurrentCoords(), getNSpace(), inDims, down));
        } else {
            float[][] outAffine = new float[4][3];
            System.arraycopy(affine[3], 0, outAffine[3], 0, 3);
            for (int i = 0; i < outDims.length; i++) {
                for (int j = 0; j < 3; j++) {
                    outAffine[i][j] = affine[i][j] * down[i];
                }
            }
            outField.setAffine(outAffine);
        }
        for (DataArray dta : getComponents()) {
            outField.addComponent(DataArray.create(downArray(dta.getRawArray(), dta.getVectorLength(), inDims, down),
                    dta.getVectorLength(), dta.getName())).unit(dta.getUnit()).userData(dta.getUserData());
        }

        return outField;
    }

    @Override
    public boolean isStructureCompatibleWith(Field f) {
        if (!(f instanceof RegularField)) {
            return false;
        }
        RegularField rf = (RegularField) f;
        if (rf.getDims().length != dims.length) {
            return false;
        }
        for (int i = 0; i < dims.length; i++) {
            if (dims[i] != rf.getDims()[i]) {
                return false;
            }
        }
        return true;
    }

    /**
     * Returns true if a given field has dimensions compatible with this field,
     * false otherwise.
     *
     * @param f input field
     *
     * @return true if a given field has dimensions compatible with this field,
     * false otherwise
     */
    public boolean isDimensionCompatibleWith(Field f) {
        if (!(f instanceof RegularField)) {
            return false;
        }
        RegularField rf = (RegularField) f;
        if (rf.getDims().length != dims.length) {
            return false;
        }
        return true;
    }

    @Override
    public void checkTrueNSpace() {
        trueNSpace = -1;
        switch (dims.length) {
            case 3:
                trueNSpace = 3;
                timestamp = System.nanoTime();
                return;
            case 2:
                trueNSpace = 2;
                if (coords == null) {
                    if (affine[1][2] != 0 || affine[0][2] != 0 || affine[3][2] != 0) {
                        trueNSpace = -1;
                    }
                } else {
                    if (getNSpace() == 2) {
                        return;
                    }
                    for (int i = 0; i < nElements; i++) {
                        if (coords.getFloat(3 * i + 2) != 0) {
                            trueNSpace = -1;
                            timestamp = System.nanoTime();
                            return;
                        }
                    }
                }
                break;
            case 1:
                trueNSpace = 1;
                if (coords == null) {
                    if (affine[0][1] != 0 || affine[0][2] != 0
                            || affine[3][1] != 0 || affine[3][2] != 0) {
                        trueNSpace = -1;
                    }
                } else {
                    switch (getNSpace()) {
                        case 1:
                            timestamp = System.nanoTime();
                            return;
                        case 2:
                            for (int i = 0; i < nElements; i++) {
                                if (coords.getFloat(2 * i + 1) != 0) {
                                    trueNSpace = -1;
                                    timestamp = System.nanoTime();
                                    return;
                                }
                            }
                            break;
                        case 3:
                            for (int i = 0; i < nElements; i++) {
                                if (coords.getFloat(3 * i + 1) != 0 || coords.getFloat(3 * i + 2) != 0) {
                                    trueNSpace = -1;
                                    timestamp = System.nanoTime();
                                    return;
                                }
                            }
                            break;
                    }
                }
                break;
        }
        timestamp = System.nanoTime();
    }

    private FloatLargeArray updateCoordsFromAffine() {
        if (coordsFromAffine == null) {
            coordsFromAffine = new FloatLargeArray(3 * nElements, false);
        }
        long n = dims[dims.length - 1];
        int nthreads = (int) min(ConcurrencyUtils.getNumberOfThreads(), n);
        long k = n / nthreads;
        Future<?>[] futures = new Future[nthreads];
        for (int j = 0; j < nthreads; j++) {
            final long firstIdx = j * k;
            final long lastIdx = (j == nthreads - 1) ? n : firstIdx + k;
            futures[j] = ConcurrencyUtils.submit(new Runnable() {
                @Override
                public void run() {
                    long l;
                    float[] c = new float[3];
                    float[] d = new float[3];
                    switch (dims.length) {
                        case 3:
                            l = firstIdx * dims[1] * dims[0] * 3;
                            for (long i = firstIdx; i < lastIdx; i++) {
                                for (int j = 0; j < 3; j++) {
                                    c[j] = affine[3][j] + i * affine[2][j];
                                }
                                for (long j = 0; j < dims[1]; j++) {
                                    for (int k = 0; k < 3; k++) {
                                        d[k] = c[k] + j * affine[1][k];
                                    }
                                    for (long k = 0; k < dims[0]; k++) {
                                        for (int m = 0; m < 3; m++, l++) {
                                            coordsFromAffine.setFloat(l, d[m] + k * affine[0][m]);
                                        }
                                    }
                                }
                            }
                            break;
                        case 2:
                            l = firstIdx * dims[0] * 3;
                            for (long i = firstIdx; i < lastIdx; i++) {
                                for (int j = 0; j < 3; j++) {
                                    c[j] = affine[3][j] + i * affine[1][j];
                                }
                                for (long j = 0; j < dims[0]; j++) {
                                    for (int k = 0; k < 3; k++, l++) {
                                        coordsFromAffine.setFloat(l, c[k] + j * affine[0][k]);
                                    }
                                }
                            }
                            break;
                        case 1:
                            l = firstIdx * 3;
                            for (long i = firstIdx; i < lastIdx; i++) {
                                for (int k = 0; k < 3; k++, l++) {
                                    coordsFromAffine.setFloat(l, affine[3][k] + i * affine[0][k]);
                                }
                            }
                    }
                }
            }
            );
        }
        try {
            ConcurrencyUtils.waitForCompletion(futures);
        } catch (InterruptedException | ExecutionException ex) {
            throw new IllegalStateException(ex);
        }
        timestamp = System.nanoTime();
        coordsTimestamp = System.nanoTime();
        return coordsFromAffine;
    }

    /**
     * Returns coordinates computed from affine vectors.
     *
     * @return coordinates computed from affine vectors
     */
    public FloatLargeArray getCoordsFromAffine() {
        if (coordsFromAffine != null) {
            return coordsFromAffine;
        }
        return updateCoordsFromAffine();
    }

    /**
     * Returns 3D coordinates. For 1D field the second and the third coordinate
     * is set to zero. For 2D field the third coordinate is set to zero.
     *
     * @return 3D coordinates
     */
    public FloatLargeArray get3DCoords() {
        if (this.coords == null) {
            return updateCoordsFromAffine();
        } else {
            FloatLargeArray outCoords;
            switch (getNSpace()) {
                case 3:
                    return coords;
                case 2:
                    outCoords = new FloatLargeArray(3 * nElements, false);
                    for (long i = 0; i < nElements; i++) {
                        outCoords.setFloat(3 * i, coords.getFloat(2 * i));
                        outCoords.setFloat(3 * i + 1, coords.getFloat(2 * i + 1));
                        outCoords.setFloat(3 * i + 2, 0.0f);
                    }
                    return outCoords;
                case 1:
                    outCoords = new FloatLargeArray(3 * nElements, false);
                    for (long i = 0; i < nElements; i++) {
                        outCoords.setFloat(3 * i, coords.getFloat(i));
                        outCoords.setFloat(3 * i + 1, 0.0f);
                        outCoords.setFloat(3 * i + 2, 0.0f);
                    }
                    return outCoords;
            }
        }
        throw new IllegalArgumentException("The coordinates cannot be computed.");
    }

    @Override
    public FloatLargeArray getNormals() {
        if (dims.length != 2) {
            return null;
        }
        normals = new FloatLargeArray(3 * nElements, false);
        if (timeCoords == null || timeCoords.isEmpty()) {
            float[] h = new float[3];
            h[0] = affine[0][1] * affine[1][2] - affine[0][2] * affine[1][1];
            h[1] = affine[0][2] * affine[1][0] - affine[0][0] * affine[1][2];
            h[2] = affine[0][0] * affine[1][1] - affine[0][1] * affine[1][0];
            float r = (float) (sqrt(h[0] * h[0] + h[1] * h[1] + h[2] * h[2]));
            for (int i = 0; i < h.length; i++) {
                h[i] /= r;
            }
            for (long i = 0, k = 0; i < nElements; i++) {
                for (int j = 0; j < h.length; j++, k++) {
                    normals.setFloat(k, h[j]);
                }
            }
        } else if (getNSpace() == 2) {
            for (long i = 0; i < nElements; i++) {
                normals.setFloat(3 * i, 0);
                normals.setFloat(3 * i + 1, 0);
                normals.setFloat(3 * i + 2, 1);
            }
        } else {
            FloatLargeArray c = (FloatLargeArray) timeCoords.getValues().get(currentFrame);
            float[] u = new float[3];
            float[] v = new float[3];
            float[] h = new float[3];
            long n = 3 * dims[0];
            for (long i = 0, k = 0; i < dims[1]; i++) {
                for (long j = 0; j < dims[0]; j++, k += 3) {
                    if (i == 0) {
                        for (int l = 0; l < 3; l++) {
                            v[l] = c.getFloat(k + n + l) - c.getFloat(k + l);
                        }
                    } else if (i == dims[1] - 1) {
                        for (int l = 0; l < 3; l++) {
                            v[l] = c.getFloat(k + l) - c.getFloat(k - n + l);
                        }
                    } else {
                        for (int l = 0; l < 3; l++) {
                            v[l] = c.getFloat(k + n + l) - c.getFloat(k - n + l);
                        }
                    }
                    if (j == 0) {
                        for (int l = 0; l < 3; l++) {
                            u[l] = c.getFloat(k + 3 + l) - c.getFloat(k + l);
                        }
                    } else if (j == dims[0] - 1) {
                        for (int l = 0; l < 3; l++) {
                            u[l] = c.getFloat(k + l) - c.getFloat(k - 3 + l);
                        }
                    } else {
                        for (int l = 0; l < 3; l++) {
                            u[l] = c.getFloat(k + 3 + l) - c.getFloat(k - 3 + l);
                        }
                    }
                    h[0] = u[1] * v[2] - u[2] * v[1];
                    h[1] = u[2] * v[0] - u[0] * v[2];
                    h[2] = u[0] * v[1] - u[1] * v[0];
                    float r = (float) (sqrt(h[0] * h[0] + h[1] * h[1] + h[2] * h[2]));
                    for (int l = 0; l < 3; l++) {
                        normals.setFloat(k + l, h[l] / r);
                    }
                }
            }
        }
        return normals;
    }

    /**
     * Retruns index of a field node closest to the given point. 
     *
     * @param x x coordinate
     * @param y y coordinate
     * @param z z coordinate
     *
     * @return index of a field node closest to the given point
     */
    public int[] getIndices(float x, float y, float z) {
        int[] ind = new int[3];
        if (dims.length == 3) {
            float[] p = getFloatIndices(x, y, z);
            for (int i = 0; i < 3; i++) {
                ind[i] = round(p[i]);
            }
        } else if ((timeCoords == null || timeCoords.isEmpty()) && dims.length == 2) {
            float[] v = {x, y};
            float[] p = new float[2];
            for (int i = 0; i < 2; i++) {
                p[i] = v[i] - affine[3][i];
            }
            float[] det = new float[3];
            det[2] = affine[0][0] * affine[1][1] - affine[1][0] * affine[0][1];
            det[0] = p[0] * affine[1][1] - affine[1][0] * p[1];
            det[1] = affine[0][0] * p[1] - p[0] * affine[0][1];
            ind[0] = round(det[0] / det[2]);
            ind[1] = round(det[1] / det[2]);
            ind[2] = 0;
        }
        for (int i = 0; i < dims.length; i++) {
            if (ind[i] < 0) {
                ind[i] = 0;
            }
            if (ind[i] >= dims[i]) {
                ind[i] = dims[i] - 1;
            }
        }
        return ind;
    }

     /**
     * Retruns index of a field node closest to the given point. 
     *
     * @param x x coordinate
     * @param y y coordinate
     *
     * @return index of a field node closest to the given point
     */
    public int[] getIndices(float x, float y) {
        int[] ind = new int[3];
        if (dims.length == 3) {
            float[] p = getFloatIndices(x, y);
            for (int i = 0; i < 3; i++) {
                ind[i] = round(p[i]);
            }
        } else if ((timeCoords == null || timeCoords.isEmpty()) && dims.length == 2) {
            float[] v = {x, y};
            float[] p = new float[2];
            for (int i = 0; i < 2; i++) {
                p[i] = v[i] - affine[3][i];
            }
            float[] det = new float[3];
            det[2] = affine[0][0] * affine[1][1] - affine[1][0] * affine[0][1];
            det[0] = p[0] * affine[1][1] - affine[1][0] * p[1];
            det[1] = affine[0][0] * p[1] - p[0] * affine[0][1];
            ind[0] = round(det[0] / det[2]);
            ind[1] = round(det[1] / det[2]);
            ind[2] = 0;
        }
        for (int i = 0; i < dims.length; i++) {
            if (ind[i] < 0) {
                ind[i] = 0;
            }
            if (ind[i] >= dims[i]) {
                ind[i] = dims[i] - 1;
            }
        }
        return ind;
    }

    /**
     * Returns coordinates of a given point in the index coordinate system.
     *
     * @param x x coordinate
     * @param y y coordinate
     * @param z z coordinate
     *
     * @return coordinates of a given point in the index coordinate system
     */
    public float[] getFloatIndices(float x, float y, float z) {
        if (dims == null || dims.length != 3) {
            throw new IllegalArgumentException("dims == null || dims.length != 3");
        }

        float[] ind = {0, 0, 0};
        if (timeCoords == null || timeCoords.isEmpty()) {
            float[] p = new float[]{x - affine[3][0], y - affine[3][1], z - affine[3][2]};

            for (int i = 0; i < 3; i++) {
                ind[i] = 0;
                for (int j = 0; j < 3; j++) {
                    ind[i] += invAffine[j][i] * p[j];
                }
            }
        } else {
            if (geoTree == null) {
                createGeoTree();
            }
            TetrahedronPosition tCoords = getFieldCoords(new float[]{x, y, z});
            if (tCoords == null) {
                return new float[]{-1, -1, -1};
            }
            for (int m = 0; m < 4; m++) {
                int s = tCoords.getVertices()[m];
                float t = tCoords.getCoords()[m];
                int i0 = s % dims[0];
                int i1 = s / dims[0];
                int i2 = i1 / dims[1];
                i1 %= dims[1];
                ind[0] += t * i0;
                ind[1] += t * i1;
                ind[2] += t * i2;
            }
        }
        return ind;
    }

    /**
     * Returns coordinates of a given point in the index coordinate system.
     *
     * @param x x coordinate
     * @param y y coordinate
     *
     * @return coordinates of a given point in the index coordinate system
     */
    public float[] getFloatIndices(float x, float y) {
        if (dims == null || dims.length != 2) {
            throw new IllegalArgumentException("dims == null || dims.length != 2");
        }

        float[] ind = {0, 0};
        if (timeCoords == null || timeCoords.isEmpty()) {
            float[] p = new float[]{x - affine[3][0], y - affine[3][1]};
            for (int i = 0; i < 2; i++) {
                for (int j = 0; j < 2; j++) {
                    ind[i] += invAffine[i][j] * p[j];
                }
            }
        } else {
            if (geoTree == null) {
                createGeoTree();
            }
            TetrahedronPosition tCoords = getFieldCoords(new float[]{x, y});
            if (tCoords == null) {
                return new float[]{-1, -1, -1};
            }
            for (int m = 0; m < 4; m++) {
                int s = tCoords.getVertices()[m];
                float t = tCoords.getCoords()[m];
                int i0 = s % dims[0];
                int i1 = s / dims[0];
                int i2 = i1 / dims[1];
                i1 %= dims[1];
                ind[0] += t * i0;
                ind[1] += t * i1;
                ind[2] += t * i2;
            }
        }
        return ind;
    }

    @Override
    public void createGeoTree() {
        FloatLargeArray c = (FloatLargeArray) timeCoords.getValue(currentTime);

        if (dims.length != trueNSpace) {
            return;
        }
        float[] cellLow = new float[trueNSpace];
        float[] cellUp = new float[trueNSpace];
        int nCells = 1;
        for (int i = 0; i < trueNSpace; i++) {
            cellExtentsDown[i] = (dims[i] + MAXCELLDIM - 1) / MAXCELLDIM;
            cellExtentsDims[i] = (dims[i] + cellExtentsDown[i] - 2) / cellExtentsDown[i]; // check
            nCells *= cellExtentsDims[i];
        }
        int[] cells = new int[nCells];
        cellExtents = new float[2 * trueNSpace][nCells];
        switch (trueNSpace) {
            case 3:
                for (int i = 0, l = 0; i < cellExtentsDims[2]; i++) {
                    for (int j = 0; j < cellExtentsDims[1]; j++) {
                        for (int k = 0; k < cellExtentsDims[0]; k++, l++) {
                            cells[l] = l;
                            int i0 = i * cellExtentsDown[2];
                            int i1 = i0 + cellExtentsDown[2] + 1;
                            if (i1 > dims[2]) {
                                i1 = dims[2];
                            }
                            int j0 = j * cellExtentsDown[1];
                            int j1 = j0 + cellExtentsDown[1] + 1;
                            if (j1 > dims[1]) {
                                j1 = dims[1];
                            }
                            int k0 = k * cellExtentsDown[0];
                            int k1 = k0 + cellExtentsDown[0] + 1;
                            if (k1 > dims[0]) {
                                k1 = dims[0];
                            }
                            for (int m = 0; m < trueNSpace; m++) {
                                cellLow[m] = Float.MAX_VALUE;
                                cellUp[m] = -Float.MAX_VALUE;
                            }
                            for (long ii = i0; ii < i1; ii++) {
                                for (long jj = j0; jj < j1; jj++) {
                                    for (long kk = k0; kk < k1; kk++) {
                                        long m = 3 * ((ii * dims[1] + jj) * dims[0] + kk);
                                        for (int n = 0; n < trueNSpace; n++) {
                                            if (c.getFloat(m + n) > cellUp[n]) {
                                                cellUp[n] = c.getFloat(m + n);
                                            }
                                            if (c.getFloat(m + n) < cellLow[n]) {
                                                cellLow[n] = c.getFloat(m + n);
                                            }
                                        }
                                    }
                                }
                            }
                            for (int m = 0; m < trueNSpace; m++) {
                                cellExtents[m][l] = cellLow[m];
                                cellExtents[m + trueNSpace][l] = cellUp[m];
                            }
                        }
                    }
                }
                break;
            case 2:
                for (int j = 0, l = 0; j < cellExtentsDims[1]; j++) {
                    for (int k = 0; k < cellExtentsDims[0]; k++, l++) {
                        cells[l] = l;
                        int j0 = j * cellExtentsDown[1];
                        int j1 = j0 + cellExtentsDown[1] + 1;
                        if (j1 > dims[1]) {
                            j1 = dims[1];
                        }
                        int k0 = k * cellExtentsDown[0];
                        int k1 = k0 + cellExtentsDown[0] + 1;
                        if (k1 > dims[0]) {
                            k1 = dims[0];
                        }
                        for (int m = 0; m < trueNSpace; m++) {
                            cellLow[m] = Float.MAX_VALUE;
                            cellUp[m] = -Float.MAX_VALUE;
                        }
                        for (long jj = j0; jj < j1; jj++) {
                            for (long kk = k0; kk < k1; kk++) {
                                long m = 3 * (jj * dims[0] + kk);
                                for (int n = 0; n < trueNSpace; n++) {
                                    if (c.getFloat(m + n) > cellUp[n]) {
                                        cellUp[n] = c.getFloat(m + n);
                                    }
                                    if (c.getFloat(m + n) < cellLow[n]) {
                                        cellLow[n] = c.getFloat(m + n);
                                    }
                                }
                            }
                        }
                        for (int m = 0; m < trueNSpace; m++) {
                            cellExtents[m][l] = cellLow[m];
                            cellExtents[m + trueNSpace][l] = cellUp[m];
                        }
                    }
                }
                break;
            case 1:
                for (int k = 0; k < cellExtentsDims[0]; k++) {
                    cells[k] = k;
                    int k0 = k * cellExtentsDown[0];
                    int k1 = k0 + cellExtentsDown[0] + 1;
                    if (k1 > dims[0]) {
                        k1 = dims[0];
                    }
                    for (int m = 0; m < trueNSpace; m++) {
                        cellLow[m] = Float.MAX_VALUE;
                        cellUp[m] = -Float.MAX_VALUE;
                    }
                    for (long kk = k0; kk < k1; kk++) {
                        long m = trueNSpace * kk;
                        for (int n = 0; n < trueNSpace; n++) {
                            if (c.getFloat(m + n) > cellUp[n]) {
                                cellUp[n] = c.getFloat(m + n);
                            }
                            if (c.getFloat(m + n) < cellLow[n]) {
                                cellLow[n] = c.getFloat(m + n);
                            }
                        }
                    }
                    for (int m = 0; m < trueNSpace; m++) {
                        cellExtents[m][k] = cellLow[m];
                        cellExtents[m + trueNSpace][k] = cellUp[m];
                    }
                }
                break;
        }

        geoTree = new GeoTreeNode(trueNSpace, cells, cellExtents);
        geoTree.splitFully();
        timestamp = System.nanoTime();
    }

    private RegularHex[] getCells(int n) {
        int n0 = n % cellExtentsDims[0];
        int k0 = n0 * cellExtentsDown[0];
        int k1 = k0 + cellExtentsDown[0];
        if (k1 > dims[0]) {
            k1 = dims[0];
        }
        int n1 = (n / cellExtentsDims[0]) % cellExtentsDims[1];
        int j0 = n1 * cellExtentsDown[1];
        int j1 = j0 + cellExtentsDown[1];
        if (j1 > dims[1]) {
            j1 = dims[1];
        }
        int n2 = n / (cellExtentsDims[0] * cellExtentsDims[1]);
        int i0 = n2 * cellExtentsDown[2];
        int i1 = i0 + cellExtentsDown[2];
        if (i1 > dims[2]) {
            i1 = dims[2];
        }
        RegularHex[] cells = new RegularHex[(i1 - i0) * (j1 - j0) * (k1 - k0)];
        for (int ii = i0, i = 0; ii < i1; ii++) {
            for (int jj = j0; jj < j1; jj++) {
                for (int kk = k0; kk < k1; kk++, i++) {
                    int m = (ii * dims[1] + jj) * dims[0] + kk;
                    cells[i] = new RegularHex(3, m, m + cellNodeOffsets[1], m + cellNodeOffsets[2], m + cellNodeOffsets[3],
                            m + cellNodeOffsets[4], m + cellNodeOffsets[5], m + cellNodeOffsets[6], m + cellNodeOffsets[7],
                            (byte) 1, ((ii + jj + kk) % 2) == 0);
                }
            }
        }
        return cells;
    }

    private TetrahedronPosition getFieldCoords(float[] p, int[] cells) {
        if (p == null || p.length != 3) {
            throw new IllegalArgumentException("p == null || p.length != 3");
        }

        FloatLargeArray c = (FloatLargeArray) timeCoords.getValue(currentTime);
        cellsLoop:
        for (int i = 0; i < cells.length; i++) {
            int cl = cells[i];
            for (int j = 0; j < 3; j++) {
                if (cellExtents[j][cl] > p[j]) {
                    continue cellsLoop;
                }
                if (cellExtents[j + 3][cl] < p[j]) {
                    continue cellsLoop;
                }
            }
            RegularHex[] boxes = getCells(cl);
            float[] cellLow = new float[3];
            float[] cellUp = new float[3];
            boxesLoop:
            for (RegularHex box : boxes) {
                for (int m = 0; m < 3; m++) {
                    cellLow[m] = Float.MAX_VALUE;
                    cellUp[m] = -Float.MAX_VALUE;
                }
                RegularHex cell = box;
                int[] verts = cell.getVertices();
                for (int k = 0; k < verts.length; k++) {
                    int l = verts[k];
                    for (int n = 0; n < 3; n++) {
                        if (c.getFloat(3 * l + n) > cellUp[n]) {
                            cellUp[n] = c.getFloat(3 * l + n);
                        }
                        if (c.getFloat(3 * l + n) < cellLow[n]) {
                            cellLow[n] = c.getFloat(3 * l + n);
                        }
                    }
                }
                for (int n = 0; n < 3; n++) {
                    if (cellLow[n] > p[n]) {
                        continue boxesLoop;
                    }
                    if (cellUp[n] < p[n]) {
                        continue boxesLoop;
                    }
                }
                Cell[] tets = cell.triangulation();
                for (Cell tet : tets) {
                    TetrahedronPosition result = ((Tetra) tet).barycentricCoords(p, c);
                    if (result != null) {
                        result.setCell(cell);
                        result.setCells(cells);
                        return result;
                    }
                }
            }
        }
        //throw new IllegalArgumentException("Cannot compute field coordinates.");
        return null;
    }

    @Override
    public TetrahedronPosition getFieldCoords(float[] p) {
        return getFieldCoords(p, geoTree.getCells(p));
    }

    @Override
    public boolean getFieldCoords(float[] p, TetrahedronPosition result) {
        FloatLargeArray c = (FloatLargeArray) timeCoords.getValue(currentTime);
        Cell[] tets;
        float[] res;
        res = getBarycentricCoords((Tetra) result.getSimplex(), p);
        if (res != null) {
            result.setVertices(result.getSimplex().getVertices());
            result.setCoords(res);
            return true;
        }
        if (result.getCell() != null && result.getCell().getType() != CellType.TETRA) {
            tets = ((RegularHex) (result.getCell())).triangulation();
            for (int j = 0; j < tets.length; j++) {
                if (tets[j].getType() == CellType.TETRA) {
                    res = getBarycentricCoords((Tetra) tets[j], p);
                    if (res != null) {
                        result.setSimplex(tets[j]);
                        result.setVertices(result.getSimplex().getVertices());
                        result.setCoords(res);
                        return true;
                    }
                }
            }
        }
        if (result.getCells() != null) {
            cLoop:
            for (int i = 0; i < result.getCells().length; i++) {
                int cl = result.getCells()[i];
                for (int j = 0; j < 3; j++) {
                    if (cellExtents[j][cl] > p[j]) {
                        continue cLoop;
                    }
                    if (cellExtents[j + 3][cl] < p[j]) {
                        continue cLoop;
                    }
                }
                RegularHex[] boxes = getCells(cl);
                float[] cellLow = new float[3];
                float[] cellUp = new float[3];
                boxesLoop:
                for (int j = 0; j < boxes.length; j++) {
                    for (int m = 0; m < 3; m++) {
                        cellLow[m] = Float.MAX_VALUE;
                        cellUp[m] = -Float.MAX_VALUE;
                    }
                    RegularHex cell = boxes[j];
                    int[] verts = cell.getVertices();
                    for (int k = 0; k < verts.length; k++) {
                        long l = verts[k];
                        for (int n = 0; n < 3; n++) {
                            if (c.getFloat(3 * l + n) > cellUp[n]) {
                                cellUp[n] = c.getFloat(3 * l + n);
                            }
                            if (c.getFloat(3 * l + n) < cellLow[n]) {
                                cellLow[n] = c.getFloat(3 * l + n);
                            }
                        }
                    }
                    for (int n = 0; n < 3; n++) {
                        if (cellLow[n] > p[n]) {
                            continue boxesLoop;
                        }
                        if (cellUp[n] < p[n]) {
                            continue boxesLoop;
                        }
                    }
                    tets = cell.triangulation();
                    for (int k = 0; k < tets.length; k++) {
                        res = getBarycentricCoords((Tetra) tets[k], p);
                        if (res != null) {
                            result.setSimplex(tets[j]);
                            result.setVertices(result.getSimplex().getVertices());
                            result.setCoords(res);
                            result.setCell(cell);
                            return true;
                        }
                    }
                }
            }
        }
        cLoop:
        for (int i = 0; i < result.getCells().length; i++) {
            int cl = result.getCells()[i];
            for (int j = 0; j < 3; j++) {
                if (cellExtents[j][cl] > p[j]) {
                    continue cLoop;
                }
                if (cellExtents[j + 3][cl] < p[j]) {
                    continue cLoop;
                }
            }
            RegularHex[] boxes = getCells(cl);
            float[] cellLow = new float[3];
            float[] cellUp = new float[3];
            boxesLoop:
            for (int j = 0; j < boxes.length; j++) {
                for (int m = 0; m < 3; m++) {
                    cellLow[m] = Float.MAX_VALUE;
                    cellUp[m] = -Float.MAX_VALUE;
                }
                RegularHex cell = boxes[j];
                int[] verts = cell.getVertices();
                for (int k = 0; k < verts.length; k++) {
                    long l = verts[k];
                    for (int n = 0; n < 3; n++) {
                        if (c.getFloat(3 * l + n) > cellUp[n]) {
                            cellUp[n] = c.getFloat(3 * l + n);
                        }
                        if (c.getFloat(3 * l + n) < cellLow[n]) {
                            cellLow[n] = c.getFloat(3 * l + n);
                        }
                    }
                }
                for (int n = 0; n < 3; n++) {
                    if (cellLow[n] > p[n]) {
                        continue boxesLoop;
                    }
                    if (cellUp[n] < p[n]) {
                        continue boxesLoop;
                    }
                }
                tets = cell.triangulation();
                for (int k = 0; k < tets.length; k++) {
                    res = getBarycentricCoords((Tetra) tets[k], p);
                    if (res != null) {
                        result.setSimplex(tets[j]);
                        result.setVertices(result.getSimplex().getVertices());
                        result.setCoords(res);
                        result.setCell(cell);
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * Returns vertices of tetrahedra of the triangulation of the hexahedral cell with the lowest vertex i.
     *
     * @param i vertex number
     * @return vertices of tetrahedra
     */
    public int[] getTetras(int i) {
        if (dims.length != 3 || i < 0 || i >= (dims[0] - 1) * (dims[1] - 1) * (dims[2] - 1)) {
            throw new IllegalArgumentException("dims.length != 3 || i < 0 || i >= (dims[0] - 1) * (dims[1] - 1) * (dims[2] - 1)");
        }
        int off1 = dims[0] - 1;
        int off2 = (dims[0] - 1) * (dims[1] - 1);
        int i0 = i % off1;
        int i1 = (i / off1) % (dims[1] - 1);
        int i2 = i / off2;
        int l = ((i2 * dims[1]) + i1) * dims[0] + i0;
        off1 = dims[0];
        off2 = dims[0] * dims[1];
        return triangulateRegularHex(
                l, l + 1, l + off1 + 1, l + off1,
                l + off2, l + off2 + 1, l + off2 + off1 + 1, l + off2 + off1,
                (i0 + i1 + i2) % 2 == 0);
    }

    /**
     * Returns vertices of triangles of the triangulation of the quadrilateral cell with the lowest vertex i.
     *
     * @param i vertex number
     * @return vertices of triangles
     */
    public int[] getTriangles(int i) {
        if (dims.length != 2 || i < 0 || i >= (dims[0] - 1) * (dims[1] - 1)) {
            throw new IllegalArgumentException("dims.length != 2 || i < 0 || i >= (dims[0] - 1) * (dims[1] - 1)");
        }
        int off = dims[0] - 1;
        int i0 = i % off;
        int i1 = i / off;
        int l = i1 * dims[0] + i0;
        return triangulateRegularQuad(l, l + 1, l + dims[0] + 1, l + dims[0]);
    }

    @Override
    public IrregularField getTriangulated() {
        IrregularField outField = new IrregularField(nElements);
        if (timeCoords != null && !timeCoords.isEmpty()) {
            outField.setCoords(timeCoords);
        } else {
            outField.setCurrentCoords(getCoordsFromAffine());
        }
        if (timeMask != null) {
            outField.setMask(timeMask);
        }
        CellArray ca = null;
        if (dims.length == 3) {
            int off1 = dims[0];
            int off2 = dims[0] * dims[1];
            int[] cellNodes = new int[20 * (dims[0] - 1) * (dims[1] - 1) * (dims[2] - 1)];
            for (int i = 0, m = 0; i < dims[2] - 1; i++) {
                for (int j = 0; j < dims[1] - 1; j++) {
                    for (int k = 0, l = i * off2 + j * off1; k < dims[0] - 1; k++, l++, m += 20) {
                        System.arraycopy(triangulateRegularHex(
                                l, l + 1, l + off1 + 1, l + off1,
                                l + off2, l + off2 + 1, l + off2 + off1 + 1, l + off2 + off1,
                                (i + j + k) % 2 == 0), 0, cellNodes, m, 20);
                    }
                }
            }
            byte[] orient = new byte[5 * (dims[0] - 1) * (dims[1] - 1) * (dims[2] - 1)];
            for (int i = 0; i < orient.length; i++) {
                orient[i] = 1;
            }
            ca = new CellArray(CellType.TETRA, cellNodes, orient, null);
        } else if (dims.length == 2) {
            int off1 = dims[0];
            int[] cellNodes = new int[6 * (dims[0] - 1) * (dims[1] - 1)];
            for (int j = 0, m = 0; j < dims[1] - 1; j++) {
                for (int k = 0, l = j * off1; k < dims[0] - 1; k++, l++, m += 6) {
                    System.arraycopy(triangulateRegularQuad(
                            l, l + 1, l + off1 + 1, l + off1), 0, cellNodes, m, 6);
                }
            }
            byte[] orient = new byte[2 * (dims[0] - 1) * (dims[1] - 1)];
            for (int i = 0; i < orient.length; i++) {
                orient[i] = 1;
            }
            ca = new CellArray(CellType.TRIANGLE, cellNodes, orient, null);
        }
        CellSet cs = new CellSet();
        cs.setCellArray(ca);
        cs.generateDisplayData(outField.getCurrentCoords());
        outField.addCellSet(cs);
        for (DataArray dataArray : components) {
            outField.addComponent(dataArray.cloneShallow());
        }
        outField.setExtents(getExtents());
        return outField;
    }

    @Override
    public LongLargeArray getIndices(int axis) {
        if (axis < 0 || axis > dims.length) {
            throw new IllegalArgumentException("axis < 0 || axis > dims.length");
        }

        LongLargeArray out = new LongLargeArray(nElements, false);
        switch (dims.length) {
            case 1:
                for (long i = 0; i < dims[0]; i++) {
                    out.setLong(i, i);
                }
                break;
            case 2:
                switch (axis) {
                    case 0:
                        for (long j = 0, l = 0; j < dims[1]; j++) {
                            for (long i = 0; i < dims[0]; i++, l++) {
                                out.setLong(l, i);
                            }
                        }
                        break;
                    case 1:
                        for (long j = 0, l = 0; j < dims[1]; j++) {
                            for (long i = 0; i < dims[0]; i++, l++) {
                                out.setLong(l, j);
                            }
                        }
                        break;
                }
                break;
            case 3:
                switch (axis) {
                    case 0:
                        for (long k = 0, l = 0; k < dims[2]; k++) {
                            for (long j = 0; j < dims[1]; j++) {
                                for (long i = 0; i < dims[0]; i++, l++) {
                                    out.setLong(l, i);
                                }
                            }
                        }
                        break;
                    case 1:
                        for (long k = 0, l = 0; k < dims[2]; k++) {
                            for (long j = 0; j < dims[1]; j++) {
                                for (long i = 0; i < dims[0]; i++, l++) {
                                    out.setLong(l, j);
                                }
                            }
                        }
                        break;
                    case 2:
                        for (long k = 0, l = 0; k < dims[2]; k++) {
                            for (long j = 0; j < dims[1]; j++) {
                                for (long i = 0; i < dims[0]; i++, l++) {
                                    out.setLong(l, k);
                                }
                            }
                        }
                        break;
                }
                break;
        }
        return out;
    }

    private static int[] triangulateRegularHex(int i0, int i1, int i2, int i3, int i4, int i5, int i6, int i7, boolean even) {
        if (even) {
            return new int[]{
                i0, i2, i7, i5,
                i1, i2, i0, i5,
                i3, i0, i2, i7,
                i6, i5, i7, i2,
                i5, i4, i7, i0
            };
        } else {
            return new int[]{
                i1, i3, i4, i6,
                i0, i1, i3, i4,
                i2, i3, i1, i6,
                i4, i5, i1, i6,
                i7, i6, i4, i3
            };
        }
    }

    private static int[] triangulateRegularQuad(int i0, int i1, int i2, int i3) {
        return new int[]{
            i0, i1, i2,
            i0, i2, i3
        };
    }

}
