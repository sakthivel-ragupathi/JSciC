/* ***** BEGIN LICENSE BLOCK *****
 * Java Scientific Containers (JSciC)
 * Copyright (C) 2015 onward University of Warsaw, ICM
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * ***** END LICENSE BLOCK ***** */

package pl.edu.icm.jscic.dataarrays;

import java.io.Serializable;
import java.util.Arrays;
import pl.edu.icm.jscic.utils.EngineeringFormattingUtils;
import pl.edu.icm.jscic.utils.FloatingPointUtils;
import pl.edu.icm.jscic.utils.ScalarMath;

/**
 *
 * Holds general information about a DataArray without DataArray values. Can
 * be used for compatibility checking and field data schema comparison.
 * 
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 * @author Bartosz Borucki, University of Warsaw, ICM
 */
public class DataArraySchema implements Serializable
{

    /**
     * Data array name.
     */
    private String name;
    /**
     * String describing data physical unit, e.g. "m/sec", "hPa" etc. It can be
     * used in legend/axes labeling and data computation modules to ensure
     * physical correctness of computations.
     */
    private String unit;
    /**
     * Data array type - currently one of standard Java computational
     * types, logic, complex, string and object
     */
    private DataArrayType type;
    /**
     * 1 if data are scalar (default) generally, vector length of an individual
     * data item. The length of the array storing the elements is equal to nelements*veclen.
     */
    private int veclen;
    /**
     * Number of elements stored in a DataArray. The length of the array
     * storing the elements is equal to nelements*veclen.
     */
    private long nelements;

    /**
     * Dimensions of the individual data item as an array initialized to a
     * one-dimensional array with veclen as the only dimension. When the
     * symmetric flag is set to false, product of all dimensions must be equal to
     * veclen, otherwise matrixDims must be of the form {n,n} and
     * veclen=nx(n+1)/2;
     */
    private int[] matrixDims;
    /**
     * Flag indicating that each data item represents a symmetric array.
     * Elements are stored in the order: {m00,m01,...,m0n,m11,...,m1n,...,mnn}
     * checked only when matrixDims.length = 2, matrixDims = {n,n} and
     * veclen=nx(n+1)/2
     */
    private boolean symmetric = false;

    /**
     * Minimum of data values.
     */
    private double min;

    /**
     * Maximum of data values.
     */
    private double max;

    /**
     * Preferred minimum of data values (can be different than min value). It can be
     * explicitly set by the user. In the case of vector data preferredMin = 0.
     */
    private double preferredMin;
    /**
     * Preferred maximum of data values (can be different than max value). Can be
     * explicitly set by the user.
     */
    protected double preferredMax;

    /**
     * Coefficients for linear mapping to physical minimum and maximum.
     */
    private double[] physMappingCoeffs;

    /**
     * Mean of data values.
     */
    private double mean;

    /**
     * Mean of squared data values.
     */
    private double mean2;

    /**
     * Standard deviation of data values.
     */
    private double sd;

    /**
     * User annotations for data meaning currently supported: "RGBCOLOR" when
     * 3-vector bytes are interpreted as RGB color components "RGBACOLOR" when
     * 4-vector bytes are interpreted as RGB color components with alpha
     * "MAP" followed by a series of &lt;value&gt;:&lt;name&gt; strings for data
     * showing position of objects in space.
     */
    private String[] userData;

    /**
     * A hint for data mapping methods. If true, then the low and up bounds for data mapping are automatically set to physical min and physical max values when
     * array is refreshed.
     */
    private boolean autoResetMapRange = false;

    /**
     * A flag storing the status of statistical values.
     */
    private boolean statisticsComputed;

    /**
     * If true then the DataArray is constant (read-only).
     */
    private boolean constant = false;

    /**
     * The comprehensive constructor setting all fields values.
     * 
     * @param name              user name for DataArray
     * @param unit              physical units of data items
     * @param userData          arbitrary String array to be used in modules (e.g.
     *                          {"map", "value":"name", "value":"name", ...}
     * @param type              one of DataArraySchema.FIELD_DATA_LOGIC ...
     *                          DataArraySchema.FIELD_DATA_OBJECT
     * @param nelements         number of elements
     * @param veclen            number of data components at each element
     * @param matrixDims        dimensions of the individual data item as an array
     * @param symmetric         indicates that each data element is a symmetric array
     * @param constant          indicates that the DataArray is constant (read-only)
     * @param min               minimum of data values
     * @param max               maximum of data values
     * @param preferredMin      preferred minimum of data values (0 for vector data)
     * @param preferredMax      preferred maximum of data values
     * @param physMin           minimum of physical data values
     * @param physMax           maximum of physical data values
     * @param mean              mean of data values
     * @param mean2             squared mean of data values
     * @param sd                standard deviation of data values
     * @param autoResetMapRange hint for data mapping methods
     */
    public DataArraySchema(String name, String unit, String[] userData,
                           DataArrayType type, long nelements, int veclen, int[] matrixDims, boolean symmetric, boolean constant,
                           double min, double max, double preferredMin, double preferredMax,
                           double physMin, double physMax, double mean, double mean2, double sd, boolean autoResetMapRange)
    {
        if (type == DataArrayType.FIELD_DATA_UNKNOWN) {
            throw new IllegalArgumentException("Unsupported array type");
        }

        if (name.isEmpty()) {
            throw new IllegalArgumentException("Component name cannot be empty");
        }

        if (veclen <= 0) {
            throw new IllegalArgumentException("veclen has to be positive.");
        }

        if (nelements <= 0) {
            throw new IllegalArgumentException("nelements has to be positive.");
        }

        if (veclen > 1 && constant == true) {
            throw new IllegalArgumentException("Cannot create constant DataArray with veclen > 1.");
        }
        if (unit == null) {
            unit = "";
        }

        this.name = correctDataArrayName(name);
        this.unit = unit;
        this.userData = userData;
        this.type = type;
        this.nelements = nelements;
        this.veclen = veclen;
        this.constant = constant;
        int k = 1;
        for (int i = 0; i < matrixDims.length; i++) {
            k *= matrixDims[i];
        }
        if (k == veclen || (matrixDims.length == 2 && matrixDims[0] == matrixDims[1] && veclen == (matrixDims[0] * (matrixDims[0] + 1)) / 2 && symmetric)) {
            this.matrixDims = matrixDims;
            this.symmetric = symmetric;
        }

        this.min = FloatingPointUtils.processNaNs(min, FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction);
        this.max = FloatingPointUtils.processNaNs(max, FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction);
        this.preferredMin = FloatingPointUtils.processNaNs(preferredMin, FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction);
        this.preferredMax = FloatingPointUtils.processNaNs(preferredMax, FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction);
        this.physMappingCoeffs = ScalarMath.linearMappingCoefficients(this.preferredMin, this.preferredMax, FloatingPointUtils.processNaNs(physMin, FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), FloatingPointUtils.processNaNs(physMax, FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction));
        this.mean = FloatingPointUtils.processNaNs(mean, FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction);
        this.mean2 = FloatingPointUtils.processNaNs(mean2, FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction);
        this.sd = FloatingPointUtils.processNaNs(sd, FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction);
        this.autoResetMapRange = autoResetMapRange;
        this.statisticsComputed = false;
    }

    /**
     * The comprehensive constructor setting almost all fields values.
     * 
     * @param name              user name for DataArray
     * @param unit              physical units of data items
     * @param userData          arbitrary String array to be used in modules (e.g.
     *                          {"map", "value":"name", "value":"name", ...}
     * @param type              one of DataArrayType.FIELD_DATA_LOGIC ...
     *                          DataArrayType.FIELD_DATA_OBJECT
     * @param nelements         number of elements
     * @param veclen            number of data components at each element
     * @param constant          indicates that the DataArray is constant (read-only)
     * @param min               minimum of data values
     * @param max               maximum of data values
     * @param preferredMin      preferred minimum of data values (0 for vector data)
     * @param preferredMax      preferred maximum of data values
     * @param physMin           minimum of physical data values
     * @param physMax           maximum of physical data values
     * @param mean              mean of data values
     * @param mean2             squared mean of data values
     * @param sd                standard deviation of data values
     * @param autoResetMapRange hint for data mapping methods
     */
    public DataArraySchema(String name, String unit, String[] userData,
                           DataArrayType type, long nelements, int veclen, boolean constant,
                           double min, double max, double preferredMin, double preferredMax,
                           double physMin, double physMax, double mean, double mean2, double sd, boolean autoResetMapRange)
    {
        this(name, unit, userData, type, nelements, veclen, new int[]{veclen}, false, constant, min, max, preferredMin, preferredMax, physMin, physMax, mean, mean2, sd, autoResetMapRange);
    }

    /**
     * A basic constructor setting required fields values.
     * 
     * @param name      user name for DataArray
     * @param unit      physical units of data items
     * @param userData  arbitrary String array to be used in modules (e.g.
     *                  {"map", "value":"name", "value":"name", ...}
     * @param type      one of DataArrayType.FIELD_DATA_LOGIC ...
     *                  DataArrayType.FIELD_DATA_OBJECT
     * @param nelements number of elements
     * @param veclen    number of data components at each element
     * @param constant  indicates that the DataArray is constant (read-only)
     */
    public DataArraySchema(String name, String unit, String[] userData, DataArrayType type, long nelements, int veclen, boolean constant)
    {
        this(name, unit, userData, type, nelements, veclen, constant, FloatingPointUtils.MIN_NUMBER_DOUBLE, FloatingPointUtils.MAX_NUMBER_DOUBLE, FloatingPointUtils.MIN_NUMBER_DOUBLE, FloatingPointUtils.MAX_NUMBER_DOUBLE, FloatingPointUtils.MIN_NUMBER_DOUBLE, FloatingPointUtils.MAX_NUMBER_DOUBLE, 0, 0, 0, false);
        this.statisticsComputed = false;
    }

    /**
     * A basic constructor setting required fields values.
     * 
     * @param name      user name for DataArray
     * @param type      one of DataArrayType.FIELD_DATA_LOGIC ...
     *                  DataArrayType.FIELD_DATA_OBJECT
     * @param nelements number of elements
     * @param veclen    number of data components at each element
     */
    public DataArraySchema(String name, DataArrayType type, long nelements, int veclen)
    {
        this(name, "", null, type, nelements, veclen, false);
    }

    /**
     * A basic constructor setting required fields values.
     * 
     * @param name      user name for DataArray
     * @param type      one of DataArrayType.FIELD_DATA_LOGIC ...
     *                  DataArrayType.FIELD_DATA_OBJECT
     * @param nelements number of elements
     * @param veclen    number of data components at each element
     * @param constant  indicates that the DataArray is constant (read-only)
     */
    public DataArraySchema(String name, DataArrayType type, long nelements, int veclen, boolean constant)
    {
        this(name, "", null, type, nelements, veclen, constant);
    }

    /**
     * Returns the type of DataArray.
     * 
     * @return type of DataArray
     */
    public String getTypeName()
    {
        return type.toString();
    }

    @Override
    public String toString()
    {
        if (veclen == 1) {
            return name + " scalar " + getTypeName();
        } else {
            return name + " " + veclen + "-vector " + getTypeName();
        }
    }

    /**
     * Restores the schema from the string generated by exportSchemaToString().
     * 
     * @param description string generated by exportSchemaToString()
     * 
     * @return DataArray schema
     */
    public static DataArraySchema restoreSchemaFromString(String description)
    {
        DataArraySchema s = null;
        String[] items = description.split(" *:* +");
        try {
            s = new DataArraySchema(items[0], DataArrayType.getType(Integer.parseInt(items[1])),
                                    Long.parseLong(items[3]), Integer.parseInt(items[5]), false);
            s.setPreferredRanges(Double.parseDouble(items[7]), Double.parseDouble(items[8]),
                                 Double.parseDouble(items[10]), Double.parseDouble(items[11]));
        } catch (Exception e) {
        }
        return s;
    }

    /**
     * Returns string representation of the schema.
     * 
     * @return string representation of the schema
     */
    public String exportSchemaToString()
    {
        return name + ": " + getTypeName() + " nelements: " + nelements + " veclen: " + veclen +
            " range: " + EngineeringFormattingUtils.format(getPreferredMinValue()) + " " +
            EngineeringFormattingUtils.format(getPreferredMaxValue()) +
            " phys:" + EngineeringFormattingUtils.format(getPreferredPhysMinValue()) + " " +
            EngineeringFormattingUtils.format(getPreferredPhysMaxValue());
    }

    /**
     * Returns schema description in the form of HTML table.
     * 
     * @return schema description in the form of HTML table
     */
    public String description()
    {
        return "<TR>" + name + "<TD>" + veclen + "</TD><TD>" + getTypeName() +
            String.format("</TD><TD>%6.3f</TD><TD>%6.3f</TD><TD>%6.3f</TD><TD>%6.3f</TD>><TD>%6.3f</TD><TD>%6.3f</TD></TR>",
                          getMinValue(), getMaxValue(), getPreferredMinValue(), getPreferredMaxValue(), getPreferredPhysMinValue(), getPreferredPhysMaxValue());
    }

    /**
     * Returns maximum value.
     * 
     * @return maximum value
     */
    public double getMaxValue()
    {
        return max;
    }

    /**
     * Sets minimum and maximum values.
     * 
     * @param min new value for min
     * @param max new value for max
     */
    public void setMinMaxValues(double min, double max)
    {
        this.min = FloatingPointUtils.processNaNs(min, FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction);
        this.max = FloatingPointUtils.processNaNs(max, FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction);
        if (this.max < this.min) throw new IllegalArgumentException("max < min");
    }

    /**
     * Returns preferred maximum value.
     * 
     * @return preferred maximum
     */
    public double getPreferredMaxValue()
    {
        return preferredMax;
    }

    /**
     * Returns minimum value
     * 
     * @return minimum value
     */
    public double getMinValue()
    {
        return min;
    }

    /**
     * Returns preferred minimum value.
     * 
     * @return preferred minimum
     */
    public double getPreferredMinValue()
    {
        return preferredMin;
    }

    /**
     * Sets the preferred minimum and preferred maximum values.
     * 
     * @param preferredMin preferred minimum
     * @param preferredMax preferred maximum
     */
    public void setPreferredRange(double preferredMin, double preferredMax)
    {
        this.preferredMin = FloatingPointUtils.processNaNs(preferredMin, FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction);
        this.preferredMax = FloatingPointUtils.processNaNs(preferredMax, FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction);
        if (this.preferredMax <= this.preferredMin) {
            throw new IllegalArgumentException("preferredMax <= preferredMin");
        }
    }

    /**
     * Sets the preferred minimum and maximum values as well as preferred physical minimum and maximum values. Linear mapping coefficients between preferred
     * range and preferred physical range are computed and stored instead of preferredPhysMin and preferredPhysMax values.
     *
     * @param preferredMin     preferred minimum
     * @param preferredMax     preferred maximum
     * @param preferredPhysMin preferred physical minimum
     * @param preferredPhysMax preferred physical maximum
     */
    public void setPreferredRanges(double preferredMin, double preferredMax, double preferredPhysMin, double preferredPhysMax)
    {
        this.preferredMin = FloatingPointUtils.processNaNs(preferredMin, FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction);
        this.preferredMax = FloatingPointUtils.processNaNs(preferredMax, FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction);
        if (this.preferredMax <= this.preferredMin) {
            throw new IllegalArgumentException("preferredMax <= preferredMin");
        }
        this.physMappingCoeffs = ScalarMath.linearMappingCoefficients(this.preferredMin, this.preferredMax, FloatingPointUtils.processNaNs(preferredPhysMin, FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), FloatingPointUtils.processNaNs(preferredPhysMax, FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction));
        if (this.physMappingCoeffs[0] < 0) {
            throw new IllegalArgumentException("physMappingCoeffs[0] < 0");
        }
    }

    /**
     * Returns mean value.
     * 
     * @return mean value
     */
    public double getMeanValue()
    {
        return mean;
    }

    /**
     * Sets mean value.
     * 
     * @param mean new mean value
     */
    public void setMeanValue(double mean)
    {
        this.mean = FloatingPointUtils.processNaNs(mean, FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction);
    }

    /**
     * Returns mean of squares.
     * 
     * @return mean of squares
     */
    public double getMeanSquaredValue()
    {
        return mean2;
    }

    /**
     * Sets mean of squares value.
     * 
     * @param mean2 new mean of squares value
     */
    public void setMeanSquaredValue(double mean2)
    {
        this.mean2 = FloatingPointUtils.processNaNs(mean2, FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction);
    }

    /**
     * Returns standard deviation value.
     * 
     * @return standard deviation
     */
    public double getStandardDeviationValue()
    {
        return sd;
    }

    /**
     * Sets standard deviation value.
     * 
     * @param sd new standard deviation value
     */
    public void setStandardDeviationValue(double sd)
    {
        this.sd = FloatingPointUtils.processNaNs(sd, FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction);
    }

    /**
     * Return name of DataArray; by default name is an empty string.
     * 
     * @return name of DataArray
     */
    public String getName()
    {
        return name;
    }

    /**
     * Sets name value.
     * 
     * @param name new name value
     */
    public void setName(String name)
    {
        this.name = name.replaceAll("\\W", "_");
        if (name.isEmpty()) {
            throw new IllegalArgumentException("Component name cannot be empty");
        }
    }

    private boolean isNameValid(String name)
    {
        return true;
        //TODO: we should limit acceptable characters 
        //return name.matches("[a-zA-Z_]+(\\w|_)*");
    }

    /**
     * Returns physical maximum value.
     * 
     * @return physical maximum value
     */
    public double getPhysMaxValue()
    {
        return dataRawToPhys(getMaxValue());
    }

    /**
     * Returns preferred physical maximum value.
     * 
     * @return preferred physical maximum value
     */
    public double getPreferredPhysMaxValue()
    {
        return dataRawToPhys(getPreferredMaxValue());
    }

    /**
     * Converts raw value to physical value using linear mapping coefficients.
     * 
     * @param val raw value
     * 
     * @return physical value
     */
    public double dataRawToPhys(double val)
    {
        return physMappingCoeffs[0] * val + physMappingCoeffs[1];
    }

    /**
     * Converts physical value to raw value using linear mapping coefficients.
     * 
     * @param val physical value
     * 
     * @return raw value
     */
    public double dataPhysToRaw(double val)
    {
        return 1. / physMappingCoeffs[0] * val - physMappingCoeffs[1] / physMappingCoeffs[0];
    }

    /**
     * Returns physical minimum.
     * 
     * @return physical minimum
     */
    public double getPhysMinValue()
    {
        return dataRawToPhys(getMinValue());
    }

    /**
     * Returns preferred physical minimum.
     * 
     * @return preferred physical minimum
     */
    public double getPreferredPhysMinValue()
    {
        return dataRawToPhys(getPreferredMinValue());
    }

    /**
     * Returns type of the array.
     * 
     * @return type of array
     */
    public DataArrayType getType()
    {
        return type;
    }

    /**
     * Sets type of the array.
     * 
     * @param type type of array
     */
    public void setType(DataArrayType type)
    {
        this.type = type;
    }

    /**
     * Return true if schema is of numeric type, false otherwise.
     * 
     * @return true if schema is of numeric type, false otherwise
     */
    public boolean isNumeric()
    {
        return type == DataArrayType.FIELD_DATA_LOGIC ||
            type == DataArrayType.FIELD_DATA_BYTE ||
            type == DataArrayType.FIELD_DATA_SHORT ||
            type == DataArrayType.FIELD_DATA_INT ||
            type == DataArrayType.FIELD_DATA_LONG ||
            type == DataArrayType.FIELD_DATA_FLOAT ||
            type == DataArrayType.FIELD_DATA_DOUBLE ||
            type == DataArrayType.FIELD_DATA_COMPLEX;
    }

    /**
     * Returns unit of the array.
     * 
     * @return unit of the array
     */
    public String getUnit()
    {
        return unit;
    }

    /**
     * Sets unit of the array.
     * 
     * @param unit unit of the array
     */
    public void setUnit(String unit)
    {
        this.unit = unit;
    }

    /**
     * Returns the vector length.
     * 
     * @return	vector length
     */
    public int getVectorLength()
    {
        return veclen;
    }

    /**
     * Returns the number of elements.
     *
     * @return number of elements
     */
    public long getNElements()
    {
        return nelements;
    }

    /**
     *
     * Sets the vector length.
     * 
     * @param	veclen	vector length.
     */
    public void setVectorLength(int veclen)
    {
        this.veclen = veclen;
    }

    /**
     *
     * Sets the number of elements
     * 
     * @param	nelements number of elements.
     */
    public void setNElements(long nelements)
    {
        this.nelements = nelements;
    }

    /**
     *
     * Returns the dimensions of each matrix element of the DataArray.
     *
     * @return	the dimensions of each matrix element of the DataArray
     */
    public int[] getMatrixDims()
    {
        return matrixDims;
    }

    /**
     *
     * Returns true if each element of the DataArray is a symmetric matrix, false otherwise,
     *
     * @return	true if each element of the DataArray is a symmetric matrix, false otherwise
     */
    public boolean isSymmetric()
    {
        return symmetric;
    }

    /**
     *
     * Sets the matrix dimensions and symmetric flag.
     *
     * @param	matrixDims - dimensions of each element off the DataArray.
     * @param symmetric  - symmetric array indicator checks for parameter
     *                   compatibility
     */
    public void setMatrixProperties(int[] matrixDims, boolean symmetric)
    {
        int k = 1;
        for (int i = 0; i < matrixDims.length; i++) {
            k *= matrixDims[i];
        }
        if (k == veclen || (matrixDims.length == 2 && matrixDims[0] == matrixDims[1] && veclen == (matrixDims[0] * (matrixDims[0] + 1)) / 2 && symmetric)) {
            this.matrixDims = matrixDims;
            this.symmetric = symmetric;
        }
    }

    /**
     * Checks compatibility of two DataArraySchemas.
     * 
     * @param s                    DataArraySchema to be checked for compatibility
     * @param checkComponentNames  flag to include components name checking
     * @param checkComponentRanges flag to include components range checking
     * 
     * @return if checkComponentNames and checkComponentRanges: true if name,
     *         type, veclen, units and data range of s are equal to this
     * if checkComponentNames true if name, type, veclen and units of s are
     * equal to this
     * if checkComponentRanges true if type, veclen and data range of s are
     * equal to this
     * otherwise, true if type and veclen are equal to this
     */
    public boolean isCompatibleWith(DataArraySchema s, boolean checkComponentNames, boolean checkComponentRanges)
    {
        boolean compat;
        if (checkComponentNames) {
            compat = type == s.getType() && veclen == s.getVectorLength() && nelements == s.getNElements() &&
                ((name == null && s.getName() == null) ||
                (name != null && s.getName() != null && name.equals(s.getName()))) &&
                ((unit == null && s.getUnit() == null) ||
                (unit != null && s.getUnit() != null && unit.equals(s.getUnit()))) &&
                ((matrixDims == null && s.getMatrixDims() == null) ||
                (matrixDims != null && s.getMatrixDims() != null && Arrays.equals(matrixDims, s.getMatrixDims()))) &&
                symmetric == s.isSymmetric();
        } else {
            compat = type == s.getType() && veclen == s.getVectorLength() && nelements == s.getNElements() &&
                ((unit == null && s.getUnit() == null) ||
                (unit != null && s.getUnit() != null && unit.equals(s.getUnit()))) &&
                ((matrixDims == null && s.getMatrixDims() == null) ||
                (matrixDims != null && s.getMatrixDims() != null && Arrays.equals(matrixDims, s.getMatrixDims()))) &&
                symmetric == s.isSymmetric();

        }
        if (!compat) {
            return false;
        }
        if (checkComponentRanges) {
            if (statisticsComputed == true && s.isStatisticsComputed() == true) {
                return preferredMin == s.getPreferredMinValue() && preferredMax == s.getPreferredMaxValue();
            } else {
                return false;
            }
        } else {
            return true;
        }
    }

    /**
     * Checks compatibility of two DataArraySchemas.
     * 
     * @param s                   DataArraySchema to be checked for compatibility
     * @param checkComponentNames flag to include components name checking
     * 
     * @return true if name, type, veclen and units of s are compatible
     */
    public boolean isCompatibleWith(DataArraySchema s, boolean checkComponentNames)
    {
        boolean compat = type == s.getType() && veclen == s.getVectorLength() && nelements == s.getNElements() &&
            ((unit == null && s.getUnit() == null) ||
            (unit != null && s.getUnit() != null && unit.equals(s.getUnit()))) &&
            ((matrixDims == null && s.getMatrixDims() == null) ||
            (matrixDims != null && s.getMatrixDims() != null && Arrays.equals(matrixDims, s.getMatrixDims()))) &&
            symmetric == s.isSymmetric();

        if (checkComponentNames) {
            return compat && ((name == null && s.getName() == null) || (name != null && s.getName() != null && name.equals(s.getName())));
        } else {
            return compat;
        }
    }

    /**
     * Checks compatibility of two DataArraySchemas.
     * 
     * @param s DataArraySchema to be checked for compatibility
     * 
     * @return true if name, type, veclen and units of s are compatible
     */
    public boolean isCompatibleWith(DataArraySchema s)
    {
        return DataArraySchema.this.isCompatibleWith(s, true);
    }

    /**
     * Returns the user data. This method always returns a reference to the internal DataArray.
     *
     * @return user data
     */
    public String[] getUserData()
    {
        return userData;
    }

    /**
     * Sets the user data.
     *
     * @param userData new value of user data
     */
    public void setUserData(String[] userData)
    {
        this.userData = userData;
    }

    /**
     * Returns the user data at specified index.
     *
     * @param index index
     * 
     * @return user data at specified index
     */
    public String getUserData(int index)
    {
        if (userData == null || index < 0 || index >= userData.length) {
            return "";
        }
        return this.userData[index];
    }

    /**
     * Sets the value of user data at specified index.
     *
     * @param index       index
     * @param newUserData new value of user data at specified index
     */
    public void setUserData(int index, String newUserData)
    {
        this.userData[index] = newUserData;
    }

    /**
     * Returns the status of statistical values: true if statistics are up to date, false otherwise.
     *
     * @return true if statistics are up to date, false otherwise
     */
    public boolean isStatisticsComputed()
    {
        return statisticsComputed;
    }

    /**
     * Sets the status of statistical values.
     *
     * @param newStatisticsComputed new value of the status of statistical values
     */
    public void setStatisticsComputed(boolean newStatisticsComputed)
    {
        this.statisticsComputed = newStatisticsComputed;
    }

    /**
     * Returns true if the array is constant, false otherwise.
     *
     * @return true if the array is constant, false otherwise
     */
    public boolean isConstant()
    {
        return constant;
    }

    /**
     * Sets the constant status.
     *
     * @param newConstant new value of the constant status
     */
    public void setConstant(boolean newConstant)
    {
        this.constant = newConstant;
    }

    /**
     * Returns the hint for data mapping methods. If true, then the low and up bounds for data mapping are automatically set to physical min and physical max
     * values when array is refreshed.
     *
     * @return hint for data mapping methods
     */
    public boolean isAutoResetMapRange()
    {
        return autoResetMapRange;
    }

    /**
     * Sets the hint for data mapping methods. If true, then the low and up bounds for data mapping are automatically set to physical min and physical max
     * values when array is refreshed.
     *
     * @param autoResetMapRange new value of the hint for data mapping methods
     */
    public void setAutoResetMapRange(boolean autoResetMapRange)
    {
        this.autoResetMapRange = autoResetMapRange;
    }

    /**
     * Returns a deep copy of this DataArraySchema instance.
     *
     * @return deep copy of this DataArraySchema instance.
     */
    public DataArraySchema cloneDeep()
    {
        String[] userDataClone = null;
        if (userData != null) {
            userDataClone = new String[userData.length];
            System.arraycopy(userData, 0, userDataClone, 0, userData.length);
        }
        int[] matrixDimsClone = null;
        if (matrixDims != null) {
            matrixDimsClone = matrixDims.clone();
        }

        DataArraySchema clone = new DataArraySchema(name, unit, userDataClone, type, nelements, veclen, constant);
        clone.matrixDims = matrixDimsClone;
        clone.symmetric = symmetric;
        clone.min = min;
        clone.max = max;
        clone.preferredMin = preferredMin;
        clone.preferredMax = preferredMax;
        clone.physMappingCoeffs = physMappingCoeffs.clone();
        clone.mean = mean;
        clone.mean2 = mean2;
        clone.sd = sd;
        clone.statisticsComputed = statisticsComputed;
        return clone;
    }

    /**
     * Replaces all unsupported characters in the given string string by "_".
     * 
     * @param originalName DataArray name
     * 
     * @return DataArray name with all unsupported characters replaced by "_"
     */
    public static String correctDataArrayName(String originalName)
    {
        return originalName.replaceAll("\\W", "_");
    }
}
