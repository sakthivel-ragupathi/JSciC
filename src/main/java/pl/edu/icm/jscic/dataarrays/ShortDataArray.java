/* ***** BEGIN LICENSE BLOCK *****
 * Java Scientific Containers (JSciC)
 * Copyright (C) 2015 onward University of Warsaw, ICM
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * ***** END LICENSE BLOCK ***** */

package pl.edu.icm.jscic.dataarrays;

import pl.edu.icm.jscic.TimeData;
import pl.edu.icm.jlargearrays.ShortLargeArray;

/**
 *
 * DataArray that stores short elements.
 * 
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 * @author Bartosz Borucki, University of Warsaw, ICM
 */
public class ShortDataArray extends DataArray
{

    private static final long serialVersionUID = -2321067486048746850L;

    /**
     * Creates a new instance of ShortDataArray.
     *
     * @param	schema	DataArray schema.
     */
    public ShortDataArray(DataArraySchema schema)
    {
        super(schema);
        if (schema.getType() != DataArrayType.FIELD_DATA_SHORT) {
            throw new IllegalArgumentException("Schema type does not match array type.");
        }
        timeData = new TimeData(DataArrayType.FIELD_DATA_SHORT);
    }

    /**
     * Creates a new instance of constant ShortDataArray.
     *
     * @param ndata number of data elements in the ShortDataArray
     * @param value constant value
     */
    public ShortDataArray(long ndata, Short value)
    {
        super(DataArrayType.FIELD_DATA_SHORT, ndata, true);
        this.data = new ShortLargeArray(ndata, value);
        timeData = new TimeData(DataArrayType.FIELD_DATA_SHORT);
        timeData.addValue(data);
        recomputeStatistics();
    }

    /**
     * Creates a new instance of ShortDataArray.
     *
     * @param ndata  number of data elements in the ShortDataArray
     * @param veclen vector length (1 for scalar data)
     */
    public ShortDataArray(long ndata, int veclen)
    {
        super(DataArrayType.FIELD_DATA_SHORT, ndata, veclen);
        timeData = new TimeData(DataArrayType.FIELD_DATA_SHORT);
    }

    /**
     * Creates a new instance of ShortDataArray.
     *
     * @param data   short array to be included in the generated ShortDataArray
     * @param schema DataArray schema
     */
    public ShortDataArray(ShortLargeArray data, DataArraySchema schema)
    {
        super(schema);
        if (schema.getType() != DataArrayType.FIELD_DATA_SHORT) {
            throw new IllegalArgumentException("Schema type does not match array type.");
        }

        if (schema.getNElements() != data.length() / schema.getVectorLength()) {
            throw new IllegalArgumentException("Schema does not match array length.");
        }
        if(schema.isConstant() != data.isConstant()) {
            throw new IllegalArgumentException("schema.isConstant() != data.isConstant()");
        }

        this.data = data;
        timeData = new TimeData(DataArrayType.FIELD_DATA_SHORT);
        timeData.addValue(data);
        recomputeStatistics();
    }

    /**
     * Creates a new instance of ShortDataArray.
     *
     * @param tData  short array to be included in the generated ShortDataArray
     * @param schema DataArray schema.
     */
    public ShortDataArray(TimeData tData, DataArraySchema schema)
    {
        super(schema);
        if (schema.getType() != DataArrayType.FIELD_DATA_SHORT) {
            throw new IllegalArgumentException("Schema type does not match array type.");
        }

        if (schema.getNElements() != tData.length() / schema.getVectorLength()) {
            throw new IllegalArgumentException("Schema does not match array length.");
        }
        timeData = tData;
        setCurrentTime(currentTime);
        recomputeStatistics();
    }

    @Override
    public ShortDataArray cloneShallow()
    {
        ShortDataArray clone;
        if (timeData.isEmpty()) {
            clone = new ShortDataArray(schema.cloneDeep());
            clone.currentTime = currentTime;
        } else {
            clone = new ShortDataArray(timeData.cloneShallow(), schema.cloneDeep());
            clone.setCurrentTime(currentTime);
        }
        clone.timestamp = timestamp;
        return clone;
    }

    @Override
    public ShortDataArray cloneDeep()
    {
        ShortDataArray clone;
        if (timeData.isEmpty()) {
            clone = new ShortDataArray(schema.cloneDeep());
            clone.currentTime = currentTime;
        } else {
            clone = new ShortDataArray(timeData.cloneDeep(), schema.cloneDeep());
            clone.setCurrentTime(currentTime);
        }
        clone.timestamp = timestamp;
        return clone;
    }

    @Override
    public ShortLargeArray getRawArray()
    {
        if (data == null) {
            setCurrentTime(currentTime);
        }
        return (ShortLargeArray) data;
    }

    @Override
    public ShortLargeArray getRawArray(float time)
    {
        return (ShortLargeArray) timeData.getValue(time);
    }

    @Override
    public ShortLargeArray produceData(float time)
    {
        return (ShortLargeArray) timeData.produceValue(time, getVectorLength() * getNElements());
    }

}
