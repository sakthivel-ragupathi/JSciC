/* ***** BEGIN LICENSE BLOCK *****
 * Java Scientific Containers (JSciC)
 * Copyright (C) 2015 onward University of Warsaw, ICM
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * ***** END LICENSE BLOCK ***** */

package pl.edu.icm.jscic.dataarrays;

import java.util.ArrayList;
import pl.edu.icm.jscic.TimeData;
import pl.edu.icm.jscic.utils.FloatingPointUtils;
import pl.edu.icm.jscic.utils.InfinityAction;
import pl.edu.icm.jscic.utils.NaNAction;
import pl.edu.icm.jlargearrays.DoubleLargeArray;
import pl.edu.icm.jlargearrays.LargeArray;

/**
 *
 * DataArray that stores double precision floating point elements.
 * 
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 * @author Bartosz Borucki, University of Warsaw, ICM
 */
public class DoubleDataArray extends DataArray
{

    private static final long serialVersionUID = 3672664118918770273L;

    private NaNAction nanAction = FloatingPointUtils.defaultNanAction;
    private InfinityAction infinityAction = FloatingPointUtils.defaultInfinityAction;

    /**
     * Creates a new instance of DoubleDataArray.
     *
     * @param	schema	DataArray schema.
     */
    public DoubleDataArray(DataArraySchema schema)
    {
        super(schema);
        if (schema.getType() != DataArrayType.FIELD_DATA_DOUBLE) {
            throw new IllegalArgumentException("Schema type does not match array type.");
        }
        timeData = new TimeData(DataArrayType.FIELD_DATA_DOUBLE);
    }

    /**
     * Creates a new instance of constant DoubleDataArray.
     *
     * @param ndata number of data elements in the DoubleDataArray
     * @param value constant value
     */
    public DoubleDataArray(long ndata, Double value)
    {
        super(DataArrayType.FIELD_DATA_DOUBLE, ndata, true);
        this.data = new DoubleLargeArray(ndata, value);
        timeData = new TimeData(DataArrayType.FIELD_DATA_DOUBLE);
        timeData.addValue(data);
        recomputeStatistics();
    }

    /**
     * Creates a new instance of DoubleDataArray.
     *
     * @param ndata  number of data elements in the DoubleDataArray
     * @param veclen vector length (1 for scalar data)
     */
    public DoubleDataArray(long ndata, int veclen)
    {
        super(DataArrayType.FIELD_DATA_DOUBLE, ndata, veclen);
        timeData = new TimeData(DataArrayType.FIELD_DATA_DOUBLE);
    }

    /**
     * Creates a new instance of DoubleDataArray.
     *
     * @param data   double array to be included in the generated DoubleDataArray
     * @param schema DataArray schema.
     */
    public DoubleDataArray(DoubleLargeArray data, DataArraySchema schema)
    {
        this(data, schema, true, FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction);
    }

    /**
     * Creates a new instance of DoubleDataArray.
     *
     * @param data           double array to be included in the generated DoubleDataArray
     * @param schema         DataArray schema.
     * @param testNanInf     if true, then the DataArray is tested for NaNs and infinities
     * @param nanAction      not a number action
     * @param infinityAction infinity action
     */
    public DoubleDataArray(DoubleLargeArray data, DataArraySchema schema, boolean testNanInf, NaNAction nanAction, InfinityAction infinityAction)
    {
        super(schema);
        if (schema.getType() != DataArrayType.FIELD_DATA_DOUBLE) {
            throw new IllegalArgumentException("Schema type does not match array type.");
        }

        if (schema.getNElements() != data.length() / schema.getVectorLength()) {
            throw new IllegalArgumentException("Schema does not match array length.");
        }
        if(schema.isConstant() != data.isConstant()) {
            throw new IllegalArgumentException("schema.isConstant() != data.isConstant()");
        }
        if (testNanInf) {
            FloatingPointUtils.processNaNs(data, nanAction, infinityAction);
        }
        this.data = data;
        this.nanAction = nanAction;
        this.infinityAction = infinityAction;
        ArrayList<Float> timeSeries = new ArrayList<>(1);
        ArrayList<LargeArray> dataSeries = new ArrayList<>(1);
        timeSeries.add(0f);
        dataSeries.add(data);
        timeData = new TimeData(timeSeries, dataSeries, 0f, false, nanAction, infinityAction);
        recomputeStatistics();
    }

    /**
     * Creates a new instance of DoubleDataArray.
     *
     * @param tData  double array to be included in the generated DoubleDataArray
     * @param schema DataArray schema.
     */
    public DoubleDataArray(TimeData tData, DataArraySchema schema)
    {
        this(tData, schema, true, FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction);
    }

    /**
     * Creates a new instance of DoubleDataArray.
     *
     * @param tData          double array to be included in the generated DoubleDataArray
     * @param schema         DataArray schema.
     * @param testNanInf     if true, then the DataArray is tested for NaNs and infinities
     * @param nanAction      not a number action
     * @param infinityAction infinity action
     */
    public DoubleDataArray(TimeData tData, DataArraySchema schema, boolean testNanInf, NaNAction nanAction, InfinityAction infinityAction)
    {
        super(schema);
        if (schema.getType() != DataArrayType.FIELD_DATA_DOUBLE) {
            throw new IllegalArgumentException("Schema type does not match array type.");
        }

        if (tData.getType() != DataArrayType.FIELD_DATA_DOUBLE) {
            throw new IllegalArgumentException("Data type does not match array type.");
        }

        if (schema.getNElements() != tData.length() / schema.getVectorLength()) {
            throw new IllegalArgumentException("Schema does not match array length.");
        }
        if (testNanInf) {
            for (int i = 0; i < tData.getNSteps(); i++) {
                FloatingPointUtils.processNaNs((DoubleLargeArray) tData.getValues().get(i), nanAction, infinityAction);
            }
        }
        this.nanAction = nanAction;
        this.infinityAction = infinityAction;
        timeData = tData;
        setCurrentTime(currentTime);
        recomputeStatistics();
    }

    @Override
    public DoubleDataArray cloneShallow()
    {
        DoubleDataArray clone;
        if (timeData.isEmpty()) {
            clone = new DoubleDataArray(schema.cloneDeep());
            clone.currentTime = currentTime;

        } else {
            clone = new DoubleDataArray(timeData.cloneShallow(), schema.cloneDeep());
            clone.setCurrentTime(currentTime);
        }
        clone.timestamp = timestamp;
        return clone;
    }

    @Override
    public DoubleDataArray cloneDeep()
    {
        DoubleDataArray clone;
        if (timeData.isEmpty()) {
            clone = new DoubleDataArray(schema.cloneDeep());
            clone.currentTime = currentTime;
        } else {
            clone = new DoubleDataArray(timeData.cloneDeep(), schema.cloneDeep());
            clone.setCurrentTime(currentTime);
        }
        clone.timestamp = timestamp;
        return clone;
    }

    @Override
    public void setTimeData(TimeData tData)
    {
        if (tData == null || tData.isEmpty() || tData.getType() != timeData.getType() || tData.length() != getNElements() * getVectorLength()) {
            throw new IllegalArgumentException("tData == null || tData.isEmpty() || tData.getType() != timeData.getType() || tData.length() != getNElements() * getVectorLength()");
        }

        for (int i = 0; i < tData.getNSteps(); i++) {
            FloatingPointUtils.processNaNs((DoubleLargeArray) tData.getValues().get(i), nanAction, infinityAction);
        }

        timeData = tData;
        setCurrentTime(currentTime);
        recomputeStatistics();
    }

    @Override
    public DoubleLargeArray getRawArray(float time)
    {
        return (DoubleLargeArray) timeData.getValue(time);
    }

    @Override
    public DoubleLargeArray produceData(float time)
    {
        return (DoubleLargeArray) timeData.produceValue(time, getVectorLength() * getNElements());
    }

    @Override
    public DoubleLargeArray getRawArray()
    {
        if (data == null) {
            setCurrentTime(currentTime);
        }
        return (DoubleLargeArray) data;
    }

    /**
     * Returns not a number action
     * 
     * @return not a number action
     */
    public NaNAction getNanAction()
    {
        return nanAction;
    }

    /**
     * Sets not a number action
     * 
     * @param nanAction a new not a number action
     */
    public void setNanAction(NaNAction nanAction)
    {
        this.nanAction = nanAction;
    }

    /**
     * Returns infinity action.
     * 
     * @return infinity action
     */
    public InfinityAction getInfinityAction()
    {
        return infinityAction;
    }

    /**
     * Sets infinity action.
     * 
     * @param infinityAction infinity action
     */
    public void setInfinityAction(InfinityAction infinityAction)
    {
        this.infinityAction = infinityAction;
    }
}
