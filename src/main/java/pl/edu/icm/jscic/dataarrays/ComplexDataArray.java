/* ***** BEGIN LICENSE BLOCK *****
 * Java Scientific Containers (JSciC)
 * Copyright (C) 2015 onward University of Warsaw, ICM
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * ***** END LICENSE BLOCK ***** */

package pl.edu.icm.jscic.dataarrays;

import java.util.ArrayList;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import pl.edu.icm.jscic.TimeData;
import pl.edu.icm.jscic.utils.FloatingPointUtils;
import pl.edu.icm.jscic.utils.InfinityAction;
import pl.edu.icm.jscic.utils.NaNAction;
import pl.edu.icm.jlargearrays.FloatLargeArray;
import static org.apache.commons.math3.util.FastMath.*;
import pl.edu.icm.jlargearrays.ConcurrencyUtils;
import pl.edu.icm.jlargearrays.ComplexFloatLargeArray;
import pl.edu.icm.jlargearrays.LargeArray;

/**
 *
 * DataArray that stores single precision complex elements.
 * 
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 * @author Bartosz Borucki, University of Warsaw, ICM
 */
public class ComplexDataArray extends DataArray
{

    private static final long serialVersionUID = -145015433254665644L;

    private NaNAction nanAction = FloatingPointUtils.defaultNanAction;
    private InfinityAction infinityAction = FloatingPointUtils.defaultInfinityAction;

    /**
     * Creates a new instance of ComplexDataArray.
     *
     * @param	schema	DataArray schema
     */
    public ComplexDataArray(DataArraySchema schema)
    {
        super(schema);
        if (schema.getType() != DataArrayType.FIELD_DATA_COMPLEX) {
            throw new IllegalArgumentException("Schema type does not match array type.");
        }
        timeData = new TimeData(DataArrayType.FIELD_DATA_COMPLEX);
    }

    /**
     * Creates a new instance of constant ComplexDataArray.
     *
     * @param ndata number of data elements in the ComplexDataArray
     * @param value constant value
     */
    public ComplexDataArray(long ndata, float[] value)
    {
        super(DataArrayType.FIELD_DATA_COMPLEX, ndata, true);
        this.data = new ComplexFloatLargeArray(ndata, value);
        timeData = new TimeData(DataArrayType.FIELD_DATA_COMPLEX);
        timeData.addValue(data);
        recomputeStatistics();
    }

    /**
     * Creates a new instance of ComplexDataArray.
     *
     * @param ndata  number of data elements in the ComplexDataArray
     * @param veclen vector length (1 for scalar data)
     */
    public ComplexDataArray(long ndata, int veclen)
    {
        super(DataArrayType.FIELD_DATA_COMPLEX, ndata, veclen);
        timeData = new TimeData(DataArrayType.FIELD_DATA_COMPLEX);
    }

    /**
     * Creates a new instance of ComplexDataArray.
     *
     * @param data   complex array, this reference is used internally (the array is not cloned)
     * @param schema This DataArray schema
     */
    public ComplexDataArray(ComplexFloatLargeArray data, DataArraySchema schema)
    {
        this(data, schema, true, FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction);
    }

    /**
     * Creates a new instance of ComplexDataArray.
     *
     * @param data           complex array, this reference is used internally (the array is not cloned)
     * @param schema         DataArray schema
     * @param testNanInf     if true, then the DataArray is tested for NaNs and infinities
     * @param nanAction      not a number action
     * @param infinityAction infinity action
     */
    public ComplexDataArray(ComplexFloatLargeArray data, DataArraySchema schema, boolean testNanInf, NaNAction nanAction, InfinityAction infinityAction)
    {
        super(schema);
        if (schema.getType() != DataArrayType.FIELD_DATA_COMPLEX) {
            throw new IllegalArgumentException("Schema type does not match array type.");
        }

        if (schema.getNElements() != data.length() / schema.getVectorLength()) {
            throw new IllegalArgumentException("Schema does not match array length.");
        }
        if (schema.isConstant() != data.isConstant()) {
            throw new IllegalArgumentException("schema.isConstant() != data.isConstant()");
        }
        if (testNanInf) {
            FloatingPointUtils.processNaNs(data, nanAction, infinityAction);
        }
        this.data = data;
        ArrayList<Float> timeSeries = new ArrayList<>(1);
        ArrayList<LargeArray> dataSeries = new ArrayList<>(1);
        timeSeries.add(0f);
        dataSeries.add(data);
        timeData = new TimeData(timeSeries, dataSeries, 0f, false, nanAction, infinityAction);
        recomputeStatistics();
    }

    /**
     * Creates a new instance of ComplexDataArray.
     *
     * @param tData  complex array, this reference is used internally (the array is not cloned)
     * @param schema DataArray schema
     */
    public ComplexDataArray(TimeData tData, DataArraySchema schema)
    {
        this(tData, schema, true, FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction);
    }

    /**
     * Creates a new instance of ComplexDataArray.
     *
     * @param tData          complex array, this reference is used internally (the array is not cloned)
     * @param schema         DataArray schema
     * @param testNanInf     if true, then the DataArray is tested for NaNs and infinities
     * @param nanAction      not a number action
     * @param infinityAction infinity action
     */
    public ComplexDataArray(TimeData tData, DataArraySchema schema, boolean testNanInf, NaNAction nanAction, InfinityAction infinityAction)
    {
        super(schema);
        if (schema.getType() != DataArrayType.FIELD_DATA_COMPLEX) {
            throw new IllegalArgumentException("Schema type does not match array type.");
        }

        if (tData.getType() != DataArrayType.FIELD_DATA_COMPLEX) {
            throw new IllegalArgumentException("Data type does not match array type.");
        }

        if (schema.getNElements() != tData.length() / schema.getVectorLength()) {
            throw new IllegalArgumentException("Schema does not match array length.");
        }
        if (testNanInf) {
            for (int i = 0; i < tData.getNSteps(); i++) {
                FloatingPointUtils.processNaNs(((ComplexFloatLargeArray) tData.getValues().get(i)), nanAction, infinityAction);
            }
        }
        this.nanAction = nanAction;
        this.infinityAction = infinityAction;
        timeData = tData;
        setCurrentTime(currentTime);
        recomputeStatistics();
    }

    @Override
    public final void recomputeStatistics(TimeData timeMask, boolean recomputePreferredMinMax)
    {
        final int vlen = getVectorLength();
        double minv = Double.MAX_VALUE;
        double maxv = -Double.MAX_VALUE;
        double meanv = 0;
        double mean2v = 0;
        long mlength = 0;
        Future<?>[] futures;
        int nthreads = ConcurrencyUtils.getNumberOfThreads();
        if (timeMask == null || timeMask.getNSteps() <= 0) {
            for (int step = 0; step < timeData.getNSteps(); step++) {
                final FloatLargeArray dtaRe = ((ComplexFloatLargeArray) timeData.getValues().get(step)).getRealArray();
                final FloatLargeArray dtaIm = ((ComplexFloatLargeArray) timeData.getValues().get(step)).getImaginaryArray();
                if (vlen == 1) {
                    final long length = dtaRe.length();
                    nthreads = (int) min(nthreads, length);
                    long k = length / nthreads;
                    futures = new Future[nthreads];
                    for (int j = 0; j < nthreads; j++) {
                        final long firstIdx = j * k;
                        final long lastIdx = (j == nthreads - 1) ? length : firstIdx + k;
                        futures[j] = ConcurrencyUtils.submit(new Callable<double[]>()
                        {
                            @Override
                            public double[] call() throws Exception
                            {
                                double min = Double.MAX_VALUE;
                                double max = -Double.MAX_VALUE;
                                double mean = 0;
                                double mean2 = 0;
                                long mlength = 0;
                                for (long i = firstIdx; i < lastIdx; i++) {
                                    double d = sqrt(dtaRe.getFloat(i) * dtaRe.getFloat(i) + dtaIm.getFloat(i) * dtaIm.getFloat(i));
                                    mean += d;
                                    mean2 += d * d;
                                    mlength++;
                                    if (d < min) {
                                        min = d;
                                    }
                                    if (d > max) {
                                        max = d;
                                    }
                                }
                                return new double[]{min, max, mean, mean2, (double) mlength};
                            }
                        });
                    }
                } else {
                    final long length = dtaRe.length() / vlen;
                    nthreads = (int) min(nthreads, length);
                    long k = length / nthreads;
                    futures = new Future[nthreads];
                    for (int j = 0; j < nthreads; j++) {
                        final long firstIdx = j * k;
                        final long lastIdx = (j == nthreads - 1) ? length : firstIdx + k;
                        futures[j] = ConcurrencyUtils.submit(new Callable<double[]>()
                        {
                            @Override
                            public double[] call() throws Exception
                            {
                                double min = Double.MAX_VALUE;
                                double max = -Double.MAX_VALUE;
                                double mean = 0;
                                double mean2 = 0;
                                long mlength = 0;
                                for (long i = firstIdx; i < lastIdx; i += vlen) {
                                    double v = 0;
                                    for (long j = 0; j < vlen; j++) {
                                        v += dtaRe.getFloat(vlen * i + j) * dtaRe.getFloat(vlen * i + j) + dtaIm.getFloat(vlen * i + j) * dtaIm.getFloat(vlen * i + j);
                                    }
                                    mean2 += v;
                                    v = sqrt(v);
                                    mean += v;
                                    mlength++;
                                    if (v < min) {
                                        min = v;
                                    }
                                    if (v > max) {
                                        max = v;
                                    }
                                }
                                return new double[]{min, max, mean, mean2, (double) mlength};
                            }
                        });
                    }
                }
                try {
                    for (int j = 0; j < nthreads; j++) {
                        double[] res = (double[]) futures[j].get();
                        if (res[0] < minv) {
                            minv = res[0];
                        }
                        if (res[1] > maxv) {
                            maxv = res[1];
                        }
                        meanv += res[2];
                        mean2v += res[3];
                        mlength += res[4];
                    }
                } catch (InterruptedException | ExecutionException ex) {
                    throw new IllegalStateException(ex);
                }
            }
        } else {
            for (int step = 0; step < timeData.getNSteps(); step++) {
                final FloatLargeArray dtaRe = ((ComplexFloatLargeArray) timeData.getValues().get(step)).getRealArray();
                final FloatLargeArray dtaIm = ((ComplexFloatLargeArray) timeData.getValues().get(step)).getImaginaryArray();
                final LargeArray mask;
                if (timeMask.getNSteps() != timeData.getNSteps()) {
                    mask = timeMask.getValues().get(0);
                } else {
                    mask = timeMask.getValues().get(step);
                }
                if (vlen == 1) {
                    final long length = dtaRe.length();
                    nthreads = (int) min(nthreads, length);
                    long k = length / nthreads;
                    futures = new Future[nthreads];
                    for (int j = 0; j < nthreads; j++) {
                        final long firstIdx = j * k;
                        final long lastIdx = (j == nthreads - 1) ? length : firstIdx + k;
                        futures[j] = ConcurrencyUtils.submit(new Callable<double[]>()
                        {
                            @Override
                            public double[] call() throws Exception
                            {
                                double min = Double.MAX_VALUE;
                                double max = -Double.MAX_VALUE;
                                double mean = 0;
                                double mean2 = 0;
                                long mlength = 0;
                                for (long i = firstIdx; i < lastIdx; i++) {
                                    if (mask.getByte(i) == 0) {
                                        continue;
                                    }
                                    double d = sqrt(dtaRe.getFloat(i) * dtaRe.getFloat(i) + dtaIm.getFloat(i) * dtaIm.getFloat(i));
                                    mean += d;
                                    mean2 += d * d;
                                    mlength++;
                                    if (d < min) {
                                        min = d;
                                    }
                                    if (d > max) {
                                        max = d;
                                    }
                                }
                                return new double[]{min, max, mean, mean2, (double) mlength};
                            }
                        });
                    }
                } else {
                    final long length = dtaRe.length() / vlen;
                    nthreads = (int) min(nthreads, length);
                    long k = length / nthreads;
                    futures = new Future[nthreads];
                    for (int j = 0; j < nthreads; j++) {
                        final long firstIdx = j * k;
                        final long lastIdx = (j == nthreads - 1) ? length : firstIdx + k;
                        futures[j] = ConcurrencyUtils.submit(new Callable<double[]>()
                        {
                            @Override
                            public double[] call() throws Exception
                            {
                                double min = Double.MAX_VALUE;
                                double max = -Double.MAX_VALUE;
                                double mean = 0;
                                double mean2 = 0;
                                long mlength = 0;
                                for (long i = firstIdx; i < lastIdx; i += vlen) {
                                    if (mask.getByte(i) == 0) {
                                        continue;
                                    }
                                    double v = 0;
                                    for (long j = 0; j < vlen; j++) {
                                        v += dtaRe.getFloat(vlen * i + j) * dtaRe.getFloat(vlen * i + j) + dtaIm.getFloat(vlen * i + j) * dtaIm.getFloat(vlen * i + j);
                                    }
                                    mean2 += v;
                                    v = sqrt(v);
                                    mean += v;
                                    mlength++;
                                    if (v < min) {
                                        min = v;
                                    }
                                    if (v > max) {
                                        max = v;
                                    }
                                }
                                return new double[]{min, max, mean, mean2, (double) mlength};
                            }
                        });
                    }
                }
                try {
                    for (int j = 0; j < nthreads; j++) {
                        double[] res = (double[]) futures[j].get();
                        if (res[0] < minv) {
                            minv = res[0];
                        }
                        if (res[1] > maxv) {
                            maxv = res[1];
                        }
                        meanv += res[2];
                        mean2v += res[3];
                        mlength += res[4];
                    }
                } catch (InterruptedException | ExecutionException ex) {
                    throw new IllegalStateException(ex);
                }
            }
        }
        meanv /= (double) mlength;
        mean2v /= (double) mlength;
        setStatistics(minv, maxv, meanv, mean2v, recomputePreferredMinMax);
    }

    @Override
    public ComplexDataArray cloneShallow()
    {
        ComplexDataArray clone;
        if (timeData.isEmpty()) {
            clone = new ComplexDataArray(schema.cloneDeep());
            clone.currentTime = currentTime;
        } else {
            clone = new ComplexDataArray(timeData.cloneShallow(), schema.cloneDeep());
            clone.setCurrentTime(currentTime);
        }
        clone.timestamp = timestamp;
        return clone;
    }

    @Override
    public ComplexDataArray cloneDeep()
    {
        ComplexDataArray clone;
        if (timeData.isEmpty()) {
            clone = new ComplexDataArray(schema.cloneDeep());
            clone.currentTime = currentTime;
        } else {
            clone = new ComplexDataArray(timeData.cloneDeep(), schema.cloneDeep());
            clone.setCurrentTime(currentTime);
        }
        clone.timestamp = timestamp;
        return clone;
    }

    /**
     * Returns the real part of the ComplexDataArray
     * 
     * @return real part of the ComplexDataArray
     */
    public FloatLargeArray getFloatRealArray()
    {
        if (data == null) {
            setCurrentTime(currentTime);
        }
        return ((ComplexFloatLargeArray) data).getRealArray();
    }

    /**
     * Returns the imaginary part of the ComplexDataArray
     * 
     * @return imaginary part of the ComplexDataArray
     */
    public FloatLargeArray getFloatImaginaryArray()
    {
        if (data == null) {
            setCurrentTime(currentTime);
        }
        return ((ComplexFloatLargeArray) data).getImaginaryArray();
    }

    /**
     * Returns the absolute value of the ComplexDataArray
     * 
     * @return absolute value of the ComplexDataArray
     */
    public FloatLargeArray getFloatAbsArray()
    {
        if (data == null) {
            setCurrentTime(currentTime);
        }
        return ((ComplexFloatLargeArray) data).getAbsArray();
    }

    /**
     * Returns the argument of the ComplexDataArray
     * 
     * @return argument of the ComplexDataArray
     */
    public FloatLargeArray getFloatArgArray()
    {
        if (data == null) {
            setCurrentTime(currentTime);
        }
        return ((ComplexFloatLargeArray) data).getArgArray();
    }

    /**
     * Return a float element (abs of a complex number) at specified position.
     * 
     * @param n position
     * 
     * @return a float element (abs of a complex number)
     */
    @Override
    public float[] getFloatElement(long n)
    {
        setCurrentTime(currentTime);
        int veclen = schema.getVectorLength();
        float[] out = new float[veclen];
        long i, j;
        for (i = 0, j = n * veclen; i < veclen; i++, j++) {
            out[(int) i] = (float) sqrt(((ComplexFloatLargeArray) data).getRealArray().getFloat(j) * ((ComplexFloatLargeArray) data).getRealArray().getFloat(j) + ((ComplexFloatLargeArray) data).getImaginaryArray().getFloat(j) * ((ComplexFloatLargeArray) data).getImaginaryArray().getFloat(j));
        }
        return out;
    }

    /**
     * Return a double element (abs of a complex number) at specified position.
     * 
     * @param n position
     * 
     * @return a double element (abs of a complex number)
     */
    @Override
    public double[] getDoubleElement(long n)
    {
        setCurrentTime(currentTime);
        int veclen = schema.getVectorLength();
        double[] out = new double[veclen];
        long i, j;
        for (i = 0, j = n * veclen; i < veclen; i++, j++) {
            out[(int) i] = (double) sqrt(((ComplexFloatLargeArray) data).getRealArray().getDouble(j) * ((ComplexFloatLargeArray) data).getRealArray().getDouble(j) + ((ComplexFloatLargeArray) data).getImaginaryArray().getDouble(j) * ((ComplexFloatLargeArray) data).getImaginaryArray().getDouble(j));
        }
        return out;
    }

    @Override
    public void setTimeData(TimeData tData)
    {
        if (tData == null || tData.isEmpty() || tData.getType() != timeData.getType() || tData.length() != getNElements() * getVectorLength()) {
            throw new IllegalArgumentException("tData == null || tData.isEmpty() || tData.getType() != timeData.getType() || tData.length() != getNElements() * getVectorLength()");
        }

        for (int i = 0; i < tData.getNSteps(); i++) {
            FloatingPointUtils.processNaNs(((ComplexFloatLargeArray) tData.getValues().get(i)), nanAction, infinityAction);
        }

        timeData = tData;
        setCurrentTime(currentTime);
        recomputeStatistics();
    }

    @Override
    public ComplexFloatLargeArray getRawArray(float time)
    {
        return (ComplexFloatLargeArray) timeData.getValue(time);
    }

    @Override
    public ComplexFloatLargeArray produceData(float time)
    {
        return (ComplexFloatLargeArray) timeData.produceValue(time, getVectorLength() * getNElements());
    }

    @Override
    public ComplexFloatLargeArray getRawArray()
    {
        if (data == null) {
            setCurrentTime(currentTime);
        }
        return (ComplexFloatLargeArray) data;
    }

    /**
     * Returns not a number action
     * 
     * @return not a number action
     */
    public NaNAction getNanAction()
    {
        return nanAction;
    }

    /**
     * Sets not a number action
     * 
     * @param nanAction a new not a number action
     */
    public void setNanAction(NaNAction nanAction)
    {
        this.nanAction = nanAction;
    }

    /**
     * Returns infinity action.
     * 
     * @return infinity action
     */
    public InfinityAction getInfinityAction()
    {
        return infinityAction;
    }

    /**
     * Sets infinity action.
     * 
     * @param infinityAction infinity action
     */
    public void setInfinityAction(InfinityAction infinityAction)
    {
        this.infinityAction = infinityAction;
    }
}
