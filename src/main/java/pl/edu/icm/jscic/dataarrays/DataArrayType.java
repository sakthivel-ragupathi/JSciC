/* ***** BEGIN LICENSE BLOCK *****
 * Java Scientific Containers (JSciC)
 * Copyright (C) 2015 onward University of Warsaw, ICM
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * ***** END LICENSE BLOCK ***** */

package pl.edu.icm.jscic.dataarrays;

import pl.edu.icm.jlargearrays.LargeArrayType;

/**
 *
 * Supported types of a DataArray.
 * 
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 * @author Bartosz Borucki, University of Warsaw, ICM
 */
public enum DataArrayType
{

    FIELD_DATA_UNKNOWN,
    FIELD_DATA_LOGIC
        {

            @Override
            public int getValue()
            {
                return 0;
            }

            @Override
            public String toString()
            {
                return "logic";
            }

            @Override
            public boolean isNumericType()
            {
                return true;
            }

            @Override
            public LargeArrayType toLargeArrayType()
            {
                return LargeArrayType.LOGIC;
            }

        },
    FIELD_DATA_BYTE
        {
            @Override
            public int getValue()
            {
                return 1;
            }

            @Override
            public String toString()
            {
                return "byte";
            }

            @Override
            public boolean isNumericType()
            {
                return true;
            }

            @Override
            public LargeArrayType toLargeArrayType()
            {
                return LargeArrayType.UNSIGNED_BYTE;
            }

        },
    FIELD_DATA_SHORT
        {
            @Override
            public int getValue()
            {
                return 2;
            }

            @Override
            public String toString()
            {
                return "short";
            }

            @Override
            public boolean isNumericType()
            {
                return true;
            }

            @Override
            public LargeArrayType toLargeArrayType()
            {
                return LargeArrayType.SHORT;
            }

        },
    FIELD_DATA_INT
        {
            @Override
            public int getValue()
            {
                return 3;
            }

            @Override
            public String toString()
            {
                return "int";
            }

            @Override
            public boolean isNumericType()
            {
                return true;
            }

            @Override
            public LargeArrayType toLargeArrayType()
            {
                return LargeArrayType.INT;
            }

        },
    FIELD_DATA_LONG
        {
            @Override
            public int getValue()
            {
                return 9;
            }

            @Override
            public String toString()
            {
                return "long";
            }

            @Override
            public boolean isNumericType()
            {
                return true;
            }

            @Override
            public LargeArrayType toLargeArrayType()
            {
                return LargeArrayType.LONG;
            }

        },
    FIELD_DATA_FLOAT
        {

            @Override
            public int getValue()
            {
                return 4;
            }

            @Override
            public String toString()
            {
                return "float";
            }

            @Override
            public boolean isNumericType()
            {
                return true;
            }

            @Override
            public LargeArrayType toLargeArrayType()
            {
                return LargeArrayType.FLOAT;
            }

        },
    FIELD_DATA_DOUBLE
        {

            @Override
            public int getValue()
            {
                return 5;
            }

            @Override
            public String toString()
            {
                return "double";
            }

            @Override
            public boolean isNumericType()
            {
                return true;
            }

            @Override
            public LargeArrayType toLargeArrayType()
            {
                return LargeArrayType.DOUBLE;
            }

        },
    FIELD_DATA_COMPLEX
        {

            @Override
            public int getValue()
            {
                return 6;
            }

            @Override
            public String toString()
            {
                return "complex";
            }

            @Override
            public boolean isNumericType()
            {
                return true;
            }

            @Override
            public LargeArrayType toLargeArrayType()
            {
                return LargeArrayType.COMPLEX_FLOAT;
            }

        },
    FIELD_DATA_STRING
        {
            @Override
            public int getValue()
            {
                return 7;
            }

            @Override
            public String toString()
            {
                return "string";
            }

            @Override
            public boolean isNumericType()
            {
                return false;
            }

            @Override
            public LargeArrayType toLargeArrayType()
            {
                return LargeArrayType.STRING;
            }

        },
    FIELD_DATA_OBJECT
        {
            @Override
            public int getValue()
            {
                return 8;
            }

            @Override
            public String toString()
            {
                return "object";
            }

            @Override
            public boolean isNumericType()
            {
                return false;
            }

            @Override
            public LargeArrayType toLargeArrayType()
            {
                return LargeArrayType.OBJECT;
            }

        };

    @Override
    public String toString()
    {
        return "unknown";
    }

    /**
     * Returns true if a given type is numeric, false otherwise.
     * 
     * @return true if a given type is numeric, false otherwise
     */
    public boolean isNumericType()
    {
        return false;

    }

    /**
     * Returns a LargeArray type corresponding to a given DataArray type. Returns null for FIELD_DATA_UNKNOWN
     * 
     * @return a LargeArray type corresponding to a given DataArray type.
     */
    public LargeArrayType toLargeArrayType()
    {
        return null;
    }

    /**
     * Returns the value of a given DataArray type.
     * 
     * @return value of a given DataArray type
     */
    public int getValue()
    {
        return -1;
    }

    /**
     * Returns the array type corresponding to a given value.
     * 
     * @param value value of an array type.
     * 
     * @return array type corresponding to a given value
     */
    public static DataArrayType getType(int value)
    {
        switch (value) {
            case -1:
                return FIELD_DATA_UNKNOWN;
            case 0:
                return FIELD_DATA_LOGIC;
            case 1:
                return FIELD_DATA_BYTE;
            case 2:
                return FIELD_DATA_SHORT;
            case 3:
                return FIELD_DATA_INT;
            case 9:
                return FIELD_DATA_LONG;
            case 4:
                return FIELD_DATA_FLOAT;
            case 5:
                return FIELD_DATA_DOUBLE;
            case 6:
                return FIELD_DATA_COMPLEX;
            case 7:
                return FIELD_DATA_STRING;
            case 8:
                return FIELD_DATA_OBJECT;
            default:
                return FIELD_DATA_UNKNOWN;

        }
    }

    /**
     * Returns the value of the last supported type. Values of the types are sorted in ascending order.
     * 
     * @return value of the last supported type
     */
    public static int getMaxValue()
    {
        //Values need to be sorted in ascending order
        return values()[values().length - 1].getValue();
    }

}
