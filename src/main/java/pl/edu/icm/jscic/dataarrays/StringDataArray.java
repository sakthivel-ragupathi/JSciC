/* ***** BEGIN LICENSE BLOCK *****
 * Java Scientific Containers (JSciC)
 * Copyright (C) 2015 onward University of Warsaw, ICM
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * ***** END LICENSE BLOCK ***** */

package pl.edu.icm.jscic.dataarrays;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import static org.apache.commons.math3.util.FastMath.*;
import pl.edu.icm.jscic.TimeData;
import pl.edu.icm.jlargearrays.ConcurrencyUtils;
import pl.edu.icm.jlargearrays.LargeArray;
import pl.edu.icm.jlargearrays.StringLargeArray;

/**
 *
 * DataArray that stores String elements.
 * 
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 * @author Bartosz Borucki, University of Warsaw, ICM
 */
public class StringDataArray extends DataArray
{

    private static final long serialVersionUID = -802954703351295369L;

    /**
     * Creates a new instance of StringDataArray.
     *
     * @param	schema	DataArray schema.
     */
    public StringDataArray(DataArraySchema schema)
    {
        super(schema);
        if (schema.getType() != DataArrayType.FIELD_DATA_STRING) {
            throw new IllegalArgumentException("Schema type does not match array type.");
        }
        timeData = new TimeData(DataArrayType.FIELD_DATA_STRING);
    }

    /**
     * Creates a new instance of constant StringDataArray.
     *
     * @param ndata number of data elements in the StringDataArray
     * @param value constant value
     */
    public StringDataArray(long ndata, String value)
    {
        super(DataArrayType.FIELD_DATA_STRING, ndata, true);
        this.data = new StringLargeArray(ndata, value);
        timeData = new TimeData(DataArrayType.FIELD_DATA_STRING);
        timeData.addValue(data);
        recomputeStatistics();
    }

    /**
     * Creates a new instance of StringDataArray.
     *
     * @param ndata  number of data elements in the StringDataArray
     * @param veclen vector length (1 for scalar data)
     */
    public StringDataArray(long ndata, int veclen)
    {
        super(DataArrayType.FIELD_DATA_STRING, ndata, veclen);
        timeData = new TimeData(DataArrayType.FIELD_DATA_STRING);
    }

    /**
     * Creates a new instance of StringDataArray.
     *
     * @param data   string array to be included in the generated StringDataArray
     * @param schema DataArray schema
     */
    public StringDataArray(StringLargeArray data, DataArraySchema schema)
    {
        super(schema);
        if (schema.getType() != DataArrayType.FIELD_DATA_STRING) {
            throw new IllegalArgumentException("Schema type does not match array type.");
        }
        if (schema.getNElements() != data.length() / schema.getVectorLength()) {
            throw new IllegalArgumentException("Schema does not match array length.");
        }
        if (schema.isConstant() != data.isConstant()) {
            throw new IllegalArgumentException("schema.isConstant() != data.isConstant()");
        }

        this.data = data;
        timeData = new TimeData(DataArrayType.FIELD_DATA_STRING);
        timeData.addValue(data);
        recomputeStatistics();
    }

    /**
     * Creates a new instance of StringDataArray.
     *
     * @param tData  string array to be included in the generated StringDataArray
     * @param schema DataArray schema
     */
    public StringDataArray(TimeData tData, DataArraySchema schema)
    {
        super(schema);
        if (schema.getType() != DataArrayType.FIELD_DATA_STRING) {
            throw new IllegalArgumentException("Schema type does not match array type.");
        }

        if (tData.getType() != DataArrayType.FIELD_DATA_STRING) {
            throw new IllegalArgumentException("Data type does not match array type.");
        }

        if (schema.getNElements() != tData.length() / schema.getVectorLength()) {
            throw new IllegalArgumentException("Schema does not match array length.");
        }
        timeData = tData;
        setCurrentTime(currentTime);
        recomputeStatistics();
    }

    @Override
    public final void recomputeStatistics(TimeData timeMask, boolean recomputePreferredMinMax)
    {
        final int vlen = getVectorLength();
        double minv = Double.MAX_VALUE;
        double maxv = -Double.MAX_VALUE;
        double meanv = 0;
        double mean2v = 0;
        long mlength = 0;
        Future<?>[] futures;
        int nthreads = ConcurrencyUtils.getNumberOfThreads();
        if (timeMask == null || timeMask.getNSteps() <= 0) {
            for (int step = 0; step < timeData.getNSteps(); step++) {
                final StringLargeArray dta = (StringLargeArray) timeData.getValues().get(step);
                if (vlen == 1) {
                    final long length = dta.length();
                    nthreads = (int) min(nthreads, length);
                    long k = length / nthreads;
                    futures = new Future[nthreads];
                    for (int j = 0; j < nthreads; j++) {
                        final long firstIdx = j * k;
                        final long lastIdx = (j == nthreads - 1) ? length : firstIdx + k;
                        futures[j] = ConcurrencyUtils.submit(new Callable<double[]>()
                        {
                            @Override
                            public double[] call() throws Exception
                            {
                                double min = Double.MAX_VALUE;
                                double max = -Double.MAX_VALUE;
                                double mean = 0;
                                double mean2 = 0;
                                long mlength = 0;
                                for (long i = firstIdx; i < lastIdx; i++) {
                                    String tmp = dta.get(i);
                                    double len = tmp != null ? tmp.length() : 0;
                                    mean += len;
                                    mean2 += len * len;
                                    mlength++;
                                    if (len < min) {
                                        min = len;
                                    }
                                    if (len > max) {
                                        max = len;
                                    }
                                }
                                return new double[]{min, max, mean, mean2, (double) mlength};
                            }
                        });
                    }
                } else {
                    final long length = dta.length() / vlen;
                    nthreads = (int) min(nthreads, length);
                    long k = length / nthreads;
                    futures = new Future[nthreads];
                    for (int j = 0; j < nthreads; j++) {
                        final long firstIdx = j * k;
                        final long lastIdx = (j == nthreads - 1) ? length : firstIdx + k;
                        futures[j] = ConcurrencyUtils.submit(new Callable<double[]>()
                        {
                            @Override
                            public double[] call() throws Exception
                            {
                                double min = Double.MAX_VALUE;
                                double max = -Double.MAX_VALUE;
                                double mean = 0;
                                double mean2 = 0;
                                long mlength = 0;
                                for (long i = firstIdx; i < lastIdx; i += vlen) {
                                    double v = 0;
                                    for (long j = 0; j < vlen; j++) {
                                        String tmp = dta.get(i + j);
                                        double len = tmp != null ? tmp.length() : 0;
                                        v += len * len;
                                    }
                                    mean2 += v;
                                    v = sqrt(v);
                                    mean += v;
                                    mlength++;
                                    if (v < min) {
                                        min = v;
                                    }
                                    if (v > max) {
                                        max = v;
                                    }
                                }
                                return new double[]{0., max, mean, mean2, (double) mlength};
                            }
                        });
                    }
                }
                try {
                    for (int j = 0; j < nthreads; j++) {
                        double[] res = (double[]) futures[j].get();
                        if (res[0] < minv) {
                            minv = res[0];
                        }
                        if (res[1] > maxv) {
                            maxv = res[1];
                        }
                        meanv += res[2];
                        mean2v += res[3];
                        mlength += res[4];
                    }
                } catch (InterruptedException | ExecutionException ex) {
                    throw new IllegalStateException(ex);
                }
            }
        } else {
            for (int step = 0; step < timeData.getNSteps(); step++) {
                final StringLargeArray dta = (StringLargeArray) timeData.getValues().get(step);
                final LargeArray mask;
                if (timeMask.getNSteps() != timeData.getNSteps()) {
                    mask = timeMask.getValues().get(0);
                } else {
                    mask = timeMask.getValues().get(step);
                }
                if (vlen == 1) {
                    final long length = dta.length();
                    nthreads = (int) min(nthreads, length);
                    long k = length / nthreads;
                    futures = new Future[nthreads];
                    for (int j = 0; j < nthreads; j++) {
                        final long firstIdx = j * k;
                        final long lastIdx = (j == nthreads - 1) ? length : firstIdx + k;
                        futures[j] = ConcurrencyUtils.submit(new Callable<double[]>()
                        {
                            @Override
                            public double[] call() throws Exception
                            {
                                double min = Double.MAX_VALUE;
                                double max = -Double.MAX_VALUE;
                                double mean = 0;
                                double mean2 = 0;
                                long mlength = 0;
                                for (long i = firstIdx; i < lastIdx; i++) {
                                    if (mask.getByte(i) == 0) {
                                        continue;
                                    }
                                    String tmp = dta.get(i);
                                    double len = tmp != null ? tmp.length() : 0;
                                    mean += len;
                                    mean2 += len * len;
                                    mlength++;
                                    if (len < min) {
                                        min = len;
                                    }
                                    if (len > max) {
                                        max = len;
                                    }
                                }
                                return new double[]{min, max, mean, mean2, (double) mlength};
                            }
                        });
                    }
                } else {
                    final long length = dta.length() / vlen;
                    nthreads = (int) min(nthreads, length);
                    long k = length / nthreads;
                    futures = new Future[nthreads];
                    for (int j = 0; j < nthreads; j++) {
                        final long firstIdx = j * k;
                        final long lastIdx = (j == nthreads - 1) ? length : firstIdx + k;
                        futures[j] = ConcurrencyUtils.submit(new Callable<double[]>()
                        {
                            @Override
                            public double[] call() throws Exception
                            {
                                double min = Double.MAX_VALUE;
                                double max = -Double.MAX_VALUE;
                                double mean = 0;
                                double mean2 = 0;
                                long mlength = 0;
                                for (long i = firstIdx; i < lastIdx; i += vlen) {
                                    if (mask.getByte(i) == 0) {
                                        continue;
                                    }
                                    double v = 0;
                                    for (long j = 0; j < vlen; j++) {
                                        String tmp = dta.get(i + j);
                                        double len = tmp != null ? tmp.length() : 0;
                                        v += len * len;
                                    }
                                    mean2 += v;
                                    v = sqrt(v);
                                    mean += v;
                                    mlength++;
                                    if (v < min) {
                                        min = v;
                                    }
                                    if (v > max) {
                                        max = v;
                                    }
                                }
                                return new double[]{min, max, mean, mean2, (double) mlength};
                            }
                        });
                    }
                }
                try {
                    for (int j = 0; j < nthreads; j++) {
                        double[] res = (double[]) futures[j].get();
                        if (res[0] < minv) {
                            minv = res[0];
                        }
                        if (res[1] > maxv) {
                            maxv = res[1];
                        }
                        meanv += res[2];
                        mean2v += res[3];
                        mlength += res[4];
                    }
                } catch (InterruptedException | ExecutionException ex) {
                    throw new IllegalStateException(ex);
                }
            }
        }
        meanv /= (double) mlength;
        mean2v /= (double) mlength;
        setStatistics(minv, maxv, meanv, mean2v, recomputePreferredMinMax);
    }

    @Override
    public StringDataArray cloneShallow()
    {
        StringDataArray clone;
        if (timeData.isEmpty()) {
            clone = new StringDataArray(schema.cloneDeep());
            clone.currentTime = currentTime;
        } else {
            clone = new StringDataArray(timeData.cloneShallow(), schema.cloneDeep());
            clone.setCurrentTime(currentTime);
        }
        clone.timestamp = timestamp;
        return clone;
    }

    @Override
    public StringDataArray cloneDeep()
    {
        StringDataArray clone;
        if (timeData.isEmpty()) {
            clone = new StringDataArray(schema.cloneDeep());
            clone.currentTime = currentTime;
        } else {
            clone = new StringDataArray(timeData.cloneDeep(), schema.cloneDeep());
            clone.setCurrentTime(currentTime);
        }
        clone.timestamp = timestamp;
        return clone;
    }

    @Override
    public float[] getFloatElement(long n)
    {
        if (data == null)
            setCurrentTime(currentTime);
        int veclen = schema.getVectorLength();
        float[] out = new float[veclen];
        for (long i = 0, j = n * veclen; i < veclen; i++, j++) {
            out[(int) i] = ((StringLargeArray) data).get(j).length();
        }
        return out;
    }

    @Override
    public double[] getDoubleElement(long n)
    {
        if (data == null)
            setCurrentTime(currentTime);
        int veclen = schema.getVectorLength();
        double[] out = new double[veclen];
        for (long i = 0, j = n * veclen; i < veclen; i++, j++) {
            out[(int) i] = ((StringLargeArray) data).get(j).length();
        }
        return out;
    }

    @Override
    public StringLargeArray getRawArray()
    {
        if (data == null)
            setCurrentTime(currentTime);
        return (StringLargeArray) data;
    }

    @Override
    public StringLargeArray getRawArray(float time)
    {
        return (StringLargeArray) timeData.getValue(time);
    }

    @Override
    public StringLargeArray produceData(float time)
    {
        return (StringLargeArray) timeData.produceValue(time, getVectorLength() * getNElements());
    }

}
