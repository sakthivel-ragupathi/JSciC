/* ***** BEGIN LICENSE BLOCK *****
 * Java Scientific Containers (JSciC)
 * Copyright (C) 2015 onward University of Warsaw, ICM
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * ***** END LICENSE BLOCK ***** */

package pl.edu.icm.jscic;

import java.util.ArrayList;
import static org.apache.commons.math3.util.FastMath.*;
import pl.edu.icm.jscic.dataarrays.DataArraySchema;
import pl.edu.icm.jscic.dataarrays.DataArrayType;

/**
 *
 * Holds general information about Field.
 * 
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 */
public class FieldSchema extends DataContainerSchema
{

    public static final String[] COORD_NAMES = {"__coord x", "__coord y", "__coord z"};

    private float[][] extents = {{Float.MAX_VALUE, Float.MAX_VALUE, Float.MAX_VALUE},
                                   {-Float.MAX_VALUE, -Float.MAX_VALUE, -Float.MAX_VALUE}};
    private float[][] physExtents = {{-.5f, -.5f, -.5f}, {.5f, .5f, .5f}};
    private boolean preservePhysicalExtents = false;
    private String[] coordsUnits = {"", "", ""};
    private int nSpace = 3;

    /**
     * Creates a new instance of FieldSchema.
     */
    public FieldSchema()
    {
        this("Field name");
    }

    /**
     * Creates a new instance of FiledSchema.
     * 
     * @param name field name
     */
    public FieldSchema(String name)
    {
        super(name);
        for (int i = 0; i < COORD_NAMES.length; i++) {
            pseudoComponentSchemas.add(new DataArraySchema(COORD_NAMES[i], "", null, DataArrayType.FIELD_DATA_FLOAT,
                                                           1, 1, false, 0, 1, 0, 1, 0, 1, Float.MAX_VALUE, Float.MAX_VALUE, Float.MAX_VALUE, false));
        }
    }

    @Override
    public FieldSchema cloneDeep()
    {
        FieldSchema clone = new FieldSchema(this.name);

        ArrayList<DataArraySchema> componentSchemasClone = new ArrayList<>();
        if (this.componentSchemas != null && this.componentSchemas.size() > 0) {
            for (DataArraySchema item : this.componentSchemas) componentSchemasClone.add(item.cloneDeep());
        }
        clone.componentSchemas = componentSchemasClone;
        clone.nSpace = nSpace;

        ArrayList<DataArraySchema> pseudoComponentSchemasClone = new ArrayList<>();
        if (this.pseudoComponentSchemas != null && this.pseudoComponentSchemas.size() > 0) {
            for (DataArraySchema item : this.pseudoComponentSchemas) pseudoComponentSchemasClone.add(item.cloneDeep());
        }
        clone.pseudoComponentSchemas = pseudoComponentSchemasClone;

        clone.extents = new float[2][3];
        for (int i = 0; i < extents.length; i++)
            System.arraycopy(this.extents[i], 0, clone.extents[i], 0, 3);
        clone.physExtents = new float[2][3];
        for (int i = 0; i < physExtents.length; i++)
            System.arraycopy(this.physExtents[i], 0, clone.physExtents[i], 0, 3);
        return clone;
    }

    /**
     * Returns field extents. This method always returns a reference to the internal array.
     * 
     * @return field extents
     */
    public float[][] getExtents()
    {
        return extents;
    }

    /**
     * Returns a diameter of this field.
     * 
     * @return diameter of this field
     */
    public float getDiameter()
    {
        double d = 0;
        for (int i = 0; i < extents[0].length; i++)
            d += (extents[1][i] - extents[0][i]) * (extents[1][i] - extents[0][i]);
        return (float) Math.sqrt(d) / 2;
    }

    /**
     * Sets field extents.
     * 
     * @param extents new value of the field extents. This array is not copied.
     */
    public void setExtents(float[][] extents)
    {
        this.extents = extents;
        for (int i = 0; i < getNSpace(); i++)
            for (int j = 0; j < getNPseudoComponents(); j++)
                if (getPseudoComponentSchema(i).getName().equals(FieldSchema.COORD_NAMES[i])) {
                    DataArraySchema sch = getPseudoComponentSchema(i);
                    double min = extents[0][i];
                    double max = extents[1][i];
                    if (min == max) {
                        if (min == 0.0) {
                            min = -0.1;
                            max = 0.1;
                        } else {
                            min -= 0.1 * abs(min);
                            max += 0.1 * abs(max);
                        }
                    }
                    sch.setPreferredRanges(min, max, min, max);
                }
    }

    /**
     * Assigns the value of extents to physical extents.
     */
    protected void physExtentsFromExtents()
    {
        if (preservePhysicalExtents)
            preservePhysicalExtents = false;
        else
            for (int i = 0; i < 2; i++)
                System.arraycopy(extents[i], 0, physExtents[i], 0, nSpace);
    }

    /**
     * Returns physical extents. This method always returns a reference to the internal array.
     * 
     * @return physical extents.
     */
    public float[][] getPhysExtents()
    {
        return physExtents;
    }

    /**
     * Sets physical extents.
     * 
     * @param physExtents     new value of physical extents. This array is not copied.
     * @param lockPhysExtents if true, then the value of physical extents will be locked
     */
    public void setPhysExtents(float[][] physExtents, boolean lockPhysExtents)
    {
        this.physExtents = physExtents;
        preservePhysicalExtents = lockPhysExtents;
    }

    /**
     * Returns units of coordinates. This method always returns a reference to the internal array.
     * 
     * @return units of coordinates
     */
    public String[] getCoordsUnits()
    {
        return coordsUnits;
    }

    /**
     * Sets units of coordinates.
     * 
     * @param coordsUnits new value of units of coordinates. This array is not copied.
     */
    public void setCoordsUnits(String[] coordsUnits)
    {
        this.coordsUnits = coordsUnits;
    }

    /**
     * Sets space dimension.
     * 
     * @param nSpace new value of space dimension.
     */
    public void setNSpace(int nSpace)
    {
        if (nSpace < 1 || nSpace > 3) {
            throw new IllegalArgumentException("Unsupported field nSpace=" + nSpace + ". Acceptable nSpace values are 1, 2 or 3.");
        }
        this.nSpace = nSpace;
    }

    /**
     * Returns space dimension.
     * 
     * @return space dimension
     */
    public int getNSpace()
    {
        return nSpace;
    }

}
