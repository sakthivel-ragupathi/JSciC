/* ***** BEGIN LICENSE BLOCK *****
 * Java Scientific Containers (JSciC)
 * Copyright (C) 2015 onward University of Warsaw, ICM
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * ***** END LICENSE BLOCK ***** */
package pl.edu.icm.jscic;

import static org.apache.commons.math3.util.FastMath.*;
import pl.edu.icm.jscic.utils.ArrayUtils;

/**
 * A node of cell tree used to find the cell containing the given point.
 *
 * @author Krzysztof S. Nowinski
 *
 * University of Warsaw, ICM
 */
public class GeoTreeNode implements Runnable {

    private static final int MAX_CLASS = 4;
    private static final int NBINS = 16385;
    private int level = 0;
    private int maxParallelLevel = -1;
    private boolean fullySplit = false;
    private int axis = -1;
    private float threshold = 0;
    private int[] cells;
    private GeoTreeNode nodeBelow = null;
    private GeoTreeNode nodeAbove = null;
    private float[][] cellExtents;
    private int dim = 3;

    /**
     * Creates a new instance of GeoTreeNode
     *
     * @param dim space dimension of the field
     * @param cells indices of cells
     * @param cellExtents for each cell, minimal and maximal coordinates of cell
     * nodes
     */
    public GeoTreeNode(int dim, int[] cells, float[][] cellExtents) {
        this.dim = dim;
        this.cells = cells;
        this.cellExtents = cellExtents;
    }

    /**
     * Creates a new instance of GeoTreeNode
     *
     * @param dim space dimension of the field
     * @param cells indices of cells
     * @param cellExtents for each cell, minimal and maximal coordinates of cell
     * nodes
     * @param level level of the node in the cell tree
     * @param maxParallelLevel number of threads
     */
    public GeoTreeNode(int dim, int[] cells, float[][] cellExtents, int level, int maxParallelLevel) {
        this.dim = dim;
        this.cells = cells;
        this.cellExtents = cellExtents;
        this.level = level;
        this.maxParallelLevel = maxParallelLevel;
    }

    /**
     * Creates a new instance of 3D GeoTreeNode
     *
     * @param cells indices of cells
     * @param cellExtents for each cell, minimal and maximal coordinates of cell
     * nodes
     *
     */
    public GeoTreeNode(int[] cells, float[][] cellExtents) {
        this(3, cells, cellExtents);
    }

    /**
     * Creates a new instance of 3D GeoTreeNode
     *
     * @param cells indices of cells
     * @param cellExtents for each cell, minimal and maximal coordinates of cell
     * nodes
     * @param level level of the node in the cell tree
     * @param maxParallelLevel number of threads
     */
    public GeoTreeNode(int[] cells, float[][] cellExtents, int level, int maxParallelLevel) {
        this(3, cells, cellExtents, level, maxParallelLevel);
    }

    /**
     * Returns a deep copy of this instance.
     *
     * @return a deep copy of this instance
     */
    public GeoTreeNode cloneDeep() {
        GeoTreeNode clone = new GeoTreeNode(dim, cells.clone(), ArrayUtils.cloneDeep(cellExtents), level, maxParallelLevel);
        clone.fullySplit = this.fullySplit;
        clone.axis = this.axis;
        clone.threshold = this.threshold;
        clone.nodeBelow = this.nodeAbove;
        clone.nodeAbove = this.nodeAbove;
        return clone;
    }

    /**
     * Returns a shallow copy of this instance.
     *
     * @return a shallow copy of this instance
     */
    public GeoTreeNode cloneShallow() {
        GeoTreeNode clone = new GeoTreeNode(dim, cells, cellExtents, level, maxParallelLevel);
        clone.fullySplit = this.fullySplit;
        clone.axis = this.axis;
        clone.threshold = this.threshold;
        clone.nodeBelow = this.nodeAbove;
        clone.nodeAbove = this.nodeAbove;
        return clone;
    }


    private void split() {
        if (cells == null || cells.length < MAX_CLASS) {
            return;
        }
        float[] cellsLow, cellsUp;
        int[] below = new int[NBINS];
        int[] above = new int[NBINS];
        int bestAxis = -1;
        int bestBalance = Integer.MAX_VALUE;
        int bestAbove = -1, bestBelow = -1;
        float bestThr = 0;
        for (int ax = 0; ax < dim; ax++) {
            cellsLow = cellExtents[ax];
            cellsUp = cellExtents[ax + dim];
            float low = Float.MAX_VALUE;
            float up = -Float.MAX_VALUE;
            for (int i = 0; i < cells.length; i++) {
                if (cellsLow[cells[i]] < low) {
                    low = cellsLow[cells[i]];
                }
                if (cellsUp[cells[i]] > up) {
                    up = cellsUp[cells[i]];
                }
            }
            float delta = (NBINS - 1) / (up - low);
            for (int i = 0; i < above.length; i++) {
                above[i] = below[i] = 0;
            }
            for (int i = 0; i < cells.length; i++) {
                above[max(0, min(NBINS - 1, (int) ((cellsUp[cells[i]] - low) * delta)))] += 1;
                below[max(0, min(NBINS - 1, (int) (ceil((cellsLow[cells[i]] - low) * delta))))] += 1;
            }
            for (int i = 1; i < below.length; i++) {
                below[i] += below[i - 1];
            }
            for (int i = above.length - 2; i >= 0; i--) {
                above[i] += above[i + 1];
            }
            int balance = 0;
            for (int i = 0; i < above.length - 1; i++) {
                if (above[i] <= below[i]) {
                    balance = above[i] + below[i];
                    if (balance < bestBalance) {
                        bestBalance = balance;
                        bestAxis = ax;
                        bestThr = low + i / delta;
                        bestAbove = above[i];
                        bestBelow = below[i];
                    }
                    break;
                }
            }
        }
        if (bestAbove > .9 * cells.length || bestBelow > .9 * cells.length) {
            return;
        }
        axis = bestAxis;
        threshold = bestThr;

        int[] c = new int[cells.length];
        int j = 0;
        cellsLow = cellExtents[axis];
        cellsUp = cellExtents[axis + dim];
        for (int i = 0; i < cells.length; i++) {
            if (cellsLow[cells[i]] < threshold) {
                c[j] = cells[i];
                j += 1;
            }
        }
        int[] cellsBelow = new int[j];
        System.arraycopy(c, 0, cellsBelow, 0, cellsBelow.length);
        j = 0;
        for (int i = 0; i < cells.length; i++) {
            if (cellsUp[cells[i]] >= threshold) {
                c[j] = cells[i];
                j += 1;
            }
        }
        int[] cellsAbove = new int[j];
        System.arraycopy(c, 0, cellsAbove, 0, cellsAbove.length);
        if (cellsBelow.length > .8 * cells.length || cellsAbove.length > .8 * cells.length) {
            return;
        }
        nodeBelow = new GeoTreeNode(dim, cellsBelow, cellExtents, level + 1, maxParallelLevel);
        nodeAbove = new GeoTreeNode(dim, cellsAbove, cellExtents, level + 1, maxParallelLevel);
        cells = null;
    }

    @Override
    public void run() {
        if (level < maxParallelLevel) {
            split();
        } else {
            splitFully();
        }
    }

    /**
     * Returns indices of cells in a leaf node.
     *
     * @return null if node is not a leaf, indices of cells otherwise
     */
    public int[] getCells() {
        return cells;
    }

    /**
     * Returns the root of subtree containing cells with vertices below the current threshold
     *
     * @return the root of subtree containing cells with vertices below the current threshold
     */
    public GeoTreeNode getNodeBelow() {
        return nodeBelow;
    }

    /**
     * Returns the root of subtree containing cells with vertices above the current threshold
     *
     * @return the root of subtree containing cells with vertices above the current threshold
     */
    public GeoTreeNode getNodeAbove() {
        return nodeAbove;
    }

    /**
     * Returns true if no further splits are desirable, false otherwise 
     *
     * @return true if no further splits are desirable, false otherwise
     */
    public boolean isFullySplit() {
        return fullySplit;
    }

    /**
     * Finds cells that can contain the given point
     *
     * @param p point coordinates
     *
     * @return indices of cells that can contain the given point 
     */
    public int[] getCells(float[] p) {
        if (cells != null) {
            return cells;
        }
        if (p[axis] < threshold) {
            return nodeBelow.getCells(p);
        } else {
            return nodeAbove.getCells(p);
        }
    }

    /**
     * Creates a full tree of cells contained in this node.
     */
    public void splitFully() {
        split();
        if (cells == null) {
            nodeBelow.splitFully();
            nodeAbove.splitFully();
        }
        fullySplit = true;
    }
}
