Java Scientific Containers (JSciC)
==================================

JSciC is a set of data structures and algorithms designed mainly for scientific data visualization and processing.
JSciC is developed as a part of [VisNow](http://visnow.icm.edu.pl/) project.

## Field

The main data structure in JSciC is called Field. It is an abstract data type describing a discretization of an area in the Euclidean 1-, 2- or 3-space together with a set of numeric and non-numeric data defined over this area.
The Field consists of three basic components - the obligatory structure, the (explicitly provided or implicitly defined) geometry and a (possibly empty) set of values.
The Field data type has two implementations – a RegularField type and an IrregularField type. 


## Regular field

The RegularField type covers all data sets with a regular structure, that is, a 1- 2- or 3-dimensional table structure that is characterized by a simple list of dimensions – e.g. a 100,000,000-long time series, an 1920x1080 HD image or a 512x512x300 CT scan.
In the simplest case, the geometry is defined implicitly with the node p<sub>ijk</sub> located at the point (i,j,k). In the general case of a regular curvilinear field all coordinates of all points can be provided explicitly, 
and in an intermediate setting the user can set affine coordinates consisting of the origin point p and one, two or three cell vectors v<sub>0</sub>, v<sub>1</sub>, v<sub>2</sub>.  

## Irregular field

The IrregularField type requires explicit definitions of both structure and geometry. The structure is determined by the number of nodes and a list of cell sets.
Each cell set is a collection of cells (point cells, segments, triangles, quadrangles, tetrahedral, pyramids, prisms and hexahedra). 
This data type covers basically all data sets occurring in FEM structural and CFD computations, but can be (and is) used for visualization of molecular structures,
abstract graph presentation etc. Currently, JSciC does not support arbitrary polygons/polyhedral requiring off-line triangulation of such sets.

## Data values

JSciC uses a generic DataArray type to hold a variety of simple numeric (unsigned byte, short, int, float and double), complex, logical, string and generic Java object data.
DataArray holds the proper (flat) array of values of corresponding type and some additional data, e.g. name, physical unit, minimal and maximal values etc. 
Each data item can be a scalar, a vector or a (possibly symmetric) matrix. Thus, vector or tensor data can be processed natively with JSciC.
The flat array of values is represented by LargeArray object from [JLargeArrays](https://gitlab.com/ICM-VisLab/JLargeArrays) project. This means that JSciC can store datasets with more than 2<sup>31</sup> elements.

## Time dependent data

The data values and node coordinates can be static or dynamic (defined for a list of time moments). It is sometimes important to have different data defined
for different lists of time moments (e.g. numeric forecast outputs provide atmospheric pressure and ground level temperature for each five-minute time step 
while precipitation data are integrated over one hour intervals and orography or land cover are definitely static. JSciC allows different lists of time moments for
each variable (data array or node coordinates) providing a reasonable, piecewise linear interpolation for a given time moment. The data can be dynamically modified with time steps added or removed, and JSciC takes care of consistent interpolation.
In the case of 2D fields, the time dependent data can be converted to a stack of slices of a 3D field. The only limitation of the JSciC time dependent data capabilities is the requirement of constant data structure (topology).

## Sources and Binaries

### JSciC 1.0

[JSciC-1.0-with-dependencies.jar](http://search.maven.org/remotecontent?filepath=pl/edu/icm/JSciC/1.0/JSciC-1.0-with-dependencies.jar) 

[JSciC-1.0-javadoc.jar](http://search.maven.org/remotecontent?filepath=pl/edu/icm/JSciC/1.0/JSciC-1.0-javadoc.jar) 

[JSciC-1.0-sources.jar](http://search.maven.org/remotecontent?filepath=pl/edu/icm/JSciC/1.0/JSciC-1.0-sources.jar) 

JSciC is available on maven central as

        <dependency>
            <groupId>pl.edu.icm</groupId>
            <artifactId>JSciC</artifactId>
            <version>1.0</version>
        </dependency>

##  JavaDoc
The documentation is available at [JSciC-1.0-javadoc](http://vislab.icm.edu.pl/JSciC-1.0-javadoc/).        